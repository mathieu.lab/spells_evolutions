dofile("data/scripts/game_helpers.lua")

-- new REFS
dofile_once("mods/spells_evolutions/files/lib/matolabu_tools.lua")
dofile("data/scripts/gun/gun_actions.lua")

function item_pickup(entity_item, entity_who_picked, name)
    local x, y = EntityGetTransform(entity_item)
    EntityLoad("data/entities/particles/image_emitters/spell_refresh_effect.xml", x, y - 12)
    GamePrintImportant("$itemtitle_spell_refresh", "$itemdesc_spell_refresh")

    -- GameRegenItemActionsInPlayer( entity_who_picked )

    ------------------------------------------------------------- START: of custom refresh spell code

    local player = getPlayerEntity()

    if entity_who_picked == player then

        local t_player_children = EntityGetAllChildren(entity_who_picked)

        -- GET inventories entities
        local ivnentory_quick
        local inventory_full
        for k, v in pairs(t_player_children) do
            if EntityGetName(v) == "inventory_quick" then
                ivnentory_quick = v
            elseif EntityGetName(v) == "inventory_full" then
                inventory_full = v
            end

            if ivnentory_quick ~= nil and inventory_full ~= nil then
                break -- we got what we need, quit!
            end
        end

        -- FUNCTIONS
        function refresh_spells(t_spells)
            for i, spell in pairs(t_spells) do

                local spell_ItemComponent = EntityGetFirstComponentIncludingDisabled(spell, "ItemComponent")
                local uses_remaining = ComponentGetValue2(spell_ItemComponent, "uses_remaining")

                if uses_remaining ~= -1 then -- if it's a munition spell

                    local spell_ItemActionComponent = EntityGetFirstComponentIncludingDisabled(spell,
                        "ItemActionComponent")
                    local entity_action_id = ComponentGetValue2(spell_ItemActionComponent, "action_id")

                    for i, action in pairs(actions) do

                        if action.id == entity_action_id then
                            local action_max_uses = action.max_uses

                            if uses_remaining < action_max_uses then -- refresh the spell only if the ammunition is below the initial quatity.
                                ComponentSetValue2(spell_ItemComponent, "uses_remaining", action_max_uses)
                            end

                            break
                        end
                    end
                end
            end
        end

        ----------------------------------------------
        -- refresh spells in inventory FULL
        ----------------------------------------------

        local t_inv_full_spells = EntityGetAllChildren(inventory_full) or {}
        refresh_spells(t_inv_full_spells)

        -------------------------------------------------
        -- refresh spells in inventory QUICK
        -------------------------------------------------

        local t_inv_quick_items = EntityGetAllChildren(ivnentory_quick)
        local t_inv_quick_wands = {}
        for i, item in pairs(t_inv_quick_items) do
            -- if EntityHasTag(item, "wand") == true then
            if EntityHasTag(item, "wand") == true then
                table.insert(t_inv_quick_wands, item)
            end
        end

        for i, wand in pairs(t_inv_quick_wands) do
            local t_wand_spells = EntityGetAllChildren(wand) or {}
            refresh_spells(t_wand_spells)
        end
    end

    ------------------------------------------------------------- END: of custom refresh spell code

    -- remove the item from the game
    EntityKill(entity_item)
end
