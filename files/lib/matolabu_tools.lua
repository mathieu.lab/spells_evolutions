-- NOTE
------------- Table -----------------
-- pairs = all keys
-- ipairs = only numeric an continuous keys
--
-- hashtag: #
--
-- simple custom print function --> convenient to disable all prints quickly
function zePrint(anything)
    print(tostring(anything))
end

-- simple utility function to return the player entity
function getPlayerEntity()
    local players = EntityGetWithTag("player_unit")
    if #players == 0 then
        return
    end
    return players[1]
end

-- inspired by Coxas' function: get current wand held --> But compatible with any entity!
function getCurrentlyEquippedWand(entity_id)
    local i2c_id = EntityGetFirstComponentIncludingDisabled(entity_id, "Inventory2Component")
    local wand_id = ComponentGetValue2(i2c_id, "mActiveItem")
    if (EntityHasTag(wand_id, "wand")) then
        return wand_id
    else
        return -1
    end
end

-- convert Noita wand's "rotation" to a clean radian value
function RotationToRadian(rotation)
    if rotation < 0 then
        rotation = -rotation
    elseif rotation > 0 then
        rotation = math.pi + (math.pi - rotation)
    elseif rotation == 0 then
    end

    return rotation
end

-- OLD GetEnemyModifierProbability

-- get the name of the .xml entity file V2!
function GetTheFilenameOfTheEntity(entity_id)
    local entity_filename = EntityGetFilename(entity_id)
    local enemy_name = string.gsub(entity_filename, ".xml", "") -- remove ".xml" extension

    -- get the index of the character just after the last "/", so the index of the first character of the entity name
    local start_index = 0
    while string.find(enemy_name, "/", start_index) ~= nil do
        start_index = string.find(enemy_name, "/", start_index) + 1
    end

    local enemy_name = string.sub(enemy_name, start_index) -- final: clean cut the entity name

    return enemy_name
end

-- print table
function PrintTable(table, tablename)
    local table_name = tablename or ""
    print("= = = = = START print a table: " .. table_name .. " = = = = =")
    if table ~= nil then
        for k, v in pairs(table) do
            if tostring(type(v)) == "table" then
                print("--> type of value is a Table, let's print it! ")
                print(tostring(k) .. " = " .. tostring(v) .. " ------------------------------------- table content = \n")
                for k2, v2 in pairs(v) do
                    print(tostring(k2) .. " = " .. tostring(v2))
                end
            else
                -- print("_next value type = "..  tostring(type(v)))
                print(tostring(k) .. " = " .. tostring(v))
            end
        end
    else
        print([[/!\/!\/!\/!\/!\ The table we want to print = nil /!\/!\/!\/!\/!\]])
    end
    print("= = = = = END print a table: " .. table_name .. " = = = = =")
end

-- print entity
function PrintEntity(entity_id)
    print("-- =========== START print entity + components + members ===========")
    if entity_id ~= nil and entity_id ~= 0 then
        print("entity id = " .. tostring(entity_id))
        print("entity name = " .. tostring(EntityGetName(entity_id)))
        print("entity filename = " .. tostring(EntityGetFilename(entity_id)))
        print("entity tags = " .. tostring(EntityGetTags(entity_id)))

        local entity_x, entity_y = EntityGetTransform(entity_id)
        print("entity_x = " .. tostring(entity_x))
        print("entity_y = " .. tostring(entity_y))

        local t_entity_children = EntityGetAllChildren(entity_id) or {}
        print("entity number of children = " .. tostring(#t_entity_children))

        local t_entity_components = EntityGetAllComponents(entity_id)
        for i, comp in pairs(t_entity_components) do
            print("\n\n----- ComponentGetTypeName = " .. ComponentGetTypeName(comp))
            local members = ComponentGetMembers(comp)
            for j, comp2 in pairs(members) do
                if j == "mAimingVector" -- ControlsComponent
                or j == "mAimingVectorNormalized" -- //
                or j == "mAimingVectorNonZeroLatest" -- //
                or j == "mGamepadAimingVectorRaw" -- //
                or j == "mJumpVelocity" -- //
                or j == "mMousePosition" -- //
                or j == "mMousePositionRaw" -- //
                or j == "mMousePositionRawPrev" -- //
                or j == "mMouseDelta" -- //
                or j == "mGamepadIndirectAiming" -- //
                or j == "mGamePadCursorInWorld" -- //
                or j == "offset" -- HitboxComponent & HotspotComponent
                or j == "mSmoothedItemAngleVec" -- Inventory2Component
                or j == "ui_container_size" -- InventoryComponent
                or j == "ui_element_size" -- //
                or j == "ui_position_on_screen" -- //
                or j == "spawn_pos" -- ItemComponent
                or j == "inventory_slot" -- //
                or j == "mLatestItemOverlapInfoBoxPosition" -- ItemPickUpperComponent
                or j == "transform_offset" -- SpriteComponent
                or j == "offset_animator_offset" -- //
                then
                    local x, y = ComponentGetValue2(comp, j)
                    print(tostring(j) .. " =          " .. tostring(x) .. "            ;             " .. tostring(y))
                else
                    print(tostring(j) .. " = " .. tostring(comp2))
                end
            end
        end
    elseif entity_id == nil then
        print([[ /!\ /!\ /!\ /!\ /!\ The entity to print == nill, game over!]])
    elseif entity_id == 0 then
        print([[ /!\ /!\ /!\ /!\ /!\ The entity to print == 0, game over!]])
    end
    print("-- =========== END print an entity components and members ===========")
end

-- copied from Evaisa's "spell_runes" mod. If you see this message: thank you very much for all the help you have given me!
function EntityGetVariable(entity_id, variable_name, variable_type)
    value = nil
    t_variable_storage_components = EntityGetComponentIncludingDisabled(entity_id, "VariableStorageComponent")
    if (t_variable_storage_components ~= nil) then
        for k, v in pairs(t_variable_storage_components) do
            name_out = ComponentGetValue2(v, "name")
            if (name_out == variable_name) then
                value = ComponentGetValue2(v, "value_" .. variable_type)
            end
        end
    end
    return value
end

-- copied from Evaisa's "spell_runes" mod. If you see this message: thank you very much for all the help you have given me!
function EntitySetVariable(entity_id, variable_name, variable_type, value)
    t_variable_storage_components = EntityGetComponentIncludingDisabled(entity_id, "VariableStorageComponent")
    has_been_set = false
    if (t_variable_storage_components ~= nil) then
        for k, v in pairs(t_variable_storage_components) do
            name_out = ComponentGetValue2(v, "name")
            if (name_out == variable_name) then
                ComponentSetValue2(v, "value_" .. variable_type, value)
                has_been_set = true
            end
        end
    end
    if (has_been_set == false) then
        comp = {}
        comp.name = variable_name
        comp["value_" .. variable_type] = value
        EntityAddComponent2(entity_id, "VariableStorageComponent", comp)
    end
end

function EntityKillVariable(entity_id, variable_name)
    t_variable_storage_components = EntityGetComponentIncludingDisabled(entity_id, "VariableStorageComponent")
    if (t_variable_storage_components ~= nil) then
        for k, v in pairs(t_variable_storage_components) do
            name_out = ComponentGetValue2(v, "name")
            if (name_out == variable_name) then
                EntityRemoveComponent(entity_id, v)
            end
        end
    end
end

function EntityCollideEntity(entity_a_id, entity_b_id) -- V1: for now, test only the first HitboxComponent of each entity!
    local A = entity_a_id
    local B = entity_b_id
    local A_x, A_y = EntityGetTransform(entity_a_id)
    local B_x, B_y = EntityGetTransform(entity_b_id)

    local A_hitbox_comp = EntityGetFirstComponentIncludingDisabled(A, "HitboxComponent")
    local B_hitbox_comp = EntityGetFirstComponentIncludingDisabled(B, "HitboxComponent")

    ----------------------------------------------------------------------------------- nobody has an HitboxComponent
    if A_hitbox_comp == nil and B_hitbox_comp == nil then
        if A_x == B_x and A_y == B_y then
            return true
        else
            return false
        end
        ----------------------------------------------------------------------------------- both have an HitboxComponent
    elseif A_hitbox_comp ~= nil and B_hitbox_comp ~= nil then
        local L -- entity on the left
        local L_hitbox_comp
        local L_x

        local R -- entity on the right
        local R_hitbox_comp
        local R_x

        if A_x < B_x then
            L = A
            L_hitbox_comp = A_hitbox_comp
            L_x = A_x
            R = B
            R_hitbox_comp = B_hitbox_comp
            R_x = B_x
        else
            L = B
            L_hitbox_comp = B_hitbox_comp
            L_x = B_x
            R = A
            R_hitbox_comp = A_hitbox_comp
            R_x = A_x
        end

        local L_hitbox_max_x = ComponentGetValue2(L_hitbox_comp, "aabb_max_x")
        local R_hitbox_min_x = ComponentGetValue2(R_hitbox_comp, "aabb_min_x")

        if L_x + L_hitbox_max_x < R_x + R_hitbox_min_x then -- horizontal test
            -- they don't collide, it's sure!
            return false

        else -- maybe they collide, let's do a vertical test
            local U -- entity up
            local U_hitbox_comp
            local U_y

            local D -- entity down
            local D_hitbox_comp
            local D_y

            if A_y < B_y then
                U = A
                U_hitbox_comp = A_hitbox_comp
                U_y = A_y
                D = B
                D_hitbox_comp = B_hitbox_comp
                D_y = B_y
            else
                U = B
                U_hitbox_comp = B_hitbox_comp
                U_y = B_y
                D = A
                D_hitbox_comp = A_hitbox_comp
                D_y = A_y
            end

            local U_hitbox_max_y = ComponentGetValue2(U_hitbox_comp, "aabb_max_y")
            local D_hitbox_min_y = ComponentGetValue2(D_hitbox_comp, "aabb_min_y")

            if U_y + U_hitbox_max_y < D_y + D_hitbox_min_y then -- vertical test
                -- they don't collide, too bad!
                return false
            else
                -- they collide horizontally AND vertically, oh yeah!
                return true
            end
        end

        ----------------------------------------------------------------------------------- Only one entity has an HitboxComponent
    else
        -- TODO
    end
end

function CanPlayerTinkerWithWands() -- bool
    local player_can_tinker_with_wands = false

    local entity_id = getPlayerEntity()
    local entity_x, entity_y = EntityGetTransform(entity_id)
    local player_has_perk_edit_wands_everywhere = GameGetGameEffect(entity_id, "EDIT_WANDS_EVERYWHERE")
    local player_has_perk_no_wand_editing = GameGetGameEffect(entity_id, "NO_WAND_EDITING")

    if player_has_perk_edit_wands_everywhere ~= 0 and player_has_perk_no_wand_editing == 0 then -- if only EDIT_WANDS_EVERYWHERE
        player_can_tinker_with_wands = true

    elseif player_has_perk_edit_wands_everywhere == 0 and player_has_perk_no_wand_editing ~= 0 then -- if only NO_WAND_EDITING
        player_can_tinker_with_wands = false

    else -- neither of these perks OR both of them (same effec)

        local player_is_in_workshop_tinker_zone = false

        local t_workshop_entities = EntityGetInRadiusWithTag(entity_x, entity_y, 10000, "workshop") or {}

        for i, workshop_id in pairs(t_workshop_entities) do
            local player_collide_this_workshop = EntityCollideEntity(entity_id, workshop_id)

            if player_collide_this_workshop == true then
                player_is_in_workshop_tinker_zone = true
                break
            end
        end
        player_can_tinker_with_wands = player_is_in_workshop_tinker_zone
    end
    return player_can_tinker_with_wands
end

function GetAllInfiniteProjectileSpells()
    local t_all_projectile_spells = {}
    dofile_once("data/scripts/gun/gun_enums.lua")
    dofile("data/scripts/gun/gun_actions.lua") -- dofile, not once, because we need the updated list of spells!
    for i, action in ipairs(actions) do

        -- print(" ---> action.id = " .. tostring(action.id))

        if action.type == ACTION_TYPE_PROJECTILE then
            local action_max_uses = action.max_uses
            if action_max_uses == -1 or action_max_uses == nil then
                table.insert(t_all_projectile_spells, action)
            end
        end
    end
    return t_all_projectile_spells
end

function SpellCanBeBoostedOrIsBoosted(spell_entity_id_or_spell_action_id)
    -- This function check if the spell is 
    -- a boosted spell (lvl2-3-4-5): by checking the prefix of its "id"
    -- or can be boosted (lvl1): by checking if the spell have boosted versions of itself in AllInfiniteProjectileSpells

    local spell_id
    if type(spell_entity_id_or_spell_action_id) == "number" then
        local spell_ItemActionComponent = EntityGetFirstComponentIncludingDisabled(spell_entity_id_or_spell_action_id,
            "ItemActionComponent")
        spell_id = ComponentGetValue2(spell_ItemActionComponent, "action_id")

    elseif type(spell_entity_id_or_spell_action_id) == "string" then
        spell_id = spell_entity_id_or_spell_action_id
    end

    local t_all_infinite_projectile_spells = GetAllInfiniteProjectileSpells() or {}

    -- create the final 'can be boosted or is boosted spells' table
    local t_boosted_or_can_be_boosted_spells = {}
    for i, spell in pairs(t_all_infinite_projectile_spells) do
        local current_spell_id = spell.id

        -- get the spell.id prefix + spell.name prefixes
        dofile_once("mods/spells_evolutions/files/config.lua")

        local entity_action_id_prefix = string.match(current_spell_id, "(.-)%_") or ""
        -- LightPrint("entity_action_id_prefix = " .. tostring(entity_action_id_prefix))
        local entity_action_id_suffix = string.sub(current_spell_id, -5) or ""
        -- LightPrint("entity_action_id_suffix = " .. tostring(entity_action_id_suffix))

        -- IF I'm a boosted spell
        if entity_action_id_prefix == spell_id_prefix then -- I'm a boosted spell
            table.insert(t_boosted_or_can_be_boosted_spells, spell)

            -- IF I'm a NOT boosted spell
        else
            for j, inf_spell in pairs(t_all_infinite_projectile_spells) do
                -- IF there are boosted version of myself
                if spell_id_prefix .. "_" .. current_spell_id .. "_LVL2" == inf_spell.id then -- if I've a boosted version
                    table.insert(t_boosted_or_can_be_boosted_spells, spell)
                    break
                end
            end
        end
    end

    local match = false
    for i, action in pairs(t_boosted_or_can_be_boosted_spells) do
        if action.id == spell_id then
            match = true
            break
        end
    end
    return match
end

function SpellCanBeCumulated(spell_entity_id_or_spell_action_id)
    -- This function check if the spell is a spell with munitions

    local spell_id
    if type(spell_entity_id_or_spell_action_id) == "number" then
        local spell_ItemActionComponent = EntityGetFirstComponentIncludingDisabled(spell_entity_id_or_spell_action_id,
            "ItemActionComponent")
        spell_id = ComponentGetValue2(spell_ItemActionComponent, "action_id")

    elseif type(spell_entity_id_or_spell_action_id) == "string" then
        spell_id = spell_entity_id_or_spell_action_id
    end

    return false
end

function RemoveLuaScript(entity_id, lua_script_path)
    local t_lua_components = EntityGetComponentIncludingDisabled(entity_id, "LuaComponent") or {}
    for i, comp in pairs(t_lua_components) do
        if ComponentGetValue2(comp, "script_source_file") == lua_script_path then
            EntityRemoveComponent(entity_id, comp)
            break
        end
    end
end

function GetSpellLevel(spell_entity_id_or_spell_action_id)
    local spell_action_id
    if type(spell_entity_id_or_spell_action_id) == "number" then
        local spell_ItemActionComponent = EntityGetFirstComponentIncludingDisabled(spell_entity_id_or_spell_action_id,
            "ItemActionComponent")
        spell_action_id = ComponentGetValue2(spell_ItemActionComponent, "action_id")

    elseif type(spell_entity_id_or_spell_action_id) == "string" then
        spell_action_id = spell_entity_id_or_spell_action_id
    end

    local spell_test_if_level = string.match(spell_action_id, "(.+)%_LVL%d")

    if spell_test_if_level == nil then ---------------------------------------------- Spell is LVL 1 
        return 1
    else ---------------------------------------------------------------------------------- Spell is LVL 2+
        local spell_level = string.sub(spell_action_id, -1)
        local spell_level_number = tonumber(spell_level)
        return spell_level_number
    end
end

function GetSlotSpritePath(spell_entity_id_or_spell_action_id, transparent_bool)
    local spell_level = GetSpellLevel(spell_entity_id_or_spell_action_id)

    local render
    if transparent_bool == true then
        render = "_transparent.png"
    else
        render = "_opaque.png"
    end

    local slot_sprite_path = "mods/spells_evolutions/files/ui_gfx/slots/slot_lvl" .. tostring(spell_level) .. render
    return slot_sprite_path
end

-- "Only A Spell" Prints
local spell_to_print = "SKULL_GRENADE"

function OnlyASpellPrint(spell, string)
    if spell.id == spell_to_print then
        print([[------------------------------------------------------------]] .. spell.id ..
                  [[------------------------------------------------------------
        ]] .. " print --> " .. string)
    end
end

function OnlyThisSpellPrint(spell, spell_wanted, string) -- v1.8
    if spell.id == spell_wanted then
        -- print([[------------------------------------------------------------]] .. spell.id ..
        --           [[------------------------------------------------------------
        -- ]] .. " print --> " .. string)
        print(string)
    end
end

function OnlyASpellPrintTable(spell, table, tablename)
    if spell.id == spell_to_print then
        print([[------------------------------------------------------------]] .. spell.id ..
                  [[------------------------------------------------------------
        ]])
        PrintTable(table, tablename)
    end
end

-- Be careful, when the table is read by the "for" the entries do not necessarily come out in the right order...
function StringFindAll(string, pattern, start_index)
    local t_starts = {}
    local t_ends = {}
    local t_strings = {}

    local init
    if start_index ~= nil then
        init = start_index
    elseif start_index == nil then
        init = 1
    end

    local i = 1
    while (string.find(string, pattern, init) ~= nil) do
        local find_start, find_end = string.find(string, pattern, init)
        table.insert(t_starts, i, find_start)
        table.insert(t_ends, i, find_end)
        table.insert(t_strings, i, string.sub(string, find_start, find_end))
        init = find_end + 1

        i = i + 1
    end

    return t_starts, t_ends, t_strings
end

-- Because sometimes if I do 'local table_copy = table_source' 
-- and I edit the copy
-- it edits the source too..... example: with the table: 'spell.related_projectiles'
function CopyTable(table_source)
    local table_copy = {}
    for k, v in pairs(table_source) do
        table_copy[k] = v
    end
    return table_copy
end

-- PREPARE string to be compatible with pattern matching, in case of finding specials signs --->    ()%[]+-
function PrepareStringForPatternMatching(string)
    local new_string = string
    -- print(" >>>>>>>>>>>>>>>>>> string = "..string)
    new_string = string.gsub(new_string, "%%", "%%%%") -- must be in first place, otherwise it modifies the other modifications...
    new_string = string.gsub(new_string, "%(", "%%%(")
    new_string = string.gsub(new_string, "%)", "%%%)")
    new_string = string.gsub(new_string, "%[", "%%%[")
    new_string = string.gsub(new_string, "%]", "%%%]") -- seems to not be essential, but we never know...
    new_string = string.gsub(new_string, "%+", "%%%+")
    new_string = string.gsub(new_string, "%-", "%%%-")
    -- print(" >>>>>>>>>>>>>>>>>> new_string = "..new_string)
    return new_string
end
