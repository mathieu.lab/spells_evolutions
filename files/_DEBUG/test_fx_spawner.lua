dofile_once("mods/spells_evolutions/files/lib/matolabu_tools.lua")
dofile_once("mods/spells_evolutions/files/monsters_drop_list.lua")

local entity_id = GetUpdatedEntityID()
local x, y = EntityGetTransform(entity_id)

local vfx_y = y

-- EntityLoad("data/entities/particles/evaporation.xml", x, y) -- petite évaporation discrette

-- BREF
-- EntityLoad("data/entities/particles/gold_pickup_particles.xml", x-100, y) -- petites part gold discretes
-- EntityLoad("data/entities/particles/poof_yellow_tiny.xml", x, y) -- mini mini jaune
-- EntityLoad("data/entities/particles/tiny_ghost_poof.xml", x+50, y) -- mini mini blanc
-- EntityLoad("data/entities/particles/poof_red_tiny.xml", x+100, y) -- tout petit, rouge
-- EntityLoad("data/entities/particles/swarm_poof.xml", x, y) -- petit violet
-- EntityLoad("data/entities/particles/runestone_null.xml", x+100, y) -- petit violet sombre
-- EntityLoad("data/entities/particles/perk_reroll.xml", x-100, y) -- petit, des parts vertes
-- EntityLoad("data/entities/particles/beamstone_kick.xml", x-50, y) -- part vertes

-- EntityLoad("data/entities/particles/lantern_small_death.xml", x+100, y) -- moyen, peu de part, feu ?
-- EntityLoad("data/entities/particles/dust_explosion.xml", x-100, y) -- des part blanches qui tombent
-- EntityLoad("data/entities/particles/destruction.xml", x+50, y) -- explo part violettes
-- EntityLoad("data/entities/particles/poof_white_appear.xml", x-50, y) -- moyen, blanc, condensé
-- EntityLoad("data/entities/particles/poof_green_sparse.xml", x-50, y) -- moyen/big, green
-- EntityLoad("data/entities/particles/poof_red_sparse.xml", x+50, y) -- moyen, rouge
-- EntityLoad("data/entities/particles/blood_explosion.xml", x+100, y) -- boule de sang (en fou partout)

-- EntityLoad("data/entities/particles/_example_expanding_polymorph_explosion.xml", x-100, y) -- large, fumée rose
-- EntityLoad("data/entities/particles/polymorph_explosion.xml", x, y) -- moyen fumée rose + parts roses

-- EntityLoad("data/entities/particles/lantern_death.xml", x+50, y) -- large, peu de part, feu ?
-- EntityLoad("data/entities/particles/poof_blue.xml", x+50, y) -- gros, parts bleues
-- EntityLoad("data/entities/particles/poof_green.xml", x+100, y) -- gros, parts vertes
-- EntityLoad("data/entities/particles/poof_pink.xml", x, y) -- big, rose
-- EntityLoad("data/entities/particles/poof_white.xml", x+100, y) -- gros, blanc
-- EntityLoad("data/entities/particles/fireworks/firework_green.xml", x-50, y) -- gros pouf vert qui se dissipe
-- EntityLoad("data/entities/particles/fireworks/firework_pink.xml", x+100, y) -- gros gros pouf rose qui vol

-- EntityLoad("data/entities/particles/poof_green_huge.xml", x-100, y) -- HUGE, green EN FOU PARTOUT

-- EntityLoad("data/entities/particles/knockback_star.xml", x, y) -- petite étoile
-- EntityLoad("data/entities/particles/wand_pickup.xml", x-100, y) -- pleins de petites étoiles
-- EntityLoad("data/entities/particles/particle_sparks.xml", x+100, y) -- moyennes flames (esthétiques)

-- EntityLoad("data/entities/particles/neutralized.xml", x, y) -- eclat bleu et marron, moyen, peu de part
-- EntityLoad("data/entities/particles/neutralized_tiny.xml", x+50, y) -- eclat bleu et marron, petit, peu de part

-- EntityLoad("data/entities/particles/gold_pickup.xml", x, y) -- petit eclat gold
-- EntityLoad("data/entities/particles/gold_pickup_large.xml", x+100, y) -- moyen eclat gold
-- EntityLoad("data/entities/particles/gold_pickup_huge.xml", x+50, y) -- gros eclat gold

----------------------------------------------
-- ECLAT BLANC
----------------------------------------------
-- EntityLoad("data/entities/particles/particle_explosion/explosion_flare_blue.xml", x+150, y) -- flash, avec très peu de part bleues
-- EntityLoad("data/entities/particles/particle_explosion/explosion_flare_blue_small.xml", x+200, y) -- flash avec plus de part bleues
-- EntityLoad("data/entities/particles/particle_explosion/explosion_flare_medium.xml", x+250, y) -- avec ++ part oranges
-- EntityLoad("data/entities/particles/particle_explosion/explosion_flare_small.xml", x+300, y) -- avec + part oranges

----------------------------------------------
-- EXPLOSIONS
----------------------------------------------
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_smoke_gunpowder_short.xml", x-100, y) -- petite expl griscramé stylé
-- EntityLoad("data/entities/particles/particle_explosion/explosion_glue.xml", x, y) -- petite explo blanche stylée
-- EntityLoad("data/entities/particles/particle_explosion/explosion_smoke_pillar_top_left.xml", x+50, y) -- fumée mini beige
-- EntityLoad("data/entities/particles/particle_explosion/explosion_smoke_pillar.xml", x-50, y) --  fumée beige
-- EntityLoad("data/entities/particles/particle_explosion/explosion_smoke_pillar_pink.xml", x-50, y) --  fumée rose
-- EntityLoad("data/entities/particles/particle_explosion/explosion_smoke_pillar_red.xml", x, y)  --  fumée rouge
-- EntityLoad("data/entities/particles/particle_explosion/explosion_smoke_pillar_green.xml", x, y) --  fumée verte
-- EntityLoad("data/entities/particles/particle_explosion/explosion_smoke_pillar_green_large.xml", x+50, y)  --  fumée+ verte
-- EntityLoad("data/entities/particles/particle_explosion/explosion_smoke_pillar_grey.xml", x+100, y)  --  fumée grise
-- EntityLoad("data/entities/particles/particle_explosion/explosion_smoke_pillar_large.xml", x-100, y)   --  fumée++ grise

-- EntityLoad("data/entities/particles/particle_explosion/main_blue_tiny.xml", x-50, y) -- mini trails bleu
-- EntityLoad("data/entities/particles/particle_explosion/main_blue_small.xml", x-100, y) -- trails bleu
-- EntityLoad("data/entities/particles/particle_explosion/main_bluesmoke_small.xml", x+50, y) -- fumée bleue + trails bleus

-- EntityLoad("data/entities/particles/particle_explosion/main_green.xml", x-100, y) -- large fumée vert
-- EntityLoad("data/entities/particles/particle_explosion/main_green_small.xml", x, y) -- fumée vet + fioritures

-- EntityLoad("data/entities/particles/particle_explosion/main.xml", x+50, y) -- explo beige + trails orange
-- EntityLoad("data/entities/particles/particle_explosion/main_contained.xml", x+100, y) -- explo beige +  part orange + trail orange
-- EntityLoad("data/entities/particles/particle_explosion/main_small.xml", x+50, y)  -- explo beige + trails orange

-- EntityLoad("data/entities/particles/fireworks/firework_orange.xml", x+50, y) -- HUGE explo beige + trails orange
-- EntityLoad("data/entities/particles/particle_explosion/main_large.xml", x+50, y) -- HUGE Fumée beige
-- EntityLoad("data/entities/particles/particle_explosion/main_large_radius.xml", x+100, y) -- HUGE Fumée beige
-- EntityLoad("data/entities/particles/particle_explosion/main_medium.xml", x-100, y) -- Fumée beige + trails orange
-- EntityLoad("data/entities/particles/particle_explosion/main_nospark.xml", x-50, y) -- Fumée beige + trails orange

-- EntityLoad("data/entities/particles/particle_explosion/main_bluesmoke.xml", x, y)  -- HUGE explo bleuciel + trails bleu
-- EntityLoad("data/entities/particles/particle_explosion/main_pink.xml", x, y)
-- EntityLoad("data/entities/particles/particle_explosion/main_green_large.xml", x-50, y)

-- EntityLoad("data/entities/particles/particle_explosion/main_gunpowder_small.xml", x-100, y)
-- EntityLoad("data/entities/particles/particle_explosion/main_gunpowder_large.xml", x+50, y) -- expl+feu+ large trail qui retombent orange
-- EntityLoad("data/entities/particles/particle_explosion/main_gunpowder_medium.xml", x+100, y) -- expl+feu+ large trail qui retombent orange

-- EntityLoad("data/entities/particles/fireworks/firework_blue.xml", x-100, y) -- grosse trails bleues
-- EntityLoad("data/entities/particles/particle_explosion/main_blue.xml", x+100, y) -- grosse trails bleues

----------------------------------------------
-- RESTE UN PEU
----------------------------------------------
-- EntityLoad("data/entities/particles/heal_effect.xml", x+50, y) -- pailettes vertes, 4 sec

-- TRAIL 
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_blue_slow.xml", x+100, y) -- petit trail curly
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_green.xml", x-100, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_green_slow.xml", x, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_orange_slow.xml", x+100, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_pink.xml", x-100, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_pink_slow.xml", x, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_purple.xml", x+50, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_purple_slow.xml", x-100, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_red_slow.xml", x, y)

-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_blue_fast.xml", x+50, y) -- long trail curly
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_green_fast.xml", x-50, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_orange_fast.xml", x+50, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_pink_fast.xml", x-50, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_purple_fast.xml", x+100, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_red_fast.xml", x-50, y)

-- EntityLoad("data/entities/particles/particle_explosion/main_swirly_blue_giga.xml", x+100, y) -- pleins de long trails bleus curly
-- EntityLoad("data/entities/particles/particle_explosion/main_swirly_green_giga.xml", x-100, y)
-- EntityLoad("data/entities/particles/particle_explosion/main_swirly_orange_giga.xml", x-50, y)
-- EntityLoad("data/entities/particles/particle_explosion/main_swirly_pink.xml", x, y)
-- EntityLoad("data/entities/particles/particle_explosion/main_swirly_pink_giga.xml", x, y)
-- EntityLoad("data/entities/particles/particle_explosion/main_swirly_purple.xml", x+100, y)
-- EntityLoad("data/entities/particles/particle_explosion/main_swirly_purple_giga.xml", x-100, y)
-- EntityLoad("data/entities/particles/particle_explosion/main_swirly_purple_small.xml", x-50, y)
-- EntityLoad("data/entities/particles/particle_explosion/main_swirly_red.xml", x, y)
-- EntityLoad("data/entities/particles/particle_explosion/main_swirly_red_giga.xml", x+50, y)

-- feu ? 
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_spark_red_large.xml", x-100, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_spark_short.xml", x-50, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_spark_small.xml", x, y)

-- PAS UTILE MAIS INTERSSANT
-- EntityLoad("data/entities/particles/blue_dust.xml", x, y) -- particules bleues un peu partout dans l'air 
-- EntityLoad("data/entities/particles/gold_dust.xml", x-50, y) -- particules gold un peu partout dans l'air 
-- EntityLoad("data/entities/particles/telepathy.xml", x+50, y) -- etincelles de batons
-- EntityLoad("data/entities/particles/teleportation_source.xml", x-50, y) -- batons verticaux qui s'écartent
-- EntityLoad("data/entities/particles/teleportation_target.xml", x, y) -- batons verticaux qui s'écartent
-- EntityLoad("data/entities/particles/treble_eye.xml", x+50, y) -- oeil géant
-- EntityLoad("data/entities/particles/perks/projectile_repulsion_field.xml", x+50, y) -- le truc autour du failed alchemist là

--------------------------------------------------
-- 1 + 1 = LVL2    BLEU
--------------------------------------------------

-- EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl2/fusion_to_lvl2.xml", x, y) --   ===== MEGA PACK
-- EntityLoad("data/entities/particles/poof_blue.xml", x, y) -- gros, parts bleues  ===== PARTICULES
-- EntityLoad("data/entities/particles/particle_explosion/main_blue_small.xml", x, y) -- trails bleu =========== TRAIL
-- GameScreenshake(5, x, y)

--------------------------------------------------
-- 2 + 2 = LVL3    ROUGE
--------------------------------------------------

-- EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl3/fusion_to_lvl3.xml", x, y) --  ===== MEGA PACK
-- EntityLoad("data/entities/particles/poof_red_sparse.xml", x, y) -- moyen, rouge    ===== PARTICULES
-- GameScreenshake(10, x, y)

------------------------------------ NOP

-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_spark_red_large.xml", x, y)     -- =====  TRAILS
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_spark_red_large.xml", x, y)     -- =====  TRAILS
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_spark_red_large.xml", x, y)     -- =====  TRAILS
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_spark_red_large.xml", x, y)     -- =====  TRAILS
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_spark_red_large.xml", x, y)     -- =====  TRAILS
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_spark_red_large.xml", x, y)     -- =====  TRAILS
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_spark_red_large.xml", x, y)     -- =====  TRAILS
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_spark_red_large.xml", x, y)     -- =====  TRAILS
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_spark_red_large.xml", x, y)     -- =====  TRAILS
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_spark_red_large.xml", x, y)     -- =====  TRAILS

-- EntityLoad("data/entities/particles/particle_explosion/main_swirly_red.xml", x, y) -- TROP GROS
-- trop vraies explosions
-- EntityLoad("data/entities/particles/particle_explosion/main_gunpowder_small.xml", x-100, y)
-- EntityLoad("data/entities/particles/particle_explosion/main_gunpowder_large.xml", x, y) -- expl+feu+ large trail qui retombent orange
-- EntityLoad("data/entities/particles/particle_explosion/main_gunpowder_medium.xml", x+100, y) -- expl+feu+ large trail qui retombent orange
-- EntityLoad("data/entities/particles/particle_explosion/main.xml", x-100, y) -- explo beige + trails orange
-- EntityLoad("data/entities/particles/particle_explosion/main_contained.xml", x, y) -- explo beige +  part orange + trail orange
-- EntityLoad("data/entities/particles/particle_explosion/main_small.xml", x+100, y)  -- explo beige + trails orange
-- EntityLoad("data/entities/particles/fireworks/firework_orange.xml", x-200, y) -- HUGE explo beige + trails orange
-- EntityLoad("data/entities/particles/particle_explosion/main_large.xml", x-100, y) -- HUGE Fumée beige
-- EntityLoad("data/entities/particles/particle_explosion/main_large_radius.xml", x, y) -- HUGE Fumée beige
-- EntityLoad("data/entities/particles/particle_explosion/main_medium.xml", x+100, y) -- Fumée beige + trails orange
-- EntityLoad("data/entities/particles/particle_explosion/main_nospark.xml", x+200, y) -- Fumée beige + trails orange
-- AUTRE
-- EntityLoad("data/entities/particles/particle_sparks.xml", x-100, y) -- moyennes flames (esthétiques)
-- EntityLoad("data/entities/particles/lantern_death.xml", x+100, y) -- large, peu de part, feu ?
-- FUMEE
-- EntityLoad("data/entities/particles/particle_explosion/explosion_smoke_pillar.xml", x, y) --  fumée beige
-- pas intéressant si déjà le mix d'au-dessus

-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_red_fast.xml", x, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_red_fast.xml", x, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_red_fast.xml", x, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_red_fast.xml", x, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_red_fast.xml", x, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_red_fast.xml", x, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_red_fast.xml", x, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_red_fast.xml", x, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_red_fast.xml", x, y)
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_red_fast.xml", x, y)

--------------------------------------------------
-- 3 + 3 = LVL 4    VERT TURQUOISE
-------------------------------------------------

-- EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl4/fusion_to_lvl4.xml", x, vfx_y) -- === MEGA PACK
-- EntityLoad("data/entities/particles/fireworks/firework_green.xml", x, vfx_y) -- gros pouf vert qui se dissipe    ========  PARTICULES
-- GameScreenshake(20, x, y)

------------- NOP

-- EntityLoad("data/entities/particles/particle_explosion/main_green_large.xml", x-50, y) -- MEGA fummées vertes qui partent loin et retombent boule de feu...

-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_green.xml", x-100, y)   -- x1   
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_green_slow.xml", x, y)   -- x1
-- EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_green_fast.xml", x+100, y)   -- x1

-- EntityLoad("data/entities/particles/poof_green_sparse.xml", x-100, y) -- moyen/big, green
-- EntityLoad("data/entities/particles/poof_green.xml", x+100, y) -- gros, parts vertes

-- EntityLoad("data/entities/particles/particle_explosion/explosion_smoke_pillar_green.xml", x-100, y) --  fumée verte
-- EntityLoad("data/entities/particles/particle_explosion/explosion_smoke_pillar_green_large.xml", x+100, y)  --  fumée+ verte

-- EntityLoad("data/entities/particles/particle_explosion/main_green_small.xml", x+100, y) -- 2-3 fumées vert + fioritures + part oranges/marrons

--------------------------------------------------
-- 4 + 4 = LVL 5     ROSE VIOLET
--------------------------------------------------

-- VFX

-- EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl5/fusion_to_lvl5.xml", x, vfx_y) -- == MEGA PACK CUSTOM
-- EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl5/firework_pink_edit.xml", x, vfx_y) -- gros gros pouf rose qui vol    ====  PARTICULES
-- EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl5/gold_pickup_huge_edit.xml", x, vfx_y) -- gros eclat gold
-- EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl5/polymorph_explosion_edit.xml", x, vfx_y) -- moyen fumée rose + parts roses
-- EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl5/poof_pink_huge.xml", x, vfx_y) -- HUGE PART FOUT PARTOUT
-- GameScreenshake(30, x, y)

-- SFX

-- GamePlaySound("data/audio/Desktop/projectiles.bank", "animals/wraith_glowing/shoot", x, y) -- random  3 sounds 
-- GamePlaySound("data/audio/Desktop/projectiles.bank", "animals/wraith_glowing/shoot", x, y) -- random  3 sounds 
-- GamePlaySound("data/audio/Desktop/event_cues.bank", "event_cues/spell_refresh/create", x, y) -- spell_refresh_1
-- GamePlaySound("data/audio/Desktop/event_cues.bank", "event_cues/heartbeat/create", x, y) -- heartbeat_double
-- GamePlaySound("data/audio/Desktop/explosion.bank", "explosions/holy", x, y) -- bomb_holy_create_add_01 & 02
-- GamePlaySound("data/audio/Desktop/explosion.bank", "explosions/holy", x, y) -- bomb_holy_create_add_01 & 02
-- GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
-- GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
-- GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)

----------------------- NOP

-- EntityLoad("data/entities/particles/_example_expanding_polymorph_explosion.xml", x-100, y) -- large, fumée rose

-- EntityLoad("data/entities/particles/poof_pink.xml", x+100, y) -- big, rose

-- EntityLoad("data/entities/particles/particle_explosion/main_swirly_purple.xml", x+100, y)
-- EntityLoad("data/entities/particles/particle_explosion/main_swirly_purple_giga.xml", x-100, y)
-- EntityLoad("data/entities/particles/particle_explosion/main_swirly_purple_small.xml", x-50, y)

-- EntityLoad("data/entities/particles/gold_pickup_particles.xml", x-100, y) -- petites part gold discretes
-- EntityLoad("data/entities/particles/gold_pickup.xml", x, y) -- petit eclat gold
-- EntityLoad("data/entities/particles/gold_pickup_large.xml", x+100, y) -- moyen eclat gold
-- EntityLoad("data/entities/particles/gold_pickup_huge.xml", x+50, y) -- gros eclat gold
-- EntityLoad("data/entities/particles/knockback_star.xml", x, y) -- petite étoile
-- EntityLoad("data/entities/particles/wand_pickup.xml", x-100, y) -- pleins de petites étoiles

---------------------------------- TESTER 2021-12-16 ----------------------------------------------

-- 1+1 = 2

-- EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl2/fusion_to_lvl2.xml", x, vfx_y) --   ===== MEGA PACK
-- EntityLoad("data/entities/particles/poof_blue.xml", x, vfx_y) -- gros, parts bleues  ===== PARTICULES
-- EntityLoad("data/entities/particles/particle_explosion/main_blue_small.xml", x, vfx_y) -- trails bleu =========== TRAIL
-- EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl2/polymorph_explosion_edit_blue.xml", x, vfx_y) -- p'tites part qui flottent

-- 2 + 2 = 3 

-- EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl3/fusion_to_lvl3.xml", x, vfx_y) --  ===== MEGA PACK
-- EntityLoad("data/entities/particles/poof_red_sparse.xml", x, vfx_y) -- moyen, rouge    ===== PARTICULES
-- EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl3/polymorph_explosion_edit_red.xml", x, vfx_y) -- p'tites part qui flottent

-- 3 + 3 = 4

-- EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl4/fusion_to_lvl4.xml", x, vfx_y) -- === MEGA PACK
-- EntityLoad("data/entities/particles/fireworks/firework_green.xml", x, vfx_y) -- gros pouf vert qui se dissipe    ========  PARTICULES

-- 4 + 4 = 5

-- EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl5/fusion_to_lvl5.xml", x, vfx_y) -- == MEGA PACK CUSTOM
-- EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl5/firework_pink_edit.xml", x, vfx_y) -- gros gros pouf rose qui vol    ====  PARTICULES
-- EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl5/gold_pickup_huge_edit.xml", x, vfx_y) -- gros eclat gold
-- EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl5/polymorph_explosion_edit.xml", x, vfx_y) -- moyen fumée rose + parts roses
-- EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl5/poof_pink_huge.xml", x, vfx_y) -- HUGE PART FOUT PARTOUT



----- TESTER for begin merging animation

-- EntityLoad("data/entities/particles/poof_white.xml", x, y) -- gros, blanc
-- EntityLoad("data/entities/particles/dust_explosion.xml", x, y) -- des part blanches qui tombent