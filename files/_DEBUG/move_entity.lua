dofile_once("mods/spells_evolutions/files/lib/matolabu_tools.lua")

local entity_id = GetUpdatedEntityID()

-- local projectile_component = EntityGetFirstComponentIncludingDisabled(entity_id, "ProjectileComponent")
-- local t_projectile_component_members = ComponentGetMembers(projectile_component)
-- local t_config_explosion_members = ComponentObjectGetMembers(projectile_component, "config_explosion") or {}

-- GET entity's transform
local x, y, rot, scale_x, scale_y = EntityGetTransform(entity_id)
-- print(tostring(entity_id) .. "'s transform is = \n x = " .. tostring(x) .. "\n y = " .. tostring(y))

-- EntitySetTransform( entity_id:int, x:number, y:number = 0, rotation:number = 0, scale_x:number = 1, scale_y:number = 1 ) 	

-- EntityApplyTransform( entity_id, x:number, y:number = 0, rotation:number = 0, scale_x:number = 1, scale_y:number = 1 )
-- Sets the transform and tries to immediately refresh components that calculate values based on an entity's transform.

-- same?
-- EntityApplyTransform(entity_id, x + 1, y, rot, scale_x, scale_y)
-- EntitySetTransform(entity_id, x + 1, y, rot, scale_x, scale_y)

-- LOAD timer
local timer_current = EntityGetVariable(entity_id, "timer_current", "float")
-- IF no valaue, let's create it
if timer_current == nil then
    EntitySetVariable(entity_id, "timer_current", "float", 0)
    timer_current = 0
end

-- LOAD x
local x_at_start = EntityGetVariable(entity_id, "x_at_start", "float")
-- IF no valaue, let's create it
if x_at_start == nil then
    EntitySetVariable(entity_id, "x_at_start", "float", x)
    x_at_start = x
end

-- LOAD y
local y_at_start = EntityGetVariable(entity_id, "y_at_start", "float")
-- IF no valaue, let's create it
if y_at_start == nil then
    EntitySetVariable(entity_id, "y_at_start", "float", y)
    y_at_start = y
end


-- CALCULATE movement X
local x_current = x_at_start + (math.sin(timer_current) * 20)
local y_current = y_at_start + (math.sin(timer_current + (math.pi/2)) * 20)

-- SET entity location
EntitySetTransform(entity_id, x_current, y_current, rot, scale_x, scale_y)

-- INCREMENT timer
timer_current = timer_current + 0.1
EntitySetVariable(entity_id, "timer_current", "float", timer_current)