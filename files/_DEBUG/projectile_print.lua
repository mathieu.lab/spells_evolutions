dofile_once("mods/spells_evolutions/files/lib/matolabu_tools.lua")

local entity_id = GetUpdatedEntityID()

local projectile_component = EntityGetFirstComponentIncludingDisabled(entity_id, "ProjectileComponent")
local t_projectile_component_members = ComponentGetMembers(projectile_component)
local t_config_explosion_members = ComponentObjectGetMembers(projectile_component, "config_explosion") or {}

local PhysicsThrowableComponent = EntityGetFirstComponentIncludingDisabled(entity_id, "PhysicsThrowableComponent")
local t_PhysicsThrowableComponent_members = ComponentGetMembers(PhysicsThrowableComponent)

print(" -\n-\n-\n-\n------------->>>>>>>>>>>>>>>>>>>>>>>>>>>> ENTITY FILENAME = "..EntityGetFilename( entity_id))
PrintTable(t_projectile_component_members, "t_projectile_component_members")
PrintTable(t_config_explosion_members, "t_config_explosion_members")
PrintTable(t_PhysicsThrowableComponent_members, "t_PhysicsThrowableComponent_members")