dofile_once("mods/spells_evolutions/files/create_upgraded_spells.lua")

local entity_id = GetUpdatedEntityID()

local projectile_component = EntityGetFirstComponentIncludingDisabled(entity_id, "ProjectileComponent")

-- enable explosion audio
local t_config_explosion_members = ComponentObjectGetMembers(projectile_component, "config_explosion") or {}
for member, value in pairs(t_config_explosion_members) do
    if tostring(member) == "audio_enabled" then
        ComponentObjectSetValue2(projectile_component, "config_explosion", "audio_enabled", true)
    end
end

-- get my boost_level
local t_variable_storage_components = EntityGetComponent(entity_id, "VariableStorageComponent")
local boost_level
for i, comp in pairs(t_variable_storage_components) do
    if ComponentGetValue2(comp, "name") == "boost_level" then
        boost_level = ComponentGetValue2(comp, "value_int")
    end
end

-- get my param_multiplier
local param_multiplier = GetBoostMultiplier(boost_level)

-- if PhysicsThrowable, let's add some force!
local throw_force_to_add = 1 / best_multiplier -- 1 is really not bad! ^^ 
local max_throw_speed_to_add = 180 / best_multiplier -- 180, it's 2x the default value

local PhysicsImageShapeComponent = EntityGetFirstComponentIncludingDisabled(entity_id, "PhysicsImageShapeComponent")
if PhysicsImageShapeComponent ~= nil then -- if it's a physic projectile

    -- get or create PhysicsThrowableComponent
    local PhysicsThrowableComponent = EntityGetFirstComponentIncludingDisabled(entity_id, "PhysicsThrowableComponent")
    if PhysicsThrowableComponent == nil then -- no PhysicsThrowableComponent? Let's create it!
        local comp = {}
        comp["throw_force_coef"] = 1
        EntityAddComponent2(entity_id, "PhysicsThrowableComponent", comp)
        PhysicsThrowableComponent = EntityGetFirstComponentIncludingDisabled(entity_id, "PhysicsThrowableComponent")
    end

    -- some spells don't have the tag... don't know why
    if ComponentHasTag(PhysicsThrowableComponent, "enabled_in_world") == false then
        ComponentAddTag(PhysicsThrowableComponent, "enabled_in_world")
    end

    -- edit the force value
    local throw_force_coeff = ComponentGetValue2(PhysicsThrowableComponent, "throw_force_coeff")
    local new_throw_force_coeff = throw_force_coeff + (throw_force_to_add * param_multiplier)
    ComponentSetValue2(PhysicsThrowableComponent, "throw_force_coeff", new_throw_force_coeff)

    local max_throw_speed = ComponentGetValue2(PhysicsThrowableComponent, "max_throw_speed")
    local new_max_throw_speed = max_throw_speed + (max_throw_speed_to_add * param_multiplier)
    ComponentSetValue2(PhysicsThrowableComponent, "max_throw_speed", new_max_throw_speed)
end

-- set sprite size!!! xD
local scale_xy_multiplier_max = 2 -- 2 déjà assez fat !!! Sachant que y'a pas de hitbox... plus? c'est chaud!
local t_sprite_components = EntityGetComponentIncludingDisabled(entity_id, "SpriteComponent") or {}
-- "or {}"" for SFM_DG_INFINITE_BOMB_LVL2 for example ! It has no SpriteComponent but a PhysicsImageShapeComponent, that can't be scaled... :'(

for i, sprite_comp in pairs(t_sprite_components) do
    ComponentSetValue2(sprite_comp, "has_special_scale", true)
    local init_special_scale_x = ComponentGetValue2(sprite_comp, "special_scale_x") or 1
    local init_special_scale_y = ComponentGetValue2(sprite_comp, "special_scale_y") or 1

    local new_scale_x = init_special_scale_x +
                            ((init_special_scale_x / best_multiplier * param_multiplier) * (scale_xy_multiplier_max - 1))
    local new_scale_y = init_special_scale_y +
                            ((init_special_scale_y / best_multiplier * param_multiplier) * (scale_xy_multiplier_max - 1))

    ComponentSetValue2(sprite_comp, "special_scale_x", new_scale_x)
    ComponentSetValue2(sprite_comp, "special_scale_y", new_scale_y)
end

-- set boosted "shoot_light_flash_radius"
local shoot_light_flash_radius_add = 500 / best_multiplier * param_multiplier -- 500
-- print(">>>>>>>>>>>>>> shoot_light_flash_radius_add = " .. tostring(shoot_light_flash_radius_add))
local ori_shoot_light_flash_radius = ComponentGetValue2(projectile_component, "shoot_light_flash_radius") or 0
-- print(">>>>>>>>>>>>>> ori_shoot_light_flash_radius = " .. tostring(ori_shoot_light_flash_radius))
local new_shoot_light_flash_radius = ori_shoot_light_flash_radius + shoot_light_flash_radius_add
-- print(">>>>>>>>>>>>>> new_shoot_light_flash_radius = " .. tostring(new_shoot_light_flash_radius))
ComponentSetValue2(projectile_component, "shoot_light_flash_radius", new_shoot_light_flash_radius)
-- print(">>>>>>>>>>>>>>  FINAL shoot_light_flash_radius = " ..
--           tostring(ComponentGetValue2(projectile_component, "shoot_light_flash_radius")))

-- set more particles
local particle_count_add = 20 / best_multiplier * param_multiplier
local particle_vel_multiplier = 1.5 / best_multiplier * param_multiplier
local particle_vel_add = 40 / best_multiplier * param_multiplier
local t_particle_emitter_components = EntityGetComponent(entity_id, "ParticleEmitterComponent") or {}
for i, comp in pairs(t_particle_emitter_components) do
    local ori_count_min = ComponentGetValue2(comp, "count_min") or 0
    local new_count_min = ori_count_min + particle_count_add
    ComponentSetValue2(comp, "count_min", new_count_min)
    -- print(">>>>>>>>>>>>>>  FINAL count_min = " ..
    --       tostring(ComponentGetValue2(comp, "count_min")))

    local ori_count_max = ComponentGetValue2(comp, "count_max") or 0
    local new_count_max = ori_count_max + particle_count_add
    ComponentSetValue2(comp, "count_max", new_count_max)
    -- print(">>>>>>>>>>>>>>  FINAL count_max = " ..
    --       tostring(ComponentGetValue2(comp, "count_max")))

    --
    local ori_x_vel_min = ComponentGetValue2(comp, "x_vel_min") or 0
    local new_x_vel_min = ori_x_vel_min * particle_vel_multiplier - particle_vel_add
    ComponentSetValue2(comp, "x_vel_min", new_x_vel_min)
    -- print(">>>>>>>>>>>>>>  FINAL x_vel_min = " ..
    --       tostring(ComponentGetValue2(comp, "x_vel_min")))

    local ori_x_vel_max = ComponentGetValue2(comp, "x_vel_max") or 0
    local new_x_vel_max = ori_x_vel_max * particle_vel_multiplier + particle_vel_add
    ComponentSetValue2(comp, "x_vel_max", new_x_vel_max)
    -- print(">>>>>>>>>>>>>>  FINAL x_vel_max = " ..
    --       tostring(ComponentGetValue2(comp, "x_vel_max")))

    local ori_y_vel_min = ComponentGetValue2(comp, "y_vel_min") or 0
    local new_y_vel_min = ori_y_vel_min * particle_vel_multiplier - particle_vel_add
    ComponentSetValue2(comp, "y_vel_min", new_y_vel_min)
    -- print(">>>>>>>>>>>>>>  FINAL y_vel_min = " ..
    --       tostring(ComponentGetValue2(comp, "y_vel_min")))

    local ori_y_vel_max = ComponentGetValue2(comp, "y_vel_max") or 0
    local new_y_vel_max = ori_y_vel_max * particle_vel_multiplier + particle_vel_add
    ComponentSetValue2(comp, "y_vel_max", new_y_vel_max)
    -- print(">>>>>>>>>>>>>>  FINAL y_vel_max = " ..
    --       tostring(ComponentGetValue2(comp, "y_vel_max")))

    -- emission
    local ori_emission_interval_min_frames = ComponentGetValue2(comp, "emission_interval_min_frames") or 0
    local new_emission_interval_min_frames = math.max(1, ori_emission_interval_min_frames - (boost_level - 1))
    ComponentSetValue2(comp, "emission_interval_min_frames", new_emission_interval_min_frames)
    -- print(">>>>>>>>>>>>>>  emission_interval_min_frames y_vel_max = " ..
    --   tostring(ComponentGetValue2(comp, "emission_interval_min_frames")))

    local ori_emission_interval_max_frames = ComponentGetValue2(comp, "emission_interval_max_frames") or 0
    local new_emission_interval_max_frames = math.max(1, ori_emission_interval_max_frames - (boost_level - 1))
    ComponentSetValue2(comp, "emission_interval_max_frames", new_emission_interval_max_frames)
    -- print(">>>>>>>>>>>>>>  FINAL emission_interval_min_frames = " ..
    --   tostring(ComponentGetValue2(comp, "emission_interval_min_frames")))

end
