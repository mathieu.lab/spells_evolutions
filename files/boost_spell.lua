-- Keep this Lua file for spells that are already in the world 
-- AND spells that are spawned by the game
-- (so not by my function spawn_loot())
dofile_once("mods/spells_evolutions/files/lib/matolabu_tools.lua")

local entity_id = GetUpdatedEntityID()

if SpellCanBeBoostedOrIsBoosted(entity_id) == true then -- if I'm an infinite projectile ; no need to opti because do once
    local slot_sprite_path = GetSlotSpritePath(entity_id, true)
    local entity_SpriteComponent_item_bg = EntityGetFirstComponentIncludingDisabled(entity_id, "SpriteComponent",
        "item_bg")
    ComponentSetValue2(entity_SpriteComponent_item_bg, "image_file", slot_sprite_path)
end

