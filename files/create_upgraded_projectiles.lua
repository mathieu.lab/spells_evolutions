local nxml = dofile_once("mods/spells_evolutions/files/lib/nxml.lua")
dofile_once("mods/spells_evolutions/files/lib/matolabu_tools.lua")
dofile_once("mods/spells_evolutions/files/create_upgraded_spells.lua")

function UpgradeProjectile(proj_file_text, boost_level, print_spell, new_spell_id, called_inside_myself)

    -- print("-------------------------------------- START UpgradeProjectile() : " .. print_spell.id ..
    --           " -----------------------------------------")

    -- OnlyASpellPrint(print_spell,
    --     "-------------------------------------- START UpgradeProjectile() : " .. print_spell.id ..
    --         "   inside myself? = " .. tostring(called_inside_myself))

    -- INIT some global values
    local init_on_death_explode

    local xml = nxml.parse(proj_file_text)

    local param_multiplier = GetBoostMultiplier(boost_level)

    local ProjectileComponent
    local config_explosion

    -- REF ProjectileComponent, 
    ProjectileComponent = xml:first_of("ProjectileComponent")
    -- -- example of spells that don't have any ProjectileComponent: 
    -- -- -- SUMMON_HOLLOW_EGG
    -- -- -- many of the DG_INFINITE_ spell's projectile

    --
    --
    --
    -- IF NO ProjectileComponent at the root, search inside <Base>
    if not ProjectileComponent then

        local Base = xml:first_of("Base")

        -- IF there's a Base component (all spells in original game + Infinity)
        if Base then

            ProjectileComponent = Base:first_of("ProjectileComponent")

            if ProjectileComponent then
                -- OnlyASpellPrint(print_spell, "-------------- ProjectileComponent found IN BASE")
            end

            -- IF NO ProjectileComponent in Base...
            if not ProjectileComponent then

                -- OnlyASpellPrint(print_spell, "-------------- ProjectileComponent NOT found in Base")

                -- GET ProjectileComponent INSIDE the base file
                local base_file_text

                local base_file_path = Base.attr.file
                -- OnlyASpellPrint(print_spell, " ------->>>>>> base_file_path = " .. base_file_path)
                if base_file_path ~= nil and base_file_path ~= "" then
                    base_file_text = ModTextFileGetContent(base_file_path)
                    -- OnlyASpellPrint(print_spell, "base_file_text is valid = " .. tostring(base_file_text ~= nil))
                end

                if base_file_text then

                    local base_file_xml = nxml.parse(base_file_text)
                    local base_child_ProjectileComponent = base_file_xml:first_of("ProjectileComponent")

                    -- IF we find a ProjectileComponent inside the Base file
                    if base_child_ProjectileComponent then

                        -- COPY ProjectileComponent attributes
                        local base_child_ProjectileComponent_attr = {}
                        for k, v in pairs(base_child_ProjectileComponent.attr) do
                            base_child_ProjectileComponent_attr[k] = v
                        end

                        -- CREATE a copy of ProjectileComponent in Base
                        Base:add_child(nxml.new_element("ProjectileComponent", base_child_ProjectileComponent_attr))

                        -- REF the new ProjectileComponent
                        ProjectileComponent = Base:first_of("ProjectileComponent")

                        -- LET'S DO the same for a potentiel config_explosion inside the ProjectileComponent that's inside the base file
                        -- IF the ProjectileComponent in base file, has a config_explosion
                        local base_child_config_explosion = base_child_ProjectileComponent:first_of("config_explosion")
                        if base_child_config_explosion then

                            -- COPY config_explosion attributes
                            local base_child_config_explosion_attr = {}
                            for k, v in pairs(base_child_config_explosion.attr) do
                                base_child_config_explosion_attr[k] = v
                            end

                            ProjectileComponent:add_child(nxml.new_element("config_explosion",
                                base_child_config_explosion_attr))

                        end
                    end
                end
            end
        end
    end

    --
    --
    --
    -- IF there is no ProjectileComponent anywhere, let's create a simple one
    -- -- the case in Original + Infinity, only for the spell : DG_INFINITE_TENTACLE_PORTAL
    if not ProjectileComponent then
        -- print(">>>>>>>>>>> there is no ProjectileComponent anywhere !!! in = ".. print_spell.id)
        local projectile_component_members = {}
        projectile_component_members["_tags"] = "enabled_in_world"
        -- projectile_component_members["lifetime"] = "1000"
        projectile_component_members["damage"] = "0"
        projectile_component_members["penetrate_entities"] = "1"

        -- ADD ProjectileComponent to xml
        xml:add_child(nxml.new_element("ProjectileComponent", projectile_component_members))

        -- REF ProjectileComponent
        ProjectileComponent = xml:first_of("ProjectileComponent")
    end

    --
    --
    --
    -- REF or CREATE config_explosion
    if ProjectileComponent then
        -- REF config_explosion
        config_explosion = ProjectileComponent:first_of("config_explosion")

        -- IF no config_explosion
        -- CREATE config_explosion
        if not config_explosion then
            local component_members = { -- I tried to put only needed things...
                never_cache = "1",
                explosion_sprite_lifetime = "0",
                explosion_sprite_additive = "1",
                hole_destroy_liquid = "0",
                hole_enabled = "1", --- ???
                ray_energy = "350000", --- dumb copy from another projectile
                particle_effect = "0",
                damage_mortals = "1",
                physics_throw_enabled = "1",
                light_r = "255", -- 255 needed to detect if we have to set the color
                light_g = "255", -- 255 needed to detect if we have to set the color
                light_b = "255", -- 255 needed to detect if we have to set the color
                cell_explosion_radius_min = "5",
                cell_explosion_radius_max = "150",
                cell_explosion_damage_required = "0",
                cell_explosion_velocity_min = "0",
                cell_explosion_power_ragdoll_coeff = "0.75",
                cell_explosion_probability = "0",
                cell_explosion_power = "1"
            }

            component_members["physics_explosion_power.min"] = 0 -- set like that because there's a "."
            component_members["physics_explosion_power.max"] = 0 -- set like that because there's a "."

            ProjectileComponent:add_child(nxml.new_element("config_explosion", component_members))
        end

        config_explosion = ProjectileComponent:first_of("config_explosion")
    end

    --
    --
    --
    --
    --
    --
    --
    ----------------------------------------------------------------------
    ----------------  BOOST ProjectileComponent -------------
    ----------------------------------------------------------------------
    --
    --
    --
    --
    --
    --
    --

    if ProjectileComponent then

        -- _____________________________________ SET speed
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        local action_speed_multiplier_value = 4 / best_multiplier * param_multiplier -- 4

        if not ProjectileComponent.attr.speed_min then -- ex: LIGHTNING, DG_INFINITE_SUMMON_ROCK
            ProjectileComponent.attr.speed_min = 1
        end
        if not ProjectileComponent.attr.speed_max then
            ProjectileComponent.attr.speed_max = 1
        end

        ProjectileComponent.attr.speed_min = ProjectileComponent.attr.speed_min +
                                                 (ProjectileComponent.attr.speed_min * action_speed_multiplier_value)
        ProjectileComponent.attr.speed_max = ProjectileComponent.attr.speed_max +
                                                 (ProjectileComponent.attr.speed_max * action_speed_multiplier_value)

        -- _____________________________________ SET lifetime
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        -- v1 addition
        local action_lifetime_add_value = 5 / best_multiplier * param_multiplier -- 75 -> 10 car sinon ça ken le Luminous Drill par exemple

        -- v2 multiplication
        -- local action_lifetime_add_value = 2 / best_multiplier * param_multiplier -- test 1, donc x2

        if ProjectileComponent.attr.lifetime then
            if ProjectileComponent.attr.lifetime ~= "-1" then -- to avoid instant exploding TNTBOX for example... xD

                -- v1 addition
                ProjectileComponent.attr.lifetime = ProjectileComponent.attr.lifetime + action_lifetime_add_value

                -- v2 multiplication
                -- ProjectileComponent.attr.lifetime = ProjectileComponent.attr.lifetime +
                --                                         (ProjectileComponent.attr.lifetime * action_lifetime_add_value)

            end
        end

        local LifetimeComponent = xml:first_of("LifetimeComponent")
        if LifetimeComponent then
            if LifetimeComponent.attr.lifetime then
                if LifetimeComponent.attr.lifetime ~= "-1" then

                    -- v1 addition
                    LifetimeComponent.attr.lifetime = LifetimeComponent.attr.lifetime + action_lifetime_add_value

                    -- v2 multiplication
                    -- LifetimeComponent.attr.lifetime = LifetimeComponent.attr.lifetime +
                    --                                       (LifetimeComponent.attr.lifetime * action_lifetime_add_value)
                end
            end
        end

        -- _____________________________________ SET spread
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        if ProjectileComponent.attr.direction_random_rad then
            local action_spread_degrees_value = 40 * math.pi / 180 / best_multiplier * param_multiplier -- 40 degrees
            ProjectileComponent.attr.direction_random_rad = math.max(0, ProjectileComponent.attr.direction_random_rad -
                action_spread_degrees_value)
        end

        -- _____________________________________ SET damages
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        -- -- ex: Slow Bullet = 16(init dam + init expl_dam) + 25 (add dam) + 25 (add expl_dam) = 66
        if ProjectileComponent.attr.damage then
            local action_damage_projectile_add_value = 1 / best_multiplier * param_multiplier -- 1 (1 = +25 damages)
            ProjectileComponent.attr.damage = ProjectileComponent.attr.damage + action_damage_projectile_add_value
        end

        -- _____________________________________ ENABLE projectile explode on collision
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        init_on_death_explode = ProjectileComponent.attr.on_death_explode
        ProjectileComponent.attr.on_death_explode = 1
        -- ProjectileComponent.attr.on_lifetime_out_explode= 1 -- too messy for ex:LUMINOUS_DRILL, keep default value

        -- _____________________________________ ENABLE explosion to hit the player
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ProjectileComponent.attr.explosion_dont_damage_shooter = 0

        -- _____________________________________ ENABLE explosion hole the world
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        -- config_explosion.attr.hole_enabled = 1 -- seems not to work 
        -- config_explosion.attr.is_digger = 1 -- seems not to work 

        -- _____________________________________ ENABLE enable explosion sound
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        config_explosion.attr.audio_enabled = 1

        -- _____________________________________ ENABLE shake vegetation
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        config_explosion.attr.shake_vegetation = 1

        -- _____________________________________ SET explosion damages
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        if not config_explosion.attr.damage then
            config_explosion.attr.damage = 0
        end

        local action_damage_explosion_add_value = 1 / best_multiplier * param_multiplier -- 1 (1 = +25 damages)

        -- IF no normal damages, then double explosion damages, to keep a final +50 damages
        local proj_normal_damage = false
        if ProjectileComponent.attr.damage then
            if tonumber(ProjectileComponent.attr.damage) > 0 then
                proj_normal_damage = true
            end
        end
        if proj_normal_damage == false then
            action_damage_explosion_add_value = action_damage_explosion_add_value * 2
        end

        config_explosion.attr.damage = config_explosion.attr.damage + action_damage_explosion_add_value

        -- _____________________________________ SET explosion radius
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        local action_explosion_radius_value = 24 / best_multiplier * param_multiplier -- 30

        -- IF explosion_radius doesn't exist, CREATE it
        if not config_explosion.attr.explosion_radius then
            config_explosion.attr.explosion_radius = 0
        end
        local init_radius = tonumber(config_explosion.attr.explosion_radius) -- otherwise there's a 'string' error

        config_explosion.attr.explosion_radius = init_radius + action_explosion_radius_value

        -- add bonus radius
        if config_explosion.attr.explosion_radius < 30 then
            config_explosion.attr.explosion_radius = config_explosion.attr.explosion_radius + 5
        end

        -- _____________________________________ SET explosion sprite
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        local diameter = config_explosion.attr.explosion_radius * 2

        function SetExplosionSprite(color_string)
            if diameter > 0 and diameter <= 8 then
                config_explosion.attr.explosion_sprite =
                    "mods/spells_evolutions/files/particles/explosions/explosion_008" .. color_string .. ".xml"
            elseif diameter > 8 and diameter <= 14 then
                config_explosion.attr.explosion_sprite =
                    "mods/spells_evolutions/files/particles/explosions/explosion_012" .. color_string .. ".xml"
            elseif diameter > 14 and diameter <= 28 then
                config_explosion.attr.explosion_sprite =
                    "mods/spells_evolutions/files/particles/explosions/explosion_016" .. color_string .. ".xml"
            elseif diameter > 28 and diameter <= 40 then
                config_explosion.attr.explosion_sprite =
                    "mods/spells_evolutions/files/particles/explosions/explosion_032" .. color_string .. ".xml"
            elseif diameter > 40 and diameter <= 56 then
                config_explosion.attr.explosion_sprite =
                    "mods/spells_evolutions/files/particles/explosions/explosion_048" .. color_string .. ".xml"
            elseif diameter > 56 and diameter <= 70 then
                config_explosion.attr.explosion_sprite =
                    "mods/spells_evolutions/files/particles/explosions/explosion_064" .. color_string .. ".xml"
            elseif diameter > 70 and diameter <= 82 then
                config_explosion.attr.explosion_sprite =
                    "mods/spells_evolutions/files/particles/explosions/explosion_076" .. color_string .. ".xml"
            elseif diameter > 82 and diameter <= 94 then
                config_explosion.attr.explosion_sprite =
                    "mods/spells_evolutions/files/particles/explosions/explosion_088" .. color_string .. ".xml"
            elseif diameter > 94 and diameter <= 120 then
                config_explosion.attr.explosion_sprite =
                    "mods/spells_evolutions/files/particles/explosions/explosion_100" .. color_string .. ".xml"
            elseif diameter > 120 then
                config_explosion.attr.explosion_sprite =
                    "mods/spells_evolutions/files/particles/explosions/explosion_128" .. color_string .. ".xml"
            end
        end

        -- _____________________________________ SET colors for: explosion, sparks, lights
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        -- IF we created a config_explosion for this projectile, we set light color. IF NOT, keep default values.
        local config_explosion_is_created_by_me = false
        if tostring(config_explosion.attr.light_r) == "255" and tostring(config_explosion.attr.light_g) == "255" and
            tostring(config_explosion.attr.light_b) == "255" then
            config_explosion_is_created_by_me = true
        end

        -- IF there's no shoot_light_flash color, then set it 
        local need_shoot_light_flash_color = false
        if not ProjectileComponent.attr.shoot_light_flash_r or not ProjectileComponent.attr.shoot_light_flash_g or
            not ProjectileComponent.attr.shoot_light_flash_b then
            need_shoot_light_flash_color = true
        end

        -- ______________________ PURPLE ______________________

        if proj_file_text:find("purple") then
            -- print("I found the word 'purple', so I'll be = PURPLE")
            SetExplosionSprite("_purple")

            local purple_r = "152"
            local purple_g = "89"
            local purple_b = "255"

            if config_explosion_is_created_by_me == true then
                config_explosion.attr.light_r = purple_r
                config_explosion.attr.light_g = purple_g
                config_explosion.attr.light_b = purple_b
            end

            if need_shoot_light_flash_color == true then
                ProjectileComponent.attr.shoot_light_flash_r = purple_r
                ProjectileComponent.attr.shoot_light_flash_g = purple_g
                ProjectileComponent.attr.shoot_light_flash_b = purple_b
            end

            -- sparks color
            if not config_explosion.attr.spark_material then
                config_explosion.attr.spark_material = "spark_purple_bright"
            end

            -- ______________________ BLUE ______________________

        elseif proj_file_text:find("blue") or proj_file_text:find("electric") then
            -- print("I found the word 'blue' or 'electric', so I'll be = BLUE ")
            SetExplosionSprite("_blue")

            local blue_r = "55"
            local blue_g = "196"
            local blue_b = "250"

            if config_explosion_is_created_by_me == true then
                config_explosion.attr.light_r = blue_r
                config_explosion.attr.light_g = blue_g
                config_explosion.attr.light_b = blue_b
            end

            if need_shoot_light_flash_color == true then
                ProjectileComponent.attr.shoot_light_flash_r = blue_r
                ProjectileComponent.attr.shoot_light_flash_g = blue_g
                ProjectileComponent.attr.shoot_light_flash_b = blue_b
            end

            -- sparks color
            if not config_explosion.attr.spark_material then
                config_explosion.attr.spark_material = "plasma_fading"
            end

            -- ______________________ GREEN ______________________

        elseif proj_file_text:find("green") or proj_file_text:find("acid") or proj_file_text:find("slime") then
            -- print("I found the word 'green' or 'acid' or 'slime', so I'll be = GREEN")
            SetExplosionSprite("_green")

            local green_r = "200"
            local green_g = "255"
            local green_b = "0"

            if config_explosion_is_created_by_me == true then
                config_explosion.attr.light_r = green_r
                config_explosion.attr.light_g = green_g
                config_explosion.attr.light_b = green_b
            end

            if need_shoot_light_flash_color == true then
                ProjectileComponent.attr.shoot_light_flash_r = green_r
                ProjectileComponent.attr.shoot_light_flash_g = green_g
                ProjectileComponent.attr.shoot_light_flash_b = green_b
            end

            -- sparks color
            if not config_explosion.attr.spark_material then
                config_explosion.attr.spark_material = "plasma_fading_green"
            end

            -- ______________________ PINK ______________________

        elseif proj_file_text:find("pink") then
            -- print("I found the word 'pink', so I'll be = PINK")
            SetExplosionSprite("_pink")

            local pink_r = "255"
            local pink_g = "91"
            local pink_b = "255"

            if config_explosion_is_created_by_me == true then
                config_explosion.attr.light_r = pink_r
                config_explosion.attr.light_g = pink_g
                config_explosion.attr.light_b = pink_b
            end

            if need_shoot_light_flash_color == true then
                ProjectileComponent.attr.shoot_light_flash_r = pink_r
                ProjectileComponent.attr.shoot_light_flash_g = pink_g
                ProjectileComponent.attr.shoot_light_flash_b = pink_b
            end

            -- sparks color
            if not config_explosion.attr.spark_material then
                config_explosion.attr.spark_material = "plasma_fading_pink"
            end

            -- ______________________ WHITE ______________________

        elseif proj_file_text:find("white") or proj_file_text:find("ice") then
            -- print("I found the word 'white' or 'ice', so I'll be = WHITE ")
            SetExplosionSprite("_white")

            local white_r = "255"
            local white_g = "255"
            local white_b = "255"

            if config_explosion_is_created_by_me == true then
                config_explosion.attr.light_r = white_r
                config_explosion.attr.light_g = white_g
                config_explosion.attr.light_b = white_b
            end

            if need_shoot_light_flash_color == true then
                ProjectileComponent.attr.shoot_light_flash_r = white_r
                ProjectileComponent.attr.shoot_light_flash_g = white_g
                ProjectileComponent.attr.shoot_light_flash_b = white_b
            end

            -- sparks color
            if not config_explosion.attr.spark_material then
                config_explosion.attr.spark_material = "spark_white_bright"
            end

            -- ______________________ RED ______________________

        elseif proj_file_text:find("red") or proj_file_text:find("gunpowder") or proj_file_text:find("flame") then
            -- print("I found the word 'red' or 'gunpowder' or 'flame', so I'll be = RED")
            SetExplosionSprite("_red")

            local red_r = "244"
            local red_g = "17"
            local red_b = "23"

            if config_explosion_is_created_by_me == true then
                config_explosion.attr.light_r = red_r
                config_explosion.attr.light_g = red_g
                config_explosion.attr.light_b = red_b
            end

            if need_shoot_light_flash_color == true then
                ProjectileComponent.attr.shoot_light_flash_r = red_r
                ProjectileComponent.attr.shoot_light_flash_g = red_g
                ProjectileComponent.attr.shoot_light_flash_b = red_b
            end

            -- sparks color
            if not config_explosion.attr.spark_material then
                config_explosion.attr.spark_material = "neon_tube_blood_red"
            end

            -- ______________________ DEFAULT: YELLOW ______________________

            -- IF yellow / radioactive / orange / rock / etc. / or just no color
        else
            -- print("I found NO COLOR, so I'll be = a classic BRIGHT YELLO! :D")
            SetExplosionSprite("")

            local yellow_r = "255"
            local yellow_g = "205"
            local yellow_b = "6"

            if config_explosion_is_created_by_me == true then
                config_explosion.attr.light_r = yellow_r
                config_explosion.attr.light_g = yellow_g
                config_explosion.attr.light_b = yellow_b
            end

            if need_shoot_light_flash_color == true then
                ProjectileComponent.attr.shoot_light_flash_r = yellow_r
                ProjectileComponent.attr.shoot_light_flash_g = yellow_g
                ProjectileComponent.attr.shoot_light_flash_b = yellow_b
            end

            -- sparks color
            if not config_explosion.attr.spark_material then
                config_explosion.attr.spark_material = "spark_yellow"
                -- config_explosion.attr.spark_material = "spark"
            end

        end

        -- _____________________________________ SET lights
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        -- radius of light flash when shoot 
        local shoot_light_flash_radius_add = 500 / best_multiplier * param_multiplier -- 500
        if not ProjectileComponent.attr.shoot_light_flash_radius then
            ProjectileComponent.attr.shoot_light_flash_radius = 0
        end
        ProjectileComponent.attr.shoot_light_flash_radius = ProjectileComponent.attr.shoot_light_flash_radius +
                                                                shoot_light_flash_radius_add

        -- explosion light
        config_explosion.attr.light_enabled = 1
        config_explosion.attr.light_fade_time = 0.25 -- just for doing a flash that stay a little longer after the explosion, like a retinal remanence        
        config_explosion.attr.light_radius_coeff = diameter / 5 -- /10 seems to be a little bigger than the explosion with a gradient

        -- color tester --
        -- config_explosion.attr.light_r = "0"
        -- config_explosion.attr.light_g = "255"
        -- config_explosion.attr.light_b = "0"

        -- _____________________________________ SET camera shake
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        -- when shot
        local screenshake_multiplier = 10 -- 10
        local action_screenshake_value = screenshake_multiplier / best_multiplier * param_multiplier / 2 -- /2 because the shot's ss is lighter

        if not ProjectileComponent.attr.camera_shake_when_shot then
            ProjectileComponent.attr.camera_shake_when_shot = 0
        end

        ProjectileComponent.attr.camera_shake_when_shot = ProjectileComponent.attr.camera_shake_when_shot +
                                                              action_screenshake_value

        if not config_explosion.attr.camera_shake then
            config_explosion.attr.camera_shake = 0
        end

        config_explosion.attr.camera_shake = screenshake_multiplier / best_multiplier * param_multiplier

        -- _____________________________________ SET sparks
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        config_explosion.attr.sparks_enabled = 1
        -- OnlyASpellPrint(print_spell, " ---->>>> config_explosion.attr.sparks_enabled = ".. tostring(config_explosion.attr.sparks_enabled))

        local ori_count_min = 0
        local ori_count_max = 0

        -- SET count min

        if config_explosion.attr.sparks_count_min ~= nil then
            ori_count_min = config_explosion.attr.sparks_count_min
        end
        -- OnlyASpellPrint(print_spell, " ---->>>> config_explosion.attr.sparks_count_min = ".. tostring(config_explosion.attr.sparks_count_min))
        -- OnlyASpellPrint(print_spell, " ---->>>> diameter = ".. tostring(diameter))

        -- SET count max
        if config_explosion.attr.sparks_count_max ~= nil then
            ori_count_max = config_explosion.attr.sparks_count_max
        end
        -- OnlyASpellPrint(print_spell, " ---->>>> config_explosion.attr.sparks_count_max = ".. tostring(config_explosion.attr.sparks_count_max))

        local new_sparks_count_min = math.ceil(ori_count_min + (diameter / 6)) -- gap of -2
        -- OnlyASpellPrint(print_spell, " ---->>>> new_sparks_count_min = ".. tostring(new_sparks_count_min))

        local new_sparks_count_max =  math.ceil(ori_count_max + (diameter / 4))
        -- OnlyASpellPrint(print_spell, " ---->>>> new_sparks_count_max = ".. tostring(new_sparks_count_max))

        -- DEBUG crash of SLOW_BULLET, seems to crash when min is > max..
        if new_sparks_count_min > new_sparks_count_max then
            new_sparks_count_min, new_sparks_count_max = new_sparks_count_max, new_sparks_count_min
        end
        -- OnlyASpellPrint(print_spell, " ---->>>> ROUND 2")
        -- OnlyASpellPrint(print_spell, " ---->>>> new_sparks_count_min = ".. tostring(new_sparks_count_min))
        -- OnlyASpellPrint(print_spell, " ---->>>> new_sparks_count_max = ".. tostring(new_sparks_count_max))

        new_sparks_count_min = new_sparks_count_min + 1
        new_sparks_count_max =  new_sparks_count_max + 1
        -- OnlyASpellPrint(print_spell, " ---->>>> ROUND 3!")
        -- OnlyASpellPrint(print_spell, " ---->>>> new_sparks_count_min = ".. tostring(new_sparks_count_min))
        -- OnlyASpellPrint(print_spell, " ---->>>> new_sparks_count_max = ".. tostring(new_sparks_count_max))

        config_explosion.attr.sparks_count_min = new_sparks_count_min
        config_explosion.attr.sparks_count_max = new_sparks_count_max

        -- _____________________________________ SET cell (fire particles that fill the explosion radius)
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        if not config_explosion.attr.create_cell_probability then
            config_explosion.attr.create_cell_probability = 0
        end
        config_explosion.attr.create_cell_probability = config_explosion.attr.create_cell_probability + diameter / 8 -- fire particles flying in all the explosion_radius

        config_explosion.attr.cell_explosion_damage_required = "0"

        -- IF NO create_cell_material, then init it with "fire"
        local need_create_cell_material = fasle
        if config_explosion.attr.create_cell_material then
            if config_explosion.attr.create_cell_material == "" then
                need_create_cell_material = true
            end
        else -- create_cell_material does not exist
            need_create_cell_material = true
        end
        if need_create_cell_material == true then
            config_explosion.attr.create_cell_material = "fire"
        end

        -- _____________________________________ SET material sparks (piece of world that is convert to sparks)
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        config_explosion.attr.material_sparks_enabled = 1

        if not config_explosion.attr.material_sparks_probability then
            config_explosion.attr.material_sparks_probability = 0
        end
        config_explosion.attr.material_sparks_probability =
            config_explosion.attr.material_sparks_probability + diameter / 8

        if not config_explosion.attr.material_sparks_count_min then
            config_explosion.attr.material_sparks_count_min = 0
        end
        config_explosion.attr.material_sparks_count_min = config_explosion.attr.material_sparks_count_min + diameter /
                                                              16

        if not config_explosion.attr.material_sparks_count_max then
            config_explosion.attr.material_sparks_count_max = 0
        end
        config_explosion.attr.material_sparks_count_max = config_explosion.attr.material_sparks_count_max + diameter / 8

        -- _____________________________________ SET stains (black burnt spot on the floor)
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        -- config_explosion.attr.stains_enabled = 1

        -- if not config_explosion.attr.stains_radius then
        --     config_explosion.attr.stains_radius = 0
        -- end
        -- config_explosion.attr.stains_radius = config_explosion.attr.stains_radius + diameter / 2

        -- _____________________________________ SET physics_explosion_power
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        local physics_explosion_power_min = "physics_explosion_power.min"
        local physics_explosion_power_max = "physics_explosion_power.max"

        local exp_damage = config_explosion.attr.damage
        config_explosion.attr[physics_explosion_power_min] = exp_damage * 3.5
        config_explosion.attr[physics_explosion_power_max] = exp_damage * 4

        -- _____________________________________ SET knockback forces
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        local action_knockback_force_value = 5 / best_multiplier * param_multiplier -- 5

        -- in ProjectileComponent
        if not ProjectileComponent.attr.knockback_force then
            ProjectileComponent.attr.knockback_force = 0
        end
        ProjectileComponent.attr.knockback_force = ProjectileComponent.attr.knockback_force +
                                                       action_knockback_force_value

        -- in config_explosion
        if not config_explosion.attr.knockback_force then
            config_explosion.attr.knockback_force = 0
        end
        config_explosion.attr.knockback_force = config_explosion.attr.knockback_force + action_knockback_force_value

        -- _____________________________________ SET critical damage
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        local action_damage_critical_chance_value = 10 / best_multiplier -- 10

        -- _____________________________________ SET more particles!
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        local particle_count_add = 20 / best_multiplier * param_multiplier
        local particle_vel_multiplier = 1.5 / best_multiplier * param_multiplier
        local particle_vel_add = 40 / best_multiplier * param_multiplier

        local t_particle_emitter_components = xml:all_of("ParticleEmitterComponent") or {}
        for i, comp in pairs(t_particle_emitter_components) do

            -- SET count_min
            local ori_count_min = comp.attr.count_min or 0
            comp.attr.count_min = ori_count_min + particle_count_add

            -- SET count_max
            local ori_count_max = comp.attr.count_max or 0
            comp.attr.count_max = ori_count_max + particle_count_add

            -- SET x_vel_min
            local ori_x_vel_min = comp.attr.x_vel_min or 0
            comp.attr.x_vel_min = ori_x_vel_min * particle_vel_multiplier - particle_vel_add

            -- SET x_vel_max
            local ori_x_vel_max = comp.attr.x_vel_max or 0
            comp.attr.x_vel_max = ori_x_vel_max * particle_vel_multiplier + particle_vel_add

            -- SET y_vel_min
            local ori_y_vel_min = comp.attr.y_vel_min or 0
            comp.attr.y_vel_min = ori_y_vel_min * particle_vel_multiplier - particle_vel_add

            -- SET y_vel_max
            local ori_y_vel_max = comp.attr.y_vel_max or 0
            comp.attr.y_vel_max = ori_y_vel_max * particle_vel_multiplier + particle_vel_add

            -- SET emission_interval_min_frames
            local ori_emission_interval_min_frames = comp.attr.emission_interval_min_frames or 0
            comp.attr.emission_interval_min_frames = math.max(1, ori_emission_interval_min_frames - (boost_level - 1))

            -- SET emission_interval_max_frames
            local ori_emission_interval_max_frames = comp.attr.emission_interval_max_frames or 0
            comp.attr.emission_interval_max_frames = math.max(1, ori_emission_interval_max_frames - (boost_level - 1))
        end

        -- _____________________________________ BOOST loaded entity
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        -- IF I'm not already inside the loaded_entity (function-ception, lol)
        -- I do that because, if the loaded_entity has another loaded_entity inside
        -- the "new_path" of this one, will be the same.
        -- I don't have idea to differenciate them! ..... for now

        if called_inside_myself == false then

            -- OnlyASpellPrint(print_spell, "-------------- called_inside_myself == false")

            local loaded_entity_path = config_explosion.attr.load_this_entity

            -- IF load_this_entity exist
            if loaded_entity_path then

                -- OnlyASpellPrint(print_spell, "-------------- loaded_entity_path = " .. loaded_entity_path)

                local t_paths = {}

                -- IF there's multiple loaded entities
                local path_has_commas = string.find(loaded_entity_path, ",")
                if path_has_commas then

                    -- OnlyASpellPrint(print_spell, ">>>>>>>>>>>>>>> path_has_commas")
                    -- OnlyASpellPrint(print_spell, ">>>>>>>>>>>>>>> loaded_entity_path = " .. loaded_entity_path)

                    -- WHILE there's at least one path in the string "loaded_entity_path"
                    while loaded_entity_path:find(".xml") do

                        local a_path = string.match(loaded_entity_path, "(.-)%.xml")
                        -- OnlyASpellPrint(print_spell, ">>>>>>>>>>>>>>> a_path = " .. a_path)
                        local a_path_with_dot_xml = a_path .. ".xml"
                        -- OnlyASpellPrint(print_spell, ">>>>>>>>>>>>>>> a_path_with_dot_xml = " .. a_path_with_dot_xml)

                        -- CLEAN the path, removing every "," or space at the beginning
                        -- local a_path_clean = string.match(a_path_with_dot_xml, "%,-%s-(.+)")
                        local a_path_clean = a_path_with_dot_xml
                        while string.sub(a_path_clean, 1, 1) == "," or string.sub(a_path_clean, 1, 1) == " " do
                            a_path_clean = string.sub(a_path_clean, 2)
                        end
                        -- OnlyASpellPrint(print_spell, ">>>>>>>>>>>>>>> a_path_clean = " .. a_path_clean)

                        table.insert(t_paths, a_path_clean)

                        -- REMOVE the found path
                        loaded_entity_path = string.gsub(loaded_entity_path, a_path_with_dot_xml, "")
                        -- OnlyASpellPrint(print_spell, ">>>>>>>>>>>>>>> NEW loaded_entity_path = " .. loaded_entity_path)

                        -- LOOP
                    end

                    -- IF there's only one loaded entity
                else
                    table.insert(t_paths, loaded_entity_path)
                end

                -- FOR each loaded entity path
                for i, current_path in pairs(t_paths) do

                    -- OnlyASpellPrint(print_spell,
                    --     "-------------- loaded_entity_path i = " .. tostring(i) .. "  path = " .. current_path)

                    -- LOAD file text
                    local loaded_entity_text
                    if current_path ~= nil and current_path ~= "" then
                        loaded_entity_text = ModTextFileGetContent(current_path)
                    end

                    -- OnlyASpellPrint(print_spell,
                    --     "-------------- loaded_entity_text = " .. tostring(loaded_entity_text ~= nil))

                    -- IF the path correspond to a real xml file
                    if loaded_entity_text and loaded_entity_text ~= "" then

                        -- OnlyASpellPrint(print_spell, "-------------- loaded_entity_text = DONE")
                        -- print(">>>> spell = ".. print_spell.id .. "  have a loaded entity = " .. loaded_entity_path)

                        -- CREATE boosted loaded_entity
                        local new_loaded_entity_text = UpgradeProjectile(loaded_entity_text, boost_level, print_spell,
                            new_spell_id, true)

                        -- CREATE file
                        local new_path = "mods/spells_evolutions/files/new_load_ent_" .. print_spell.id .. "_proj" ..
                                             tostring(new_spell_id) .. "_i" .. tostring(i) .. ".xml"
                        ModTextFileSetContent(new_path, new_loaded_entity_text)

                        -- OnlyASpellPrint(print_spell, "-------------- new_path =   " .. new_path)

                        -- REPLACE loaded_entity to the new boosted version
                        config_explosion.attr.load_this_entity = new_path

                    end
                end

            end
        end

        --------------------------------------------------------------------------------- DECORTICAGE de ProjectileComponent

        -- NO NEED
        -- damage_critical
        -- ProjectileComponent.attr.physics_impulse_coeff = "0" -- Trop puissant
        -- create_shell_casing
        -- shell_casing_material
        -- shell_casing_offset
        -- play_damage_sounds
        -- ProjectileComponent.attr.hit_particle_force_multiplier="0.25" -- ???

        --------------------------------------------------------------------------------- DECORTICAGE de config_explosion

        -- NO NEED
        -- damage_critical
        -- config_explosion.attr.background_lightning_count = 100 -- eclairs de ouf dans le background en extérieur xD

        -------------------------- FIND colors in projectile file

        -- local t_finds = StringFindAll(proj_file_text, "pink") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' pink ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "purple") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' purple ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "green") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' green ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "blue") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' blue ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "red") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' red ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "yellow") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' yellow ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "orange") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' orange ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "ice") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' ice ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "electric") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' electric ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "slime") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' slime ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "acid") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' acid ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "toxic") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' toxic ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "gunpowder") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' gunpowder ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "radioactive") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' radioactive ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "lava") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' lava ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "fuse") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' fuse ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "grass") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' grass ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "wood") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' wood ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "rock") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' rock ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "concrete") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' concrete ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "gold") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' gold ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "white") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' white ', number of time = "..#t_finds)
        -- end

        -- local t_finds = StringFindAll(proj_file_text, "flame") or {}
        -- if #t_finds > 0 then
        --     print("Found the word ' flame ', number of time = "..#t_finds)
        -- end 

    end

    --
    --
    --
    --
    --
    --
    --
    --
    ----------------------------------------------------------------------------------
    ----------------  BOOST PhysicsThrowableComponent -------------
    ----------------------------------------------------------------------------------
    --
    --

    -- REF PhysicsThrowableComponent
    local PhysicsThrowableComponent = xml:first_of("PhysicsThrowableComponent")

    if PhysicsThrowableComponent then
        -- OnlyASpellPrint(print_spell, "-------------- PhysicsThrowableComponent found IN THE PROJ")
    end

    -- IF no PhysicsThrowableComponent
    if not PhysicsThrowableComponent then
        -- OnlyASpellPrint(print_spell, "-------------- PhysicsThrowableComponent NOT found in the proj")

        -- SEARCH in Base
        local Base = xml:first_of("Base")

        -- IF there's a Base component (all spells in original game + Infinity)
        if Base then

            PhysicsThrowableComponent = Base:first_of("PhysicsThrowableComponent")

            if PhysicsThrowableComponent then
                -- OnlyASpellPrint(print_spell, "-------------- PhysicsThrowableComponent found IN BASE")
            end

            -- IF NO PhysicsThrowableComponent in Base...
            if not PhysicsThrowableComponent then

                -- OnlyASpellPrint(print_spell, "-------------- PhysicsThrowableComponent NOT found in Base")

                -- GET PhysicsThrowableComponent INSIDE the base file
                local base_file_path = Base.attr.file
                local base_file_text = ModTextFileGetContent(base_file_path)
                if base_file_text then

                    local base_file_xml = nxml.parse(base_file_text)
                    local base_child_PhysicsThrowableComponent = base_file_xml:first_of("PhysicsThrowableComponent")

                    -- IF we find a PhysicsThrowableComponent inside the Base file
                    if base_child_PhysicsThrowableComponent then

                        -- COPY PhysicsThrowableComponent attributes
                        local base_child_PhysicsThrowableComponent_attr = {}
                        for k, v in pairs(base_child_PhysicsThrowableComponent.attr) do
                            base_child_PhysicsThrowableComponent_attr[k] = v
                        end

                        -- CREATE a copy of PhysicsThrowableComponent in Base
                        Base:add_child(nxml.new_element("PhysicsThrowableComponent",
                            base_child_PhysicsThrowableComponent_attr))

                        -- REF the new ProjectileComponent
                        PhysicsThrowableComponent = Base:first_of("PhysicsThrowableComponent")

                    end
                end
            end
        end
    end

    -- IF we find a PhysicsThrowableComponent in projectile file or in base file
    if PhysicsThrowableComponent then
        -- print(print_spell.id .. "   ~~~~~~~~~~>   Projectile has : a PhysicsThrowableComponent!")

        -- _____________________________________ ADD tag "enabled_in_world" if if doesn'rt already have it
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        -- IF no tag at all, create the attribute!
        if not PhysicsThrowableComponent.attr._tags then
            PhysicsThrowableComponent.attr._tags = ""
        end

        -- GET tags
        local the_tags = PhysicsThrowableComponent.attr._tags

        -- IF there's some tag
        if the_tags then
            local tags_contain_enabled_in_world = string.match(the_tags, "enabled_in_world")
            -- print("tags_contain_enabled_in_world = "..tostring(tags_contain_enabled_in_world))

            if not tags_contain_enabled_in_world then

                -- IF tags field is empty
                if the_tags == "" then
                    -- print("_tags is an empty filed, let's put: enabled_in_world")
                    PhysicsThrowableComponent.attr._tags = "enabled_in_world"

                    -- IF there's already some other tags
                else
                    -- print("_tags already contain = ".. the_tags.. "   ; so we add enabled_in_world at the end!")
                    PhysicsThrowableComponent.attr._tags = tostring(PhysicsThrowableComponent.attr._tags) ..
                                                               ",enabled_in_world"
                end
            end
        end

        -- _____________________________________ EDIT the force value
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        local throw_force_to_add = 1 / best_multiplier * param_multiplier -- 1 is really not bad! ^^ 
        local max_throw_speed_to_add = 180 / best_multiplier * param_multiplier -- 180, it's 2x, because 180 seems to be the default value

        local init_throw_force_coeff = PhysicsThrowableComponent.attr.throw_force_coeff

        if init_throw_force_coeff then
            -- OnlyASpellPrint(print_spell, "-------------- throw_force_coeff found")
        end

        -- REF throw_force_coeff
        if not init_throw_force_coeff then
            init_throw_force_coeff = 1
        end

        -- SET throw_force_coeff
        PhysicsThrowableComponent.attr.throw_force_coeff = init_throw_force_coeff + throw_force_to_add

        -- /!\
        -- Don't set max_throw_speed because it KILLS the default value (set nowhere)
        -- /!\

        -- REF max_throw_speed
        local init_max_throw_speed = PhysicsThrowableComponent.attr.max_throw_speed

        if init_max_throw_speed then
            -- OnlyASpellPrint(print_spell, "-------------- max_throw_speed found")
        end

        if not init_max_throw_speed then
            init_max_throw_speed = 180 -- seems to be the hidden default value
        end
        -- SET max_throw_speed
        PhysicsThrowableComponent.attr.max_throw_speed = init_max_throw_speed + max_throw_speed_to_add

    end

    --
    --
    --
    -- _____________________________________ SET sprite size!!! xD
    -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    local t_sprite_components = xml:all_of("SpriteComponent") or {}
    -- "or {}"" for SFM_DG_INFINITE_BOMB_LVL2 for example ! It has no SpriteComponent but a PhysicsImageShapeComponent, that can't be scaled... :'(

    local scale_xy_multiplier_max = 1.5 / best_multiplier * param_multiplier -- put it here to be used by radius too
    -- donc en gros 2, ça fait qu'au niveau max le scale est multiplié par 3, car scale = scale + (scale x (maximum)2)

    -- IF there's at least one SpriteComponent
    if t_sprite_components then

        -- FOR each SpriteComponent
        for i, sprite_comp in pairs(t_sprite_components) do

            sprite_comp.attr.has_special_scale = "1"
            -- ComponentSetValue2(sprite_comp, "has_special_scale", true)

            local init_special_scale_x = sprite_comp.attr.special_scale_x or 1
            local init_special_scale_y = sprite_comp.attr.special_scale_y or 1

            local new_scale_x = init_special_scale_x + (init_special_scale_x * scale_xy_multiplier_max)
            local new_scale_y = init_special_scale_y + (init_special_scale_y * scale_xy_multiplier_max)

            sprite_comp.attr.special_scale_x = new_scale_x
            sprite_comp.attr.special_scale_y = new_scale_y

        end
    end

    local CollisionTriggerComponent = xml:first_of("CollisionTriggerComponent")
    if CollisionTriggerComponent then
        if CollisionTriggerComponent.attr.radius then
            CollisionTriggerComponent.attr.radius = CollisionTriggerComponent.attr.radius +
                                                        (CollisionTriggerComponent.attr.radius * scale_xy_multiplier_max)
        end
        if CollisionTriggerComponent.attr.width then
            CollisionTriggerComponent.attr.width = CollisionTriggerComponent.attr.width +
                                                       (CollisionTriggerComponent.attr.width * scale_xy_multiplier_max /
                                                           4)
        end
        if CollisionTriggerComponent.attr.height then
            CollisionTriggerComponent.attr.height = CollisionTriggerComponent.attr.height +
                                                        (CollisionTriggerComponent.attr.height * scale_xy_multiplier_max /
                                                            4)
        end

    end

    --
    --
    --
    --
    --
    --
    --
    --
    ----------------------------------------------------------------------------------
    ----------------  BOOST MaterialSeaSpawnerComponent -------------
    ----------------------------------------------------------------------------------
    --
    --
    -- REF MaterialSeaSpawnerComponent
    local MaterialSeaSpawnerComponent = xml:first_of("MaterialSeaSpawnerComponent")

    if MaterialSeaSpawnerComponent then
        -- OnlyASpellPrint(print_spell, "-------------- MaterialSeaSpawnerComponent found IN THE PROJ")
        -- print("------------------- found a MaterialSeaSpawnerComponent in the PROJ = " .. print_spell.id ..
        --           "  and I'm in the loaded_entity = " .. tostring(called_inside_myself))
    end

    -- IF no MaterialSeaSpawnerComponent
    if not MaterialSeaSpawnerComponent then
        -- OnlyASpellPrint(print_spell, "-------------- MaterialSeaSpawnerComponent NOT found in the proj")

        -- SEARCH in Base
        local Base = xml:first_of("Base")

        -- IF there's a Base component (all spells in original game + Infinity)
        if Base then

            MaterialSeaSpawnerComponent = Base:first_of("MaterialSeaSpawnerComponent")

            if MaterialSeaSpawnerComponent then
                -- OnlyASpellPrint(print_spell, "-------------- MaterialSeaSpawnerComponent found IN BASE")
                -- print("------------------- found a MaterialSeaSpawnerComponent in the BASE =   " .. print_spell.id ..
                --           "   and I'm in the loaded_entity = " .. tostring(called_inside_myself))

            end

            -- IF NO MaterialSeaSpawnerComponent in Base...
            if not MaterialSeaSpawnerComponent then

                -- OnlyASpellPrint(print_spell, "-------------- MaterialSeaSpawnerComponent NOT found in Base")

                -- GET MaterialSeaSpawnerComponent INSIDE the base file
                local base_file_path = Base.attr.file
                local base_file_text = ModTextFileGetContent(base_file_path)
                if base_file_text then

                    local base_file_xml = nxml.parse(base_file_text)
                    local base_child_MaterialSeaSpawnerComponent = base_file_xml:first_of("MaterialSeaSpawnerComponent")

                    -- IF we find a MaterialSeaSpawnerComponent inside the Base file
                    if base_child_MaterialSeaSpawnerComponent then

                        -- COPY MaterialSeaSpawnerComponent attributes
                        local base_child_MaterialSeaSpawnerComponent_attr = {}
                        for k, v in pairs(base_child_MaterialSeaSpawnerComponent.attr) do
                            base_child_MaterialSeaSpawnerComponent_attr[k] = v
                        end

                        -- CREATE a copy of MaterialSeaSpawnerComponent in Base
                        Base:add_child(nxml.new_element("MaterialSeaSpawnerComponent",
                            base_child_MaterialSeaSpawnerComponent_attr))

                        -- REF the new ProjectileComponent
                        MaterialSeaSpawnerComponent = Base:first_of("MaterialSeaSpawnerComponent")

                    end
                end
            end
        end
    end

    --
    if MaterialSeaSpawnerComponent then

        -- _____________________________________ SET size
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        local size_multiplier = 1 / best_multiplier * param_multiplier -- 1, so x2

        local init_size_x = MaterialSeaSpawnerComponent.attr["size.x"]
        if init_size_x then
            local size_x = "size.x"
            MaterialSeaSpawnerComponent.attr[size_x] = init_size_x + (init_size_x * size_multiplier)
        end

        local init_size_y = MaterialSeaSpawnerComponent.attr["size.y"]
        if init_size_y then
            local size_y = "size.y"
            MaterialSeaSpawnerComponent.attr[size_y] = init_size_y + (init_size_y * size_multiplier)
        end

        -- _____________________________________ REINIT on_death_explode
        -- ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        -- so if the proj initialy doesn't explode, we keep this particularity (avoid an explosion in the middle of nothing)

        if ProjectileComponent then
            if init_on_death_explode then
                ProjectileComponent.attr.on_death_explode = init_on_death_explode
            else
                ProjectileComponent.attr.on_death_explode = 0
            end
        end

    end

    --
    --
    --
    --
    --
    local new_proj_file_text = nxml.tostring(xml)
    -- OnlyASpellPrint(print_spell, new_proj_file_text)
    return new_proj_file_text
end
