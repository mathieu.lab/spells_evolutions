dofile_once("mods/spells_evolutions/files/lib/matolabu_tools.lua")
dofile_once("data/scripts/gun/gun_enums.lua")

-- dofile_once("data/scripts/gun/gun_actions.lua") -- or just "dofile" ? 

reading_name = true -- makes it work with Evaisa Enchantment mod
dofile("data/scripts/gun/gun_actions.lua") -- makes it work with Evaisa Enchantment mod

best_multiplier = 16 -- global

-- get the spell.id prefix + spell.name prefixes
dofile_once("mods/spells_evolutions/files/config.lua")

-- old  "SFM", 
-- or "SSE" Spells Super Evolution
-- "MSE" Make Spells Evolve
-- Evolve Your Spells EYS
-- BYS Boost Your Spells
-- Spells Evolution SE
-- EVO (et puis après on vera quel nom on mettra pour le mod... xD)

function GetBoostMultiplier(boost_level)
    local param_mutiplier
    if boost_level == 2 then
        param_mutiplier = 2
    elseif boost_level == 3 then
        param_mutiplier = 4
    elseif boost_level == 4 then
        param_mutiplier = 8
    elseif boost_level == 5 then
        param_mutiplier = best_multiplier
    end
    return param_mutiplier
end

function CreateUpgradedSpells()

    ---------------------------- SPAWN A CERTAIN NUMBER OF SPELLS ------------------------------
    -- local do_n = 0
    ------------------------------------------------------------------------------------------------------------------

    -- get all infinite projectile spells
    local t_infinite_projectiles = GetAllInfiniteProjectileSpells() or {}

    --[[ -- TEST de la technique de Evaisa pour récupérer les informations de la fonction.
    dofile("data/scripts/gun/gun_actions.lua")
    for k, v in pairs (actions) do
        if v.id == "INFESTATION" then
            print(" ================> id = " .. tostring(v.id))
            print(" ================> action = " .. tostring(v.action))

            dofile("data/scripts/gun/gun_enums.lua")
            dofile("data/scripts/gun/gun_actions.lua")

            dofile("data/scripts/gun/gun.lua")
            dofile("data/scripts/gun/gunaction_generated.lua")
            dofile("data/scripts/gun/gunshoteffects_generated.lua")
            local shot_effects = {}
            local c = {}
            ConfigGunActionInfo_Init(c)
            ConfigGunShotEffects_Init(shot_effects)

            PrintTable(c, "c")
            PrintTable(shot_effects, "shot_effects")
            -- PrintTable(v.action, "v.action") -- MARCHE PAS !!!! lol xD

            -- local text_gun_actions_lua = ModTextFileGetContent("data/scripts/gun/gun_actions.lua")
            -- print("-- \n-- \n-- \n-- \n-- \n-- \n-- \n-- \ntext_gun_actions_lua = \n" .. text_gun_actions_lua )
        end
    end
 ]]

    -- GET all the Lua files that contain spell definitions, in all the mods loaded before!
    -- print(
    --     "------------------------------------Lua files that contain spell definitions, in all the mods loaded before!------------------------------------------")
    local t_all_mods_actions_files_paths = {}
    table.insert(t_all_mods_actions_files_paths, 1, "data/scripts/gun/gun_actions.lua")

    local t_active_mods_ids = ModGetActiveModIDs()

    -- PrintTable(t_active_mods_ids)

    for k, v in pairs(t_active_mods_ids) do

        -- IF the mod is "Procedural Projectiles"
        if v == "spell_randomizer" then
            --- HARDCODE: add the definition file of the generated spells of "Procedural Projectiles" mod (by Evaisa)
            table.insert(t_all_mods_actions_files_paths, "data/generated_actions_file.lua") -- for Procedural Projectiles

            -- IF the mod is "Goki's Things"
        elseif v == "gkbrkn_noita" then
            -- do nothing for now => c'est dans son actions.lua, mais il utilise "generate_action_entry"... il me NIQUE LA GUEULE!!!

            -- IF the mod is "Megumin Explosion Spell"
        elseif v == "megumin_e" then
            local megumin_alt = ModSettingGet("megumin_e.EXPLOSION_ALT")
            -- print("========>>>>>>>>>>>>>>>> ModSettingGet(\"megumin_e.EXPLOSION_ALT\") = " .. tostring(megumin_alt))
            -- IF it's normal version of the explosion
            if megumin_alt ~= true then
                table.insert(t_all_mods_actions_files_paths, "mods/megumin_e/files/actions.lua")
                -- IF it's alternate version of the explosion
            elseif megumin_alt == true then
                table.insert(t_all_mods_actions_files_paths, "mods/megumin_e/files/actions_alt.lua")
            end

        else
            -- Can be optimised using string.gmatch
            local path_of_mod_init_file = "mods/" .. v .. "/init.lua"
            -- print("--> path_of_mod_init_file = " .. path_of_mod_init_file)
            local text_mod_init = ModTextFileGetContent(path_of_mod_init_file)
            if text_mod_init ~= nil then
                local text_mod_init_no_space = string.gsub(text_mod_init, "%s+", "") --- USELESS NOW, TO DELETE
                -- local string_to_find = "ModLuaFileAppend%s*%(%s*\"data/scripts/gun/gun_actions.lua\"%s*,"
                local string_to_find =
                    "ModLuaFileAppend%s*%(%s*\"data/scripts/gun/gun_actions.lua\"%s*,%s*\"(.-)\"%s*%)"
                -- print("--> string_to_find = ".. string_to_find)

                local t_actions_files_path = string.gmatch(text_mod_init, string_to_find)
                for actions_file_path in t_actions_files_path do
                    -- print("MATCH! --> Lua file that contain spell definitions = " .. actions_file_path)
                    -- print("actions_file_path ~= (...) upgraded_spells.lua =" ..
                    --           tostring(actions_file_path ~= "mods/spells_evolutions/files/upgraded_spells.lua"))
                    if actions_file_path ~= "mods/spells_evolutions/files/upgraded_spells.lua" then

                        -- test if the file exist, because sometimes it's just old commentaries
                        local action_file_path_get_content = ModTextFileGetContent(actions_file_path)
                        -- print("action_file_path_get_content ~= nil ?" .. tostring(action_file_path_get_content ~= nil ))
                        if action_file_path_get_content ~= nil then
                            -- print("===> Save this table ! :D \n----------------------------------------------------")
                            table.insert(t_all_mods_actions_files_paths, actions_file_path)
                        end
                    end
                end
            end
        end

    end

    -- PrintTable(t_all_mods_actions_files_paths, "t_all_mods_actions_files_paths")

    -- here is the string variable, in which we will create all the spells texts
    local text_all_new_actions = ""

    -- here is the string variable, in which we will create all the spells texts that we didn't find the translated name yet
    local text_all_new_actions_translation_not_found = ""

    -- =========================================================
    -- =========================================================
    -- =========================================================
    -- =========================================================
    -- =========================================================
    -- =========================================================
    --
    -- let's parse all Infinite + Projectile spells of "actions" table
    --
    -- =========================================================
    -- =========================================================
    -- =========================================================
    -- =========================================================
    -- =========================================================
    -- =========================================================

    local new_spell_id = 1 -- used to create dynamically new unique upgraded version of projectiles
    local t_ref_super_projs = {}
    local t_ref_hyper_projs = {}
    local t_ref_ultra_projs = {}
    local t_ref_legendary_projs = {}

    for i, spell in pairs(t_infinite_projectiles) do

        -- print(" >>>>>>>>>>> spell id=" .. spell.id)
        -- OnlyASpellPrint(spell, " >>>>>>>>>>> spell id=" .. spell.id)

        -- if do_n < 100000 then
        -- do_n = do_n + 1

        local spell_id = spell.id

        -- print("---------->>>>>>>>>>>> spell_id = ".. tostring(spell_id))

        local spell_is_dg_infinite = string.match(spell.id, "DG_INFINITE_(.+)")

        -- IF it's a "Infinity" spell
        if spell_is_dg_infinite ~= nil then

            -- GET the original spell
            spell_id = string.match(spell.id, "DG_INFINITE_(.+)")
            -- print("du coup on ne prend pas son spell id, mais celui de l'original qui est = ".. spell_id)
            -- print("---------->>>>>>>>>>>> NEEEEWWWW spell_id = ".. tostring(spell_id))
        end
        local string_to_find = "id%s*%=%s*\"" .. spell_id .. "\""

        ---------------------------------------------------------
        -- get the file in which the spell is defined
        ---------------------------------------------------------
        local text_actions_file
        for i, file_path in pairs(t_all_mods_actions_files_paths) do

            local text_current_actions_file = ModTextFileGetContent(file_path)
            -- print("---------->>>>>>>>>>>> text_current_actions_file = ".. tostring(text_current_actions_file ~= nil))

            local spell_is_in_file = string.find(text_current_actions_file, string_to_find)

            if spell_is_in_file then
                text_actions_file = text_current_actions_file
                -- print("create_upgraded_spells.lua ---> the inf. proj. spell : " .. spell.id ..
                --           "   is founded in the file: " .. file_path)
                -- OnlyASpellPrint(spell, "create_upgraded_spells.lua ---> the inf. proj. spell : " .. spell.id ..
                --     "   is founded in the file: " .. file_path)

                break -- BREAK: get the file in which the spell is defined
            end
        end

        -- IF we didn't find the spell in a definition file...
        if not text_actions_file then
            -- too bad, we don't create the advanced spells... 
            -- print("f not text_actions_file then          -- too bad, we don't create the advanced spells... ")
            -- So we just do nothing (and not BREAK!!!) -- v1.1

            -- IF we found the spell in a file, let's do some general things before creating spells! 
        else
            --------------------------------
            -- get action's function
            --------------------------------
            -- on prend sprite car ID est parfois ailleurs
            -- in the case of "Infinity" spells, it will take the original version of the spell! perfect ^^ 
            local spell_id_start_index = string.find(text_actions_file, string_to_find)
            -- OnlyASpellPrint(spell, ">> spell_id_start_index = " .. tostring(spell_id_start_index))

            local text_before_spell_id = string.sub(text_actions_file, 1, spell_id_start_index)
            -- OnlyASpellPrint(spell, ">> text_before_spell_id = " .. tostring(text_before_spell_id))

            local all_before_spell = string.match(text_before_spell_id, "(.+)%{")
            -- OnlyASpellPrint(spell, ">> all_before_spell = " .. tostring(all_before_spell))

            local last_curly_bracket_index = string.len(all_before_spell)
            -- OnlyASpellPrint(spell, ">> last_curly_bracket_index = " .. tostring(last_curly_bracket_index))

            local all_from_the_spell = string.sub(text_actions_file, last_curly_bracket_index)
            -- OnlyASpellPrint(spell, ">> all_from_the_spell = " .. tostring(all_from_the_spell))

            local text_spell = string.match(all_from_the_spell, "%b{}")

            -- OnlyASpellPrint(spell, ">> text_spell = " .. text_spell)
            -- print("create_upgraded_spells.lua ---> text_spell = \n" .. text_spell)

            local text_function_intro = string.match(text_spell, "function%s*%b()") -- useful at the end

            local text_spell_after_function = string.match(text_spell, "function%s*%b()%s*(.*)")
            local text_function_without_end = string.match(text_spell_after_function, "(.*)end")
            local text_function_without_end = "\n " .. text_function_without_end -- to respect the "RULE"

            -- ==================================
            -- create the four new versions of the spell!
            -- ==================================

            -- OnlyASpellPrint(spell,
            --     " >>>>>>>>>>>>>>>>>>  create the four new versions of the spell!  >>>>>>>>>>>>>>>>>>>>> spell.id=" ..
            --         spell.id)

            -- Y'a surement des trucs à Opti qu'on peut sortir du "for" qui suit, pour ne les faire qu'une fois....
            -- mais bon comme c'est au chargement, osef un peu !!!
            -- Y'a surement des trucs à Opti qu'on peut sortir du "for" qui suit, pour ne les faire qu'une fois....
            -- mais bon comme c'est au chargement, osef un peu !!!
            -- Y'a surement des trucs à Opti qu'on peut sortir du "for" qui suit, pour ne les faire qu'une fois....
            -- mais bon comme c'est au chargement, osef un peu !!!
            -- Y'a surement des trucs à Opti qu'on peut sortir du "for" qui suit, pour ne les faire qu'une fois....
            -- mais bon comme c'est au chargement, osef un peu !!!
            -- Y'a surement des trucs à Opti qu'on peut sortir du "for" qui suit, pour ne les faire qu'une fois....
            -- mais bon comme c'est au chargement, osef un peu !!!
            -- Y'a surement des trucs à Opti qu'on peut sortir du "for" qui suit, pour ne les faire qu'une fois....
            -- mais bon comme c'est au chargement, osef un peu !!!
            -- Y'a surement des trucs à Opti qu'on peut sortir du "for" qui suit, pour ne les faire qu'une fois....
            -- mais bon comme c'est au chargement, osef un peu !!!
            -- Y'a surement des trucs à Opti qu'on peut sortir du "for" qui suit, pour ne les faire qu'une fois....
            -- mais bon comme c'est au chargement, osef un peu !!!

            local no_translation_or_translation_found = true

            -- print(("--->> spell = " .. tostring(spell.id)))
            -- print(("--->>>>>> spell.description = " .. spell.description))
            -- print(("--->>>>>> spell.description TEXT = " .. tostring(GameTextGet(spell.description))))

            -- CREATE Spells lvl 2, 3, 4, 5
            for boost_level = 2, 5, 1 do

                -- OnlyASpellPrint(spell, ">>>>>>>>>> for boost_level = " .. tostring(boost_level))

                local param_mutiplier = GetBoostMultiplier(boost_level)

                local text_intro = "table.insert(actions, \n	{ \n"

                -----------------------
                -- CREATE "id"
                -----------------------

                local text_id = "id = \"" .. spell_id_prefix .. "_" .. spell.id .. "_LVL" .. boost_level .. "\", \n"

                --------------------------
                -- CREATE "name"
                --------------------------

                -- CHOOSE name suffix depending of boost level
                local name_prefix = GetNamePrefix(boost_level)

                local text_name = ""

                if spell.name ~= nil then
                    local get_name_translated_or_not

                    -- IF DG_INFINITE
                    -- if (string.sub(spell.id, 1, 11) == "DG_INFINITE") then
                    --     no_translation_or_translation_found = false
                    -- end

                    -- IF "$"
                    if (string.sub(spell.name, 1, 1) == "$") then
                        get_name_translated_or_not = GameTextGet(spell.name)

                        if get_name_translated_or_not ~= nil then -- translation found! (e.g. for Noita's original spells)
                            -- Yeay ! 

                        else -- we haven't managed to get the translation yet. 
                            no_translation_or_translation_found = false -- -- We'll save this spell in another file for now (secu, because it's false by default)

                            get_name_translated_or_not = spell.name
                        end

                        -- IF no "$"
                    else
                        get_name_translated_or_not = spell.name
                    end

                    text_name = "name = \"" .. name_prefix .. " " .. get_name_translated_or_not .. "\", \n"
                end

                -----------------------------------
                -- CREATE "description"
                -----------------------------------

                -- V3
                local text_description = ""

                if spell.description ~= nil then

                    -- REPLACE potential "\n" by "\\n" to write "\n" at the end in the final text... fiouuuuuuuuuuuuuuu xD
                    local clean_description = string.gsub(spell.description, "\n", "\\n")
                    text_description = "description = \"" .. clean_description .. "\", \n"

                    -- Even if we do not modify the description,
                    -- we still check if the translation is loaded (if there is any) 
                    -- to see if the translation loading system is operational or not.
                    local get_description_translated_or_not = ""

                    -- IF "$", a translation is called
                    if (string.sub(spell.description, 1, 1) == "$") then

                        get_description_translated_or_not = GameTextGet(spell.description)

                        -- IF no translation found yet...
                        if get_description_translated_or_not == "" or nil then
                            no_translation_or_translation_found = false
                            -- we have to retry to find the description translation later in the other process
                        end
                    end
                end

                --
                --
                --
                --
                -- V2
                -- local text_description = ""

                -- if spell.description ~= nil then

                --     local get_description_translated_or_not = ""
                --     local clean_description = ""

                --     -- IF "$", a translation is called
                --     if (string.sub(spell.description, 1, 1) == "$") then

                --         get_description_translated_or_not = GameTextGet(spell.description)

                --         -- IF no translation found yet...
                --         if get_description_translated_or_not == "" or nil then
                --             no_translation_or_translation_found = false -- we have to retry to find the description translation later in the other process
                --             clean_description = string.gsub(spell.description, "\n", "\\n")

                --             -- IF translation found! 
                --         else
                --             clean_description = string.gsub(get_description_translated_or_not, "\n", "\\n")
                --             -- REPLACE potential "\n" by "\\n" to write "\n" at the end in the final text... fiouuuuuuuuuuuuuuu xD
                --         end

                --         -- IF no "$"" and not nil
                --     else
                --         clean_description = string.gsub(spell.description, "\n", "\\n")
                --     end

                --     text_description = "description = \"" .. clean_description .. "\", \n"
                -- end

                --
                --
                --
                --
                -- V1
                -- local text_description = ""

                -- if spell.description ~= nil then

                --     -- REPLACE potential "\n" by "\\n" to write "\n" at the end in the final text... fiouuuuuuuuuuuuuuu xD
                --     local clean_description = string.gsub(spell.description, "\n", "\\n")

                --     text_description = "description = \"" .. clean_description .. "\", \n"
                -- end

                -----------------------------------
                -- sprite
                -----------------------------------

                local text_sprite = ""
                if spell.sprite ~= nil then
                    text_sprite = "sprite = \"" .. spell.sprite .. "\", \n"
                end

                -----------------------------------
                -- sprite_unidentified
                -----------------------------------

                local text_sprite_unidentified = ""
                if spell.sprite_unidentified ~= nil then
                    text_sprite_unidentified = "sprite_unidentified = \"" .. spell.sprite_unidentified .. "\", \n"
                end

                -----------------------------------
                -- type
                -----------------------------------

                local text_type = "type = ACTION_TYPE_PROJECTILE, \n"

                -- print(">>>>>> text_name = " .. text_name)

                -----------------------------------
                -- spawn_level
                -----------------------------------

                local text_spawn_level = ""
                if spell.spawn_level ~= nil then
                    text_spawn_level = "spawn_level = \"" .. spell.spawn_level .. "\", \n"
                end

                -- print(">>>>>> text_spawn_level = " .. text_spawn_level)

                -----------------------------------
                -- spawn_probability
                -----------------------------------

                local text_spawn_probability = ""
                if spell.spawn_level ~= nil then

                    local text_spawn_level_to_edit = spell.spawn_level
                    -- print(">>>>>> text_spawn_level_to_edit = " .. text_spawn_level_to_edit)

                    local pattern = "%d+"
                    local repl = "0"
                    text_spawn_level_to_edit = string.gsub(text_spawn_level_to_edit, pattern, repl)
                    -- print(">>>>>> text_spawn_level_to_edit AFTER digit = " .. text_spawn_level_to_edit)

                    text_spawn_probability = "spawn_probability = \"" .. text_spawn_level_to_edit .. "\", \n"
                end

                -- print(">>>>>> text_spawn_probability = " .. text_spawn_probability)

                -----------------------------------
                -- price
                -----------------------------------

                local text_price = ""
                -- GET price
                local price_init = spell.price or 0

                -- old
                -- local price_added_max = 75 / best_multiplier -- +50 damages   ,   1 damage = 1,5 gold   ,   soit 75
                -- local final_price = price_init + math.ceil(price_added_max * param_mutiplier) -- linéaire

                -- new
                if price_init == 0 then
                    price_init = 100 -- LIGHT_BULLET price
                end
                local final_price = price_init * param_mutiplier -- purement proportionnel
                text_price = "price = " .. tostring(final_price) .. ", \n"

                -- print(">>>>>> text_price = "..text_price)

                -- no "spawn_level" (for now)
                -- no "spawn_probability" (for now)
                -- no "price" (for now)

                -----------------------------------
                -- mana
                -----------------------------------

                local text_mana = ""
                local mana_init = spell.mana or 0
                local mana_added_max = 109 / best_multiplier -- 154 au lvl5, simply the dumb sum of all bonus added... xD not a bad number btw!
                local final_mana = mana_init + (mana_added_max * (param_mutiplier ^ 1.125)) -- courbe non-linéaire
                text_mana = "mana = " .. tostring(final_mana) .. ", \n"

                -----------------------------------
                -- max_uses
                -----------------------------------

                local text_max_uses = ""
                -- OnlyASpellPrint(spell, ">>>>>> spell.max_uses = ".. tostring(spell.max_uses))
                if spell.max_uses ~= nil then
                    text_max_uses = "max_uses = " .. spell.max_uses .. ", \n"
                    -- else -- Useful ? 
                    --     text_max_uses = "max_uses = -1, \n"
                end
                -- OnlyASpellPrint(spell, ">>>>>> text_max_uses = ".. text_max_uses)

                -----------------------------------
                -- custom_xml_file
                -----------------------------------

                local text_custom_xml_file = ""
                if spell.custom_xml_file ~= nil then
                    text_custom_xml_file = "custom_xml_file = \"" .. spell.custom_xml_file .. "\", \n"
                end

                -----------------------------
                -- prepare action text
                ----------------------------- 

                local text_extra_entities = ""
                local text_action_speed_multiplier = ""
                local text_action_fire_rate_wait = ""
                local text_action_current_reload_time = ""
                local text_action_lifetime_add = ""
                local text_action_spread_degrees = ""
                local text_action_damage_projectile_add = ""
                local text_action_damage_explosion_add = ""
                local text_action_explosion_radius = ""
                local text_action_damage_critical_chance = ""
                local text_action_shot_effects_recoil_knockback = ""
                local text_action_knockback_force = ""
                local text_action_screenshake = ""

                local new_text_function_without_end = text_function_without_end

                ---/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ---/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ---/////////////////////////////////////////////// Projectiles replacement ////////////////////////////////////////////////////
                ---/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ---/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                -- SECU COPY the table to avoid overriding the original! :O
                local t_related_projectiles
                if spell.related_projectiles ~= nil then
                    t_related_projectiles = CopyTable(spell.related_projectiles)
                end

                -- IF spell is "3_DOLLAR_BILL" from Binding of Noita => ADD special steps
                if spell.id == "3_DOLLAR_BILL" then

                    -- LOAD the table of projectiles of the spell "3_DOLLAR_BILL"
                    local t_spell_proj_types = {"brimstone", "lance", "laser", "bouncy_orb", "tentacle", "rubber_ball",
                                                "slime", "arrow", "fireball_ray", "bullet_heavy", "light_bullet",
                                                "bullet", "light_bullet_blue", "bubbleshot"}

                    -- CREATE the new table that will be filled with corresponding new projectiles
                    local t_new_spell_proj_types = {}

                    -- LOAD the table with the projectiles of the current boost_level
                    local t_ref_projs = {}
                    if boost_level == 2 then
                        t_ref_projs = t_ref_super_projs
                    elseif boost_level == 3 then
                        t_ref_projs = t_ref_hyper_projs
                    elseif boost_level == 4 then
                        t_ref_projs = t_ref_ultra_projs
                    elseif boost_level == 5 then
                        t_ref_projs = t_ref_legendary_projs
                    end

                    -- FOR each projectiles of the table of the spell "3_DOLLAR_BILL"
                    for i, proj_name in pairs(t_spell_proj_types) do

                        -- IF projectile is "brimstone"
                        if proj_name == "brimstone" then
                            table.insert(t_new_spell_proj_types, "brimstone")
                            -- do nothing, because the brimstone projectiles are boosted like the others below

                            -- IF projectile is "fireball_ray"
                        elseif proj_name == "fireball_ray" then

                            -- CREATE the new projectile file 
                            local proj_file_text = ModTextFileGetContent(
                                "data/entities/projectiles/deck/fireball_ray.xml")
                            -- no test because it's a vanilla spell
                            dofile_once("mods/spells_evolutions/files/create_upgraded_projectiles.lua")
                            local new_proj_file_text = UpgradeProjectile(proj_file_text, boost_level, spell,
                                new_spell_id, false)
                            local new_path = "mods/spells_evolutions/files/new_proj_" .. tostring(new_spell_id) ..
                                                 ".xml"
                            ModTextFileSetContent(new_path, new_proj_file_text)

                            -- HARDCOPY!!! REF correspondence between old proj and new proj
                            if boost_level == 2 then
                                t_ref_super_projs["data/entities/projectiles/deck/fireball_ray.xml"] = new_path
                            elseif boost_level == 3 then
                                t_ref_hyper_projs["data/entities/projectiles/deck/fireball_ray.xml"] = new_path
                            elseif boost_level == 4 then
                                t_ref_ultra_projs["data/entities/projectiles/deck/fireball_ray.xml"] = new_path
                            elseif boost_level == 5 then
                                t_ref_legendary_projs["data/entities/projectiles/deck/fireball_ray.xml"] = new_path
                            end

                            -- CONSTRUCT the new projectile name
                            local new_spell_proj_name = "new_proj_" .. tostring(new_spell_id)

                            -- ID++
                            new_spell_id = new_spell_id + 1

                            -- INSERT this new name in the table
                            table.insert(t_new_spell_proj_types, new_spell_proj_name)

                            -- IF it's any other projectile
                        else
                            -- OnlyASpellPrint(spell, " ON CHERCHE = " .. proj_name)

                            -- SEARCH the projectile in the table of projectiles already boosted
                            for k, v in pairs(t_ref_projs) do
                                local _, ref_proj_name = string.match(k, "(.+)%/(.-).xml")
                                -- OnlyASpellPrint(spell, " --------->>>>>>>>>>> proj_name = "..ref_proj_name)
                                if ref_proj_name == proj_name then
                                    -- OnlyASpellPrint(spell, " --------->>>>>>>>>>> proj_name FOUNDED = " .. ref_proj_name)

                                    local _, name_of_boosted_proj = string.match(v, "(.+)%/(.-).xml")

                                    -- OnlyASpellPrint(spell,
                                    --     " --------->>>>>>>>>>> name_of_boosted_proj CORRESPONDING = " ..
                                    --         name_of_boosted_proj)

                                    -- INSERT this new name in the table
                                    table.insert(t_new_spell_proj_types, name_of_boosted_proj)

                                    break
                                end
                            end
                        end
                    end -- END OF:   FOR each projectiles of the table of the spell "3_DOLLAR_BILL"

                    -- PrintTable(t_new_spell_proj_types, "t_new_spell_proj_types")

                    -- CREATE the string of the new table
                    local string_of_new_types = "{"
                    for k, v in pairs(t_new_spell_proj_types) do
                        string_of_new_types = string_of_new_types .. "\"" .. v .. "\""
                        if k < #t_new_spell_proj_types then
                            string_of_new_types = string_of_new_types .. ","
                        end
                    end
                    string_of_new_types = string_of_new_types .. "}"
                    -- OnlyASpellPrint(spell, " --------->>>>>>>>>>> string_of_new_types = " .. string_of_new_types)

                    local string_pattern_table_of_types = string.match(new_text_function_without_end,
                        "local types = (.-)SetRandomSeed")
                    -- OnlyASpellPrint(spell, " --------->>>>>>>>>>> string_pattern_table_of_types = " ..
                    --     string_pattern_table_of_types)

                    -- REPLACE the table of the spell, by the new table
                    new_text_function_without_end = string.gsub(new_text_function_without_end,
                        string_pattern_table_of_types, string_of_new_types .. "\n")

                    -- REPLACE the defajult projectile's filepath, by the one of the boosted proj
                    new_text_function_without_end = string.gsub(new_text_function_without_end,
                        "data/entities/projectiles/deck/", "mods/spells_evolutions/files/")

                    -- IF spell is not "3_DOLLAR_BILL" = normal code
                end

                -- ____OLD attempts to find projectile file in function
                -- local text_function_without_end = string.gsub(text_function_without_end, "%s+", "")
                -- local string_to_find = "add_projectile(.-)\"(.-)\"%s-%)"
                -- local string_to_find = "add_projectile(.-)%(\"(.-).xml\"" -- problème : prend la merde de LOAD_AND_SHOOT
                -- local string_to_find = "add_projectil[(e)(e_trigger_hit_world)(e_trigger_timer)(e_trigger_death)](\"(.-)%.xml\"%)"
                -- local string_to_find = "add_(.-)ectile%g*%(\"(.-).xml\"" -- problème : j'ai pas les plusieurs projectiles d'un même spell

                local string_to_find = "add_proj%g-%s-%(%s-\"(.-).xml\"" -- c'est good ! --> Chelou, le "." marche sans écrire "%."
                -- example of string found : add_projectile_trigger_timer("data/entities/projectiles/deck/bullet_slow.xml"

                -- GET all strings, corresponding to "strings_to_find" in the function text
                local _, _, t_founded_addprojs = StringFindAll(new_text_function_without_end, string_to_find)

                -- /!\ /!\ /!\ 
                -- Be careful, when the table is read by the "for", the entries do not necessarily come out in the right order...
                -- /!\ /!\ /!\ 

                -- IF we didn't find any path to a projectile in the spell function...
                if #t_founded_addprojs == 0 then
                    -- OnlyASpellPrint(spell, " /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ NO SPELL CREATION, for = " .. spell.id)

                    -- Then no upgraded spell creation... BREAK!
                    break -- BREAK: for boost_level = 2, 5, 1 do
                end

                -- OnlyASpellPrintTable(spell, t_founded_addprojs, "t_founded_addprojs")

                -- print(
                --     " >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Upgraded Spell Creation for spell.id=" ..
                --         spell.id)

                -- REPLACE found paths
                for i, current_founded_addproj in pairs(t_founded_addprojs) do
                    -- OnlyASpellPrint(spell, ">>>>>>>>>> i = " .. tostring(i))
                    -- OnlyASpellPrint(spell, ">>>>>>>>>> current_string = " .. current_founded_addproj)
                    local not_needed_text, string_path = string.match(current_founded_addproj, "(.-)\"(.*)\"")
                    -- OnlyASpellPrint(spell, ">>>>>>>>>> not_needed_text = " .. not_needed_text)
                    -- OnlyASpellPrint(spell, ">>>>>>>>>> string_path = " .. string_path)

                    -- IF the projectile is the VFX of Meugmin "Explosion!"
                    if string_path == "mods/megumin_e/files/actions/m_explosion_target.xml" then
                        -- do nothing, or it will break the vfx. In any case there's no way to scale it, so we keep the original.
                    else

                        -- WHILE start
                        -- -- IF there's at least one match at another place in the function
                        -- -- --> necessarily "true" the first time, not necessarily the following times (when i>1)
                        local match_found = false
                        -- GET all matches in the function text

                        -- CLEAN string_path in case of finding specials signs --->    ()%[]+-
                        -- -- The case with "Binding-of-Noita"... mzfk!!!
                        local string_path_pattern_matching_proof = PrepareStringForPatternMatching(string_path)

                        for match in string.gmatch(new_text_function_without_end, string_path_pattern_matching_proof) do
                            match_found = true
                            -- at least one match, don't need more, break!
                            break -- BREAK: for match in string.gmatch(new_text_function_without_end, string_path) do
                        end
                        -- so IF there's still at least one match in the function
                        if match_found == true then
                            -- OnlyASpellPrint(spell, "match found in function text")

                            ---------------------------------------------------
                            -- CREATE the upgraded projectile
                            ---------------------------------------------------
                            -- OnlyASpellPrint(spell, ">>>>>>>>>> CREATE the upgraded projectile")

                            -- LOAD the original projectile
                            -- OnlyASpellPrint(spell, "string_path = " .. string_path)
                            local proj_file_text = ModTextFileGetContent(string_path) or nil
                            -- OnlyASpellPrint(spell, "proj_file_text is valid = " .. tostring(proj_file_text ~= nil))

                            -- OnlyASpellPrint(spell, ">>>>>>>>>> proj_file_text = " .. proj_file_text)
                            if proj_file_text == nil or proj_file_text == "" then
                                -- OnlyASpellPrint(spell,
                                --     ">>>>>>>>>> This proj_file_text DOES NOT EXIST, LIKE THE CLIMATE !!! = " ..
                                --         tostring(proj_file_text))
                            end

                            -- IF the file exist 
                            -- --> NEEDED to avoid catching "add_projectiles" that are in comments! --> Like in "More Loadout" mod
                            if proj_file_text ~= nil and proj_file_text ~= "" then -- v1.2

                                -- OnlyASpellPrint(spell, ">>>>>>>>>> This proj_file_text EXIST!, let's do the shits.")

                                dofile_once("mods/spells_evolutions/files/create_upgraded_projectiles.lua")
                                local new_proj_file_text = UpgradeProjectile(proj_file_text, boost_level, spell,
                                    new_spell_id, false)

                                -- OnlyASpellPrint(spell, ">>>> new_proj_file_text = \n" .. new_proj_file_text)

                                local new_path_extension = ".xml"

                                -- KEEP extension of projectiles variants from MDYG
                                if string.find(string_path, "NEM_power_balls_c") then
                                    local _, rank = string.match(string_path, "(.+)_c(.-).xml")
                                    new_path_extension = "_c" .. rank .. new_path_extension
                                end

                                -- CREATE new path
                                local new_path = "mods/spells_evolutions/files/new_proj_" .. tostring(new_spell_id) ..
                                                     new_path_extension

                                new_spell_id = new_spell_id + 1
                                -- OnlyASpellPrint(spell, ">>>>>>>>>> new_path = " .. new_path)

                                -- REF correspondence between old proj and new proj (for spells like "3_DOLLAR_BILL")
                                if boost_level == 2 then
                                    t_ref_super_projs[string_path] = new_path
                                elseif boost_level == 3 then
                                    t_ref_hyper_projs[string_path] = new_path
                                elseif boost_level == 4 then
                                    t_ref_ultra_projs[string_path] = new_path
                                elseif boost_level == 5 then
                                    t_ref_legendary_projs[string_path] = new_path
                                end

                                -- CREATE a new projectile file with the text of the UpgradedProjectile!
                                ModTextFileSetContent(new_path, new_proj_file_text)

                                -- GET all  matches in the current new function text
                                -- OnlyASpellPrint(spell, "GET all  matches in the current new function text")
                                for match in string.gmatch(new_text_function_without_end,
                                    string_path_pattern_matching_proof) do
                                    -- print(">>>>>>>>>> match = "..match)
                                    -- OnlyASpellPrint(spell, ">>>>>>>>>> match = " .. match)
                                    -- IF there's still matches, do something, if not, do nothing.

                                    local match_pattern_matching_proof = PrepareStringForPatternMatching(match)

                                    -- REPLACE all occurences of the string_path, by the new_path of the upgraded projectile
                                    new_text_function_without_end =
                                        string.gsub(new_text_function_without_end, match_pattern_matching_proof,
                                            new_path)

                                    -- REPLACE also the same related_projectile, if there's some
                                    if t_related_projectiles ~= nil then
                                        -- OnlyASpellPrint(spell,"t_related_projectiles ~= nil")
                                        for j, related_projectile in pairs(t_related_projectiles) do
                                            -- OnlyASpellPrint(spell, tostring(j) .. "   =   ".. related_projectile)
                                            if related_projectile == match then -- don't need "match_pattern_matching_proof" because "=="  takes the string as it is.
                                                -- OnlyASpellPrint(spell,"related_projectile == match")
                                                t_related_projectiles[j] = new_path
                                                -- OnlyASpellPrint(spell,"t_related_projectiles[j] = ".. t_related_projectiles[j])
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                end

                --
                --
                --

                -- CHANGE fire rate wait of all spells launched at the same time
                local action_fire_rate_wait_value = 5 / best_multiplier -- 5 (10 =  "RECHARGE" spell)
                text_action_fire_rate_wait = "\n c.fire_rate_wait = c.fire_rate_wait - " ..
                                                 tostring(action_fire_rate_wait_value * param_mutiplier)

                -- ADD reload time to wand
                local action_current_reload_time_value = 10 / best_multiplier -- 10 (20 = "RECHARGE" spell)
                text_action_current_reload_time = "\n current_reload_time = current_reload_time - " ..
                                                      tostring(action_current_reload_time_value * param_mutiplier)

                -- SET critical chance
                local action_damage_critical_chance_value = 10 / best_multiplier -- 10
                text_action_damage_critical_chance = "\n c.damage_critical_chance = c.damage_critical_chance + " ..
                                                         tostring(action_damage_critical_chance_value * param_mutiplier)

                -- SET knock back myself
                local action_shot_effects_recoil_knockback_value = 50 / best_multiplier -- 100 -> 50
                text_action_shot_effects_recoil_knockback =
                    "\n shot_effects.recoil_knockback = shot_effects.recoil_knockback + " ..
                        tostring(action_shot_effects_recoil_knockback_value * param_mutiplier)

                --
                --
                --
                -- MERGE ALL TEXTS
                -- -- RULE: all but "text_function_intro" have a "\n " at the begining of the string!
                local text_action_content =
                    text_function_intro .. text_extra_entities .. new_text_function_without_end ..
                        text_action_speed_multiplier .. text_action_fire_rate_wait .. text_action_current_reload_time ..
                        text_action_lifetime_add .. text_action_spread_degrees .. text_action_damage_projectile_add ..
                        text_action_damage_explosion_add .. text_action_explosion_radius ..
                        text_action_damage_critical_chance .. text_action_shot_effects_recoil_knockback ..
                        text_action_knockback_force .. text_action_screenshake .. "\n end,"

                local text_action = "action = " .. text_action_content .. "\n"

                local text_outro = [[}
                                                    )
                                                    
                                                    ]]

                -- CREATE text for related_projectiles field
                local text_related_projectiles = ""
                local text_all_related_projectiles = ""

                -- IF the field "related_projectile" exist
                if t_related_projectiles then

                    -- IF there's at least 1 related projectile
                    if t_related_projectiles[1] then
                        text_all_related_projectiles = "\"" .. t_related_projectiles[1] .. "\""

                        -- IF there is more than 1 relative projectile
                        if #t_related_projectiles > 1 then
                            local i = 2
                            while #t_related_projectiles >= i do
                                -- ADD the related projectile to the text
                                text_all_related_projectiles =
                                    text_all_related_projectiles .. ", \"" .. t_related_projectiles[i] .. "\""
                                i = i + 1
                            end
                        end
                    end

                    text_related_projectiles = "related_projectiles = {" .. text_all_related_projectiles .. "}, \n"
                end

                local text_one_new_action = text_intro .. text_id .. text_name .. text_description .. text_sprite ..
                                                text_sprite_unidentified .. text_related_projectiles .. text_type ..
                                                text_spawn_level .. text_spawn_probability .. text_price .. text_mana ..
                                                text_max_uses .. text_custom_xml_file .. text_action .. text_outro

                -- save the spell, completed or not, in the new spells file
                -- it is necessary to make the evolution system and the UI work
                -- because gun_actions.lua must be updated at the end of this function.
                text_all_new_actions = text_all_new_actions .. text_one_new_action

                -- NEW translation update system
                -- create a new fil containing spells for which no translation has been found
                --
                -- IF no clean text for "name" or "description"
                if no_translation_or_translation_found == false then

                    text_intro = "{ \n"
                    text_outro = "}, \n"

                    text_one_new_action = text_intro .. text_id .. text_name .. text_description .. text_sprite ..
                                              text_sprite_unidentified .. text_related_projectiles .. text_type ..
                                              text_spawn_level .. text_spawn_probability .. text_price .. text_mana ..
                                              text_max_uses .. text_custom_xml_file .. text_action .. text_outro

                    text_all_new_actions_translation_not_found =
                        text_all_new_actions_translation_not_found .. text_one_new_action
                end

            end
            -- print(">>>>>>no_translation_or_translation_found = " .. tostring(no_translation_or_translation_found))

        end
        -- IF "do_n >= the iteration limit"
        -- else 
        --     break -- BREAK: for i, spell in pairs(t_infinite_projectiles) do
        -- end
    end

    -- PrintTable(t_ref_super_projs, "t_ref_super_projs")

    -- CREATE a new actions file

    ModTextFileSetContent("mods/spells_evolutions/files/upgraded_spells.lua", text_all_new_actions)

    text_all_new_actions_translation_not_found = "evo_notrans_actions =\n{\n" ..
                                                     text_all_new_actions_translation_not_found .. "}"
    ModTextFileSetContent("mods/spells_evolutions/files/upgraded_spells_translation_not_found.lua",
        text_all_new_actions_translation_not_found)

    -- APPEND the new actions file (that contain fully completed spells) with the Noita's one
    ModLuaFileAppend("data/scripts/gun/gun_actions.lua", "mods/spells_evolutions/files/upgraded_spells.lua")

    local all_actions = text_all_new_actions .. text_all_new_actions_translation_not_found

    -- print("----------------\n----------------\n----------------\n----------------")
    -- print("--------START--------")
    -- print("--------text_all_new_actions--------")
    -- print("----------------\n----------------\n----------------\n----------------")
    -- print(text_all_new_actions)
    -- print("----------------\n----------------\n----------------\n----------------")
    -- print("--------text_all_new_actions--------")
    -- print("--------END--------")
    -- print("----------------\n----------------\n----------------\n----------------")

    -- print("~~~~~~~~~~\n~~~~~~~~~~\n~~~~~~~~~~\n~~~~~~~~~~")
    -- print("~~~~~~~~~~START~~~~~~~~~~")
    -- print("~~~~~~~~~~text_all_new_actions_translation_not_found~~~~~~~~~~")
    -- print("~~~~~~~~~~\n~~~~~~~~~~\n~~~~~~~~~~\n~~~~~~~~~~")
    -- print(text_all_new_actions_translation_not_found)
    -- print("~~~~~~~~~~\n~~~~~~~~~~\n~~~~~~~~~~\n~~~~~~~~~~")
    -- print("~~~~~~~~~~text_all_new_actions_translation_not_found~~~~~~~~~~")
    -- print("~~~~~~~~~~END~~~~~~~~~~")
    -- print("~~~~~~~~~~\n~~~~~~~~~~\n~~~~~~~~~~\n~~~~~~~~~~")

    return all_actions -- useless?
end
