dofile_once("mods/spells_evolutions/files/lib/matolabu_tools.lua")

--
--
--
--
--

function spawn_fx(x, y) -- OPTI POSSIBLE SI PAS DANS L'ECRAN !
    EntityLoad("mods/spells_evolutions/files/entities/particles/image_emitters/transmutation_effect_custom.xml",
        x, y)
    EntityLoad("mods/spells_evolutions/files/entities/particles/gold_pickup_huge_custom.xml", x, y)
    GamePlaySound("data/audio/Desktop/projectiles.bank", "animals/wraith_glowing/shoot", x, y) -- random  3 sounds 
    GamePlaySound("data/audio/Desktop/projectiles.bank", "animals/wraith_glowing/shoot", x, y) -- random  3 sounds ( so 3x3=9 variations )
    GamePlaySound("data/audio/Desktop/materials.bank", "collision/metalhollow_barrel", x, y) -- NOT WORKING?
    -- son brillants, mystiques
    -- GamePlaySound("data/audio/Desktop/projectiles.bank", "animals/wraith_glowing/shoot", x, y) -- random  3 sounds 
    -- GamePlaySound("data/audio/Desktop/animals.bank", "animals/boss_centipede/shield/suck_projectile", x, y) -- centipede_shield_suck_01 to 06
    -- GamePlaySound("data/audio/Desktop/event_cues.bank", "event_cues/chest/create", x, y) -- chest
    -- GamePlaySound("data/audio/Desktop/event_cues.bank", "event_cues/spell_refresh/create", x, y) -- spell_refresh_1
    -- GamePlaySound("data/audio/Desktop/explosion.bank", "explosions/holy", x, y) -- bomb_holy_create_add_01 & 02
    -- GamePlaySound("data/audio/Desktop/event_cues.bank", "event_cues/rune/destroy", x, y)
    -- SONS IMPACT
    -- animals/
    -- GamePlaySound("data/audio/Desktop/animals.bank", "animals/failed_alchemist_b_orb/explode", x, y) -- failed_alchemist_b_orb_explode
    -- GamePlaySound("data/audio/Desktop/animals.bank", "animals/fireskull/death", x, y) -- fireskull_death_01 to 03
    -- GamePlaySound("data/audio/Desktop/animals.bank", "animals/tank/_land", x, y) -- tank_land_01 to 03
    -- SONS bonus
    -- event_cues
    -- GamePlaySound("data/audio/Desktop/event_cues.bank", "event_cues/heartbeat/create", x, y) -- heartbeat_double
    -- GamePlaySound("data/audio/Desktop/misc.bank", "game_effect/on_fire/create", x, y) 
    -- GamePlaySound("data/audio/Desktop/items.bank", "magic_wand/not_enough_mana_for_action", x, y) 
    -- GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y) 
end

--
--
--
--
--

function spawn_loot(loot_type, loot_name, x, y, neutral_spawn)

    if (neutral_spawn == nil) then
        neutral_spawn = false
    end

    if (neutral_spawn == false) then -- if neutral, then no fx
        spawn_fx(x, y - 5)
    end

    local loot
    local loot_modifier

    ------------------------------------------------------------------------------------------ drop SPELL --------------------------------
    if loot_type == "spell" then

        local proba_to_drop_spell = 100 -- to get from ModSetting like MM
        local proba_to_drop_modifier = 100 -- to get from ModSetting like MM

        -- proba to drop spell
        SetRandomSeed(GameGetFrameNum(), GameGetFrameNum() + 133)
        if Random(0, 99) < proba_to_drop_spell then

            local loot_spawn_gap = 10 -- CONSTANT, gap between spell & modifier
            local entity_has_modifier = false

            if (neutral_spawn == false) then -- if neutral, then no modifier

                -- if Modifier Madness mod is active & proba to drop modifier
                SetRandomSeed(GameGetFrameNum(), GameGetFrameNum() + 3000)
                if ModIsEnabled("modifier_potions") and ModIsEnabled("enemy_modifiers") and Random(0, 99) <
                    proba_to_drop_modifier then

                    -- if there's a modifier
                    local modifier_to_drop_id
                    local t_var_store_comps = EntityGetComponent(GetUpdatedEntityID(), "VariableStorageComponent") or {}
                    for i, comp in ipairs(t_var_store_comps) do
                        if ComponentGetValue2(comp, "name") == "modifier_storage_component" then

                            entity_has_modifier = true
                            -- ... spawn modifier
                            modifier_to_drop_id = ComponentGetValue2(comp, "value_string")
                            loot_modifier = CreateItemActionEntity(modifier_to_drop_id, x + loot_spawn_gap, y)
                            break

                        end
                    end
                end
            end

            -- spawn spell
            if (entity_has_modifier == false) then
                loot_spawn_gap = 0 -- if no modifier, no gap
            end

            if (neutral_spawn == true) then -- if neutral, then no gap
                loot_spawn_gap = 0
            end

            loot = CreateItemActionEntity(loot_name, x - loot_spawn_gap, y)

            -- TO ADD THE TEST: if mod "Spell Fusion Mod" is enable
            if SpellCanBeBoostedOrIsBoosted(loot_name) == true then -- no need to optimise, because do once
                local slot_sprite_path = GetSlotSpritePath(loot_name, true)
                local loot_SpriteComponent_item_bg = EntityGetFirstComponentIncludingDisabled(loot, "SpriteComponent",
                    "item_bg")
                ComponentSetValue2(loot_SpriteComponent_item_bg, "image_file", slot_sprite_path)
            end
        end
    end

    ---------------------------------------------------------------------------------------- Add velocity animation --------------------------------------
    -- print("loot = "..tostring(loot))
    -- print("loot_modifier = "..tostring(loot_modifier))

    if loot ~= nil then
        -- print("loot ~= nil")

        -- if there's no modifier 
        if loot_modifier == nil then
            -- print("loot_modifier == nil")

            -- set a bounce animation in a random direction between left and right
            local velocity_component_id = EntityGetFirstComponentIncludingDisabled(loot, "VelocityComponent")
            ComponentSetValue2(velocity_component_id, "mass", 10)
            local velocity_x = math.random(-10, 10) -- test 20 (old = 10)
            local velocity_y = math.random(-50, -100)
            ComponentSetValue2(velocity_component_id, "mVelocity", velocity_x, velocity_y)

            -- if there's modifier to spawn with the loot
        else
            -- print("loot_modifier ~= nil")

            -- set a bounce animation to the left for the loot 
            local velocity_component_id = EntityGetFirstComponentIncludingDisabled(loot, "VelocityComponent")
            ComponentSetValue2(velocity_component_id, "mass", 10)
            local velocity_x = math.random(-10, 0)
            local velocity_y = math.random(-25, -50)
            ComponentSetValue2(velocity_component_id, "mVelocity", velocity_x, velocity_y)

            -- set a bounce animation to the right for the modifier
            velocity_component_id = EntityGetFirstComponentIncludingDisabled(loot_modifier, "VelocityComponent")
            ComponentSetValue2(velocity_component_id, "mass", 10)
            velocity_x = math.random(0, 10)
            velocity_y = math.random(-25, -50)
            ComponentSetValue2(velocity_component_id, "mVelocity", velocity_x, velocity_y)

        end
    end
end
