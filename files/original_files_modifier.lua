local nxml = dofile_once("mods/spells_evolutions/files/lib/nxml.lua")

function modify_original_files()

    --------------------------------------------------------------------------------------------------------------------------
    -- DEBUG PROJ -- update "base_projectile.xml"
    --------------------------------------------------------------------------------------------------------------------------
    --[[ 
    -- create new xml component
    local component_members = {
        _tags = "enabled_in_world",
        execute_every_n_frame = "1",
        execute_times = "1",
        remove_after_executed = "1",
        script_source_file = "mods/spells_evolutions/files/_DEBUG/projectile_print.lua"
    }

    --
    -- add it in base_projectile.xml
    --
    local base_projectile_data = ModTextFileGetContent("data/entities/base_projectile.xml")
    local base_projectile_data_xml = nxml.parse(base_projectile_data)

    base_projectile_data_xml:add_child(nxml.new_element("LuaComponent", component_members))

    local base_projectile_data_patched = nxml.tostring(base_projectile_data_xml)

    ModTextFileSetContent("data/entities/base_projectile.xml", base_projectile_data_patched)

    --
    -- add it in base_projectile_physics.xml
    --
    local base_projectile_physics_data = ModTextFileGetContent("data/entities/base_projectile_physics.xml")
    local base_projectile_physics_data_xml = nxml.parse(base_projectile_physics_data)

    base_projectile_physics_data_xml:add_child(nxml.new_element("LuaComponent", component_members))

    local base_projectile_physics_data_patched = nxml.tostring(base_projectile_physics_data_xml)

    ModTextFileSetContent("data/entities/base_projectile_physics.xml", base_projectile_physics_data_patched)

    --
    -- add it in base_item_projectile.xml
    --
    local base_item_projectile_data = ModTextFileGetContent("data/entities/base_item_projectile.xml")
    local base_item_projectile_data_xml = nxml.parse(base_item_projectile_data)

    base_item_projectile_data_xml:add_child(nxml.new_element("LuaComponent", component_members))

    local base_item_projectile_data_patched = nxml.tostring(base_item_projectile_data_xml)

    ModTextFileSetContent("data/entities/base_item_projectile.xml", base_item_projectile_data_patched)
     ]]

    --------------------------------------------------------------------------------------------------------------------------
    -- BOOST INVENTORY -- update "player_base.xml"  to add the Lua script "boost_inventory"
    --------------------------------------------------------------------------------------------------------------------------

    -- create new xml component
    local component_members = {
        _tags = "enabled_in_inventory",
        execute_every_n_frame = "1",
        script_source_file = "mods/spells_evolutions/files/boost_inventory.lua"
    }

    -- add it in player_base.xml
    local player_base_data = ModTextFileGetContent("data/entities/player_base.xml")
    local player_base_data_xml = nxml.parse(player_base_data)

    player_base_data_xml:add_child(nxml.new_element("LuaComponent", component_members))

    local player_base_data_patched = nxml.tostring(player_base_data_xml)

    ModTextFileSetContent("data/entities/player_base.xml", player_base_data_patched)

    ---------------------------------------------------------------------------------------------------------------------------------------------
    -- BOOST SPELL -- update "base_custom_card.xml" and "action.xml" to add the Lua script "boost_spell"
    ---------------------------------------------------------------------------------------------------------------------------------------------
    -- this Lua file is useful for spells already spawned by the game in the world
    -- for spells spawned by the fusion, see monsters_drop_list.lua

    -- create new xml component
    local component_members = {
        _tags = "enabled_in_world,enabled_in_hand,enabled_in_inventory",
        execute_on_added = "0",
        execute_every_n_frame = "1",
        execute_times = "1",
        remove_after_executed = "1",
        script_source_file = "mods/spells_evolutions/files/boost_spell.lua"
    }

    -- add it in base_custom_card.xml
    local base_custom_card_data = ModTextFileGetContent("data/entities/base_custom_card.xml")
    local base_custom_card_xml = nxml.parse(base_custom_card_data)

    base_custom_card_xml:add_child(nxml.new_element("LuaComponent", component_members))

    local base_custom_card_data_patched = nxml.tostring(base_custom_card_xml)

    ModTextFileSetContent("data/entities/base_custom_card.xml", base_custom_card_data_patched)

    -- add it in action.xml
    local action_data = ModTextFileGetContent("data/entities/misc/custom_cards/action.xml")
    local action_xml = nxml.parse(action_data)

    action_xml:add_child(nxml.new_element("LuaComponent", component_members))

    local action_data_patched = nxml.tostring(action_xml)

    ModTextFileSetContent("data/entities/misc/custom_cards/action.xml", action_data_patched)

    ---------------------------------------------------------------------------------------------------------------------------------------------------
    -- SPELL MERGING -- update "base_custom_card.xml" and "action.xml" to add the Lua script "spell_merging"
    ---------------------------------------------------------------------------------------------------------------------------------------------------

    -- create new xml component
    local component_members = {
        _tags = "enabled_in_world",
        execute_on_added = "0",
        execute_every_n_frame = "30",
        script_source_file = "mods/spells_evolutions/files/spell_merging.lua"
    }

    -- add it in base_custom_card.xml
    local base_custom_card_data = ModTextFileGetContent("data/entities/base_custom_card.xml")
    local base_custom_card_xml = nxml.parse(base_custom_card_data)

    base_custom_card_xml:add_child(nxml.new_element("LuaComponent", component_members))

    local base_custom_card_data_patched = nxml.tostring(base_custom_card_xml)

    ModTextFileSetContent("data/entities/base_custom_card.xml", base_custom_card_data_patched)

    -- add it in action.xml
    local action_data = ModTextFileGetContent("data/entities/misc/custom_cards/action.xml")
    local action_xml = nxml.parse(action_data)

    action_xml:add_child(nxml.new_element("LuaComponent", component_members))

    local action_data_patched = nxml.tostring(action_xml)

    ModTextFileSetContent("data/entities/misc/custom_cards/action.xml", action_data_patched)

end

