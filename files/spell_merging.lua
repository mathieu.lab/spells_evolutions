dofile_once("mods/spells_evolutions/files/lib/matolabu_tools.lua")

local entity_id = GetUpdatedEntityID()

function SetExecuteEveryNFrame(entity_id, n)
    local t_lua_components = EntityGetComponentIncludingDisabled(entity_id, "LuaComponent")
    for i, lua_comp in pairs(t_lua_components) do
        if ComponentGetValue2(lua_comp, "script_source_file") == "mods/spells_evolutions/files/spell_merging.lua" then
            ComponentSetValue2(lua_comp, "execute_every_n_frame", n)
        end
    end
end

function StopMergeAndClean(merging_with_spell)
    EntityKillVariable(entity_id, "MergingStartedAt")
    EntityKillVariable(entity_id, "MergingWith")
    SetExecuteEveryNFrame(entity_id, 30)

    -- CLEAN the other spell, IF there's one
    if merging_with_spell ~= nil then
        EntityKillVariable(merging_with_spell, "MergingStartedAt")
        EntityKillVariable(merging_with_spell, "MergingWith")
        SetExecuteEveryNFrame(merging_with_spell, 30)
    end
end

-- IF I'm in player inventory
-- Theoretically this is not possible, because this LUA script onlu runs in "enabled_in_world"
if EntityGetParent(entity_id) ~= 0 then
    StopMergeAndClean()

    -- IF I'm in the world! AND I'm NOT a shop item
elseif EntityGetParent(entity_id) == 0 and EntityGetFirstComponent(entity_id, "ItemCostComponent") == nil then

    dofile_once("mods/spells_evolutions/files/monsters_drop_list.lua")
    dofile_once("mods/spells_evolutions/files/create_upgraded_spells.lua")

    local x, y = EntityGetTransform(entity_id)

    local script_path = "mods/spells_evolutions/files/spell_merging.lua"

    local entity_ItemComponent = EntityGetFirstComponentIncludingDisabled(entity_id, "ItemComponent")
    local entity_ItemComponent_uses_remaining
    local entity_ItemActionComponent = EntityGetFirstComponentIncludingDisabled(entity_id, "ItemActionComponent")
    local entity_action_id = ComponentGetValue2(entity_ItemActionComponent, "action_id")

    ---------------------------------------
    ---------------------------------------
    -- Check if I can be merged
    ---------------------------------------
    ---------------------------------------
    local spell_is_infinite_projectile = false
    local spell_can_be_merged = EntityGetVariable(entity_id, "spell_can_be_merged", "bool") -- OPTI

    -- IF already saved
    if spell_can_be_merged ~= nil then
        -- GET this one too
        spell_is_infinite_projectile = EntityGetVariable(entity_id, "spell_is_infinite_projectile", "bool")

        -- IF is ammo spell
        if spell_is_infinite_projectile == false and spell_can_be_merged == true then
            -- GET ammo
            entity_ItemComponent_uses_remaining = ComponentGetValue2(entity_ItemComponent, "uses_remaining")
        end

        -- IF no save (at first frame)
    else
        local entity_action_id_prefix = string.match(entity_action_id, "(.-)%_") or ""
        local entity_action_id_suffix = string.sub(entity_action_id, -5) or ""

        -- IF prefix is "_LVL5"
        if entity_action_id_prefix == spell_id_prefix and entity_action_id_suffix == "_LVL5" then
            -- Remove this LUA script
            RemoveLuaScript(entity_id, script_path)
            spell_can_be_merged = false
            EntitySetVariable(entity_id, "spell_can_be_merged", "bool", spell_can_be_merged)
            spell_is_infinite_projectile = true
            EntitySetVariable(entity_id, "spell_is_infinite_projectile", "bool", spell_is_infinite_projectile)

            -- IF I'm not a LVL5 spell
        else
            -- IF I'm an infinite projectile spell
            if SpellCanBeBoostedOrIsBoosted(entity_id) == true then
                spell_can_be_merged = true
                EntitySetVariable(entity_id, "spell_can_be_merged", "bool", spell_can_be_merged)
                spell_is_infinite_projectile = true
                EntitySetVariable(entity_id, "spell_is_infinite_projectile", "bool", spell_is_infinite_projectile)

                -- IF I'm NOT an infinite projectile spell
            else
                entity_ItemComponent_uses_remaining = ComponentGetValue2(entity_ItemComponent, "uses_remaining")

                -- IF I'm an infinite projectile spell (not possible at this point... right?)
                if entity_ItemComponent_uses_remaining == -1 then
                    RemoveLuaScript(entity_id, script_path)
                    spell_can_be_merged = false
                    EntitySetVariable(entity_id, "spell_can_be_merged", "bool", spell_can_be_merged)
                    spell_is_infinite_projectile = true
                    EntitySetVariable(entity_id, "spell_is_infinite_projectile", "bool", spell_is_infinite_projectile)

                    -- IF I'm a spell with limited ammo
                else
                    spell_can_be_merged = true
                    EntitySetVariable(entity_id, "spell_can_be_merged", "bool", spell_can_be_merged)
                    EntitySetVariable(entity_id, "spell_is_infinite_projectile", "bool", spell_is_infinite_projectile) -- false
                end
            end
        end
    end

    -- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    -- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    --                                                               Fusion FUNCTION
    -- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    -- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    function Fusion(card_action)

        -- offeset for VFX spawn
        -- local vfx_y = y - 15
        local vfx_y = y - 7.5 -- try this offset, to fit with the animation v1

        ----------------------------------------------------------------------------------------------------------------------
        if spell_is_infinite_projectile == true then ----------------      INFINITE PROJECTILE FUSION
            -------------------------------------------------------------------------------------------------------------------

            local spell_test_if_level = string.match(entity_action_id, "(.+)%_LVL%d")

            -- FUNCTION spawn the result spell of the fusion
            function SpawnBabySpell(name_of_baby_spell)
                local baby_spell_y_add = -5

                local card_action_x, card_action_y = EntityGetTransform(card_action)
                local spells_difference_x = x - card_action_x
                local demi_diff_x = spells_difference_x / 2
                local baby_spell_x = x - demi_diff_x
                local spells_difference_y = y - card_action_y
                local demi_diff_y = spells_difference_y / 2
                local baby_spell_y = (y - demi_diff_y) + baby_spell_y_add
                spawn_loot("spell", name_of_baby_spell, baby_spell_x, baby_spell_y, true) -- baby spell is born
                EntityKill(card_action) -- kill the other parent
                EntityKill(entity_id) -- kill myself
            end

            -- .........................................
            --        IF spell is LVL 1
            -- .........................................
            if spell_test_if_level == nil then
                local name_of_baby_spell = spell_id_prefix .. "_" .. entity_action_id .. "_LVL2"
                SpawnBabySpell(name_of_baby_spell)

                ---------------------------------------------------------------------- FUSION  1+1  ==   2 

                -- EntityLoad("mods/spells_evolutions/files/_DEBUG/test_fx_spawner.xml", x, vfx_y) ------ TESTER

                --------- VFX

                EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl2/fusion_to_lvl2.xml", x, vfx_y) --   ===== MEGA PACK
                EntityLoad("data/entities/particles/poof_blue.xml", x, vfx_y) -- gros, parts bleues  ===== PARTICULES
                EntityLoad("data/entities/particles/particle_explosion/main_blue_small.xml", x, vfx_y) -- trails bleu =========== TRAIL
                EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl2/polymorph_explosion_edit_blue.xml", x,
                    vfx_y) -- p'tites part qui flottent

                GameScreenshake(5, x, y)

                --------- SFX

                GamePlaySound("data/audio/Desktop/projectiles.bank", "animals/wraith_glowing/shoot", x, y) -- random  3 sounds 
                GamePlaySound("data/audio/Desktop/projectiles.bank", "animals/wraith_glowing/shoot", x, y) -- random  3 sounds 
                GamePlaySound("data/audio/Desktop/animals.bank", "animals/boss_centipede/shield/suck_projectile", x, y) -- centipede_shield_suck_01 to 06
                GamePlaySound("data/audio/Desktop/misc.bank", "game_effect/on_fire/create", x, y)
                GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)

                -- .........................................
                --        IF spell is LVL 2+
                -- .........................................
            else
                local spell_level = string.sub(entity_action_id, -1)
                local spell_level_number = tonumber(spell_level)
                local baby_spell_number = spell_level_number + 1
                local baby_spell = tostring(baby_spell_number)
                local name_of_baby_spell_without_number = string.sub(entity_action_id, 1, -2)
                local name_of_baby_spell = name_of_baby_spell_without_number .. baby_spell

                SpawnBabySpell(name_of_baby_spell)

                if spell_level_number == 2 then ---------------------------- FUSION  2+2  ==   3 

                    -- VFX

                    EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl3/fusion_to_lvl3.xml", x, vfx_y) --  ===== MEGA PACK
                    EntityLoad("data/entities/particles/poof_red_sparse.xml", x, vfx_y) -- moyen, rouge    ===== PARTICULES
                    EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl3/polymorph_explosion_edit_red.xml",
                        x, vfx_y) -- p'tites part qui flottent

                    GameScreenshake(10, x, y)

                    -- SFX

                    GamePlaySound("data/audio/Desktop/projectiles.bank", "animals/wraith_glowing/shoot", x, y) -- random  3 sounds 
                    GamePlaySound("data/audio/Desktop/projectiles.bank", "animals/wraith_glowing/shoot", x, y) -- random  3 sounds 
                    GamePlaySound("data/audio/Desktop/animals.bank", "animals/fireskull/death", x, y) -- fireskull_death_01 to 03
                    GamePlaySound("data/audio/Desktop/animals.bank", "animals/fireskull/death", x, y) -- fireskull_death_01 to 03
                    GamePlaySound("data/audio/Desktop/items.bank", "magic_wand/not_enough_mana_for_action", x, y)
                    GamePlaySound("data/audio/Desktop/misc.bank", "game_effect/on_fire/create", x, y)
                    GamePlaySound("data/audio/Desktop/misc.bank", "game_effect/on_fire/create", x, y)
                    GamePlaySound("data/audio/Desktop/misc.bank", "game_effect/on_fire/create", x, y)
                    GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                    GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                    GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                    GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                    GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                    GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)

                elseif spell_level_number == 3 then ----------------------- FUSION  3+3  ==   4 

                    -- VFX

                    EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl4/fusion_to_lvl4.xml", x, vfx_y) -- === MEGA PACK
                    EntityLoad("data/entities/particles/fireworks/firework_green.xml", x, vfx_y) -- gros pouf vert qui se dissipe    ========  PARTICULES
                    GameScreenshake(20, x, y)

                    -- SFX

                    GamePlaySound("data/audio/Desktop/projectiles.bank", "animals/wraith_glowing/shoot", x, y) -- random  3 sounds 
                    GamePlaySound("data/audio/Desktop/projectiles.bank", "animals/wraith_glowing/shoot", x, y) -- random  3 sounds 
                    GamePlaySound("data/audio/Desktop/projectiles.bank", "animals/wraith_glowing/shoot", x, y) -- random  3 sounds 
                    GamePlaySound("data/audio/Desktop/projectiles.bank", "animals/wraith_glowing/shoot", x, y) -- random  3 sounds 
                    GamePlaySound("data/audio/Desktop/animals.bank", "animals/failed_alchemist_b_orb/explode", x, y) -- failed_alchemist_b_orb_explode

                elseif spell_level_number == 4 then ----------------------- FUSION  4+4  ==   5

                    -- VFX

                    EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl5/fusion_to_lvl5.xml", x, vfx_y) -- == MEGA PACK CUSTOM
                    EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl5/firework_pink_edit.xml", x, vfx_y) -- gros gros pouf rose qui vol    ====  PARTICULES
                    EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl5/gold_pickup_huge_edit.xml", x,
                        vfx_y) -- gros eclat gold
                    EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl5/polymorph_explosion_edit.xml", x,
                        vfx_y) -- moyen fumée rose + parts roses
                    EntityLoad("mods/spells_evolutions/files/particles/fusion_to_lvl5/poof_pink_huge.xml", x, vfx_y) -- HUGE PART FOUT PARTOUT

                    GameScreenshake(30, x, y)

                    -- SFX

                    GamePlaySound("data/audio/Desktop/projectiles.bank", "animals/wraith_glowing/shoot", x, y) -- random  3 sounds 
                    GamePlaySound("data/audio/Desktop/projectiles.bank", "animals/wraith_glowing/shoot", x, y) -- random  3 sounds 
                    GamePlaySound("data/audio/Desktop/event_cues.bank", "event_cues/spell_refresh/create", x, y) -- spell_refresh_1
                    GamePlaySound("data/audio/Desktop/event_cues.bank", "event_cues/heartbeat/create", x, y) -- heartbeat_double
                    GamePlaySound("data/audio/Desktop/explosion.bank", "explosions/holy", x, y) -- bomb_holy_create_add_01 & 02
                    GamePlaySound("data/audio/Desktop/explosion.bank", "explosions/holy", x, y) -- bomb_holy_create_add_01 & 02
                    GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                    GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                    GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                end
            end

            -------------------------------------------------------------------------------------------------------------
        else ------------------------------------------------------------ MUNITION PROJECTILE FUSION
            -------------------------------------------------------------------------------------------------------------

            -- REF
            local card_action_ItemComponent = EntityGetFirstComponentIncludingDisabled(card_action, "ItemComponent")
            local card_action_ItemActionComponent = EntityGetFirstComponentIncludingDisabled(card_action,
                "ItemActionComponent")

            local card_action_ItemComponent_uses_remaining =
                ComponentGetValue2(card_action_ItemComponent, "uses_remaining")
            if card_action_ItemComponent_uses_remaining ~= nil then

                -- COMBINE munitions
                ComponentSetValue2(entity_ItemComponent, "uses_remaining",
                    entity_ItemComponent_uses_remaining + card_action_ItemComponent_uses_remaining)

                -- Bounce
                local entity_VelocityComponent = EntityGetFirstComponentIncludingDisabled(entity_id, "VelocityComponent")
                SetRandomSeed(GameGetFrameNum(), GameGetFrameNum() + 484)
                local velocity_x = math.random(-5, 5)
                local velocity_y = math.random(-60, -75)
                ComponentSetValue2(entity_VelocityComponent, "mVelocity", velocity_x, velocity_y)

                -- Kill the other one
                EntityKill(card_action)

                -- VFX -- Opti possible si pas dans l'écran ? 
                EntityLoad("data/entities/particles/particle_explosion/explosion_flare_small.xml", x, vfx_y) -- avec + part oranges
                -- EntityLoad("data/entities/particles/poof_red_sparse.xml", x, vfx_y) -- moyen, rouge
                EntityLoad("data/entities/particles/particle_explosion/explosion_smoke_pillar_red.xml", x, vfx_y) --  fumée rouge
                EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_red_slow.xml", x, vfx_y)
                EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_red_slow.xml", x, vfx_y)
                EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_red_slow.xml", x, vfx_y)
                EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_red_slow.xml", x, vfx_y)
                EntityLoad("data/entities/particles/particle_explosion/explosion_trail_swirl_red_slow.xml", x, vfx_y)
                EntityLoad("mods/spells_evolutions/files/entities/particles/gold_pickup_huge_custom.xml", x, vfx_y)

                EntityLoad("mods/spells_evolutions/files/particles/fusion_munition/main_gunpowder_small_edit.xml", x,
                    vfx_y)
                GameScreenshake(5, x, y)

                -- SFX   
                GamePlaySound("data/audio/Desktop/animals.bank", "animals/tank/_land", x, y) -- tank_land_01 to 03                       
                GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                GamePlaySound("data/audio/Desktop/player.bank", "game_effect/on_fire/game_effect_end", x, y)
                GamePlaySound("data/audio/Desktop/items.bank", "magic_wand/not_enough_mana_for_action", x, y)
            end
        end
    end

    -- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    -- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    -- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    --                                                                  Fusion PREPARATION
    -- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    -- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    -- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    local merging_duration = 105 -- constant 
    -- v1 = 100
    -- v2 = 150

    -- IF I'm NOT a limited ammo spell OR an infinite projectile spell (not possible at this point... right?)
    if spell_can_be_merged == false then
        RemoveLuaScript(entity_id, script_path)

        -- IF I'm a limited ammo spell OR an infinite projectile spell
    elseif spell_can_be_merged == true then

        local merge_started_at = EntityGetVariable(entity_id, "MergingStartedAt", "float")

        -- IF merge has already started
        if merge_started_at ~= nil then

            local merging_with_spell = EntityGetVariable(entity_id, "MergingWith", "int")
            local merging_with_spell_is_alive = EntityGetIsAlive(merging_with_spell)

            -- IF the spell I'm merging with is no longer alive
            if merging_with_spell_is_alive == false then
                StopMergeAndClean()

                -- IF the spell I'm merging with IS ALIVE
            elseif merging_with_spell_is_alive == true then

                local merging_with_spell_parent = EntityGetParent(merging_with_spell)

                -- IF the spell I'm merging with is NOT in the world
                if merging_with_spell_parent ~= 0 then
                    StopMergeAndClean(merging_with_spell)

                    -- IF the spell I'm merging with IS in the world
                elseif merging_with_spell_parent == 0 then

                    local merge_elapsed_time = GameGetFrameNum() - merge_started_at

                    -- IF the merging animation is finished
                    if merge_elapsed_time >= merging_duration then

                        -- IF my ID is lower than the other spell ID,    I do the Fusion (SECU: avoid dupplicate fusion)
                        if entity_id < merging_with_spell then
                            Fusion(merging_with_spell)
                        end

                        -- IF the merging animation is pending
                    else

                        -- Animation v1 xD ------- START \
                        local velocity_component_id = EntityGetFirstComponentIncludingDisabled(entity_id,
                            "VelocityComponent")

                        local spell_x, spell_y = EntityGetTransform(merging_with_spell)

                        local x_dif = spell_x - x
                        local y_dif = spell_y - y
                        local x_orientation = x_dif / (math.abs(x_dif))

                        local x_vel
                        if merge_elapsed_time < merging_duration * 0.95 then
                            x_vel = x_orientation * -1 * (merge_elapsed_time ^ 3) / 10000 * 0.5
                        else
                            x_vel = x_dif * merge_elapsed_time / 5 * 0.75
                        end

                        local y_vel = (y_dif * 5) - 15

                        ComponentSetValue2(velocity_component_id, "mVelocity", x_vel, y_vel)
                        -- Animation v1 xD ------- END /

                        --[[ -- Animation v2 :O ------- START \

                        -- local x_at_start = EntityGetVariable(entity_id, "x_at_start", "float")
                        -- local y_at_start = EntityGetVariable(entity_id, "y_at_start", "float")
                        local x_at_start = 250
                        local y_at_start = -100
                        local merge_on_the_right = EntityGetVariable(entity_id, "merge_on_the_right", "float")

                        -- CALCULATE movement X
                        local speed_init = 20 -- less is more

                        local speed_to_remove_max = speed_init - 6 -- 

                        -- movement based on duration percentage
                        local duration_percentage = merge_elapsed_time / merging_duration -- from 0 to 1 
                        local speed_progression = duration_percentage^2 -- change this to accuentuate the acceleration effect

                        -- amplification
                        local speed_to_remove = speed_to_remove_max * speed_progression -- from 0 to "speed_init - 5" with the "progression" behaviour
                        local speed_current = speed_init - speed_to_remove -- from "speed_init" to speed max

                        merge_elapsed_time = merge_elapsed_time / speed_current
                        
                        local distance_init = 20
                        local distance_to_remove_max = distance_init - 5 -- 15
                        local distance_progression = duration_percentage^3 -- change this to accuentuate the acceleration effect
                        local distance_to_remove = distance_to_remove_max * distance_progression-- from 0 to 15 with the "progression" behaviour
                        local distance_offset = distance_to_remove * -1

                        local x_current = x_at_start +
                                              (math.sin(merge_elapsed_time + (math.pi * merge_on_the_right)) * (distance_init + distance_offset))
                        local y_current = y_at_start +
                                              (math.sin(
                                merge_elapsed_time + (math.pi * merge_on_the_right) + (math.pi / 2)) * (distance_init + distance_offset))

                        -- SET entity location
                        local _, _, rot, scale_x, scale_y = EntityGetTransform(entity_id)
                        EntitySetTransform(entity_id, x_current, y_current, rot, scale_x, scale_y)

                        -- Animation v2 :O ------- END / ]]

                    end

                end
            end

            -- IF no merge started
        elseif merge_started_at == nil then

            -- GET all cards in radius
            local distance_to_merge = 16
            local t_card_action_in_radius = EntityGetInRadiusWithTag(x, y, distance_to_merge, "card_action")

            -- FOR each cards
            for i, card_action in pairs(t_card_action_in_radius) do

                -- IF my id is lower than the card ID (SECU: only one of the two spells, do the fusion!)
                if tonumber(entity_id) < tonumber(card_action) then

                    -- IF card is in world AND card is not myself
                    if EntityGetParent(card_action) == 0 and card_action ~= entity_id then

                        -- IF card is NOT a shop item
                        if EntityGetFirstComponent(card_action, "ItemCostComponent") == nil then

                            local t_card_action_all_components = EntityGetAllComponents(card_action)
                            local card_action_ItemComponent =
                                EntityGetFirstComponentIncludingDisabled(card_action, "ItemComponent")
                            local card_action_ItemActionComponent =
                                EntityGetFirstComponentIncludingDisabled(card_action, "ItemActionComponent")

                            -- IF ItemComponent & ItemActionComponent are well referenced
                            if card_action_ItemComponent ~= nil and card_action_ItemActionComponent ~= nil then

                                -- IF the card_action is the exact same spell than me
                                if ComponentGetValue2(card_action_ItemActionComponent, "action_id") ==
                                    ComponentGetValue2(entity_ItemActionComponent, "action_id") then

                                    -- IF the card_action is NOT already in a merge
                                    if EntityGetVariable(card_action, "MergingStartedAt", "float") == nil then

                                        -- Start merge!
                                        EntitySetVariable(entity_id, "MergingStartedAt", "float", GameGetFrameNum())
                                        EntitySetVariable(card_action, "MergingStartedAt", "float", GameGetFrameNum())

                                        EntitySetVariable(entity_id, "MergingWith", "int", card_action)
                                        EntitySetVariable(card_action, "MergingWith", "int", entity_id)

                                        -- CALCUL the point location between the two spells
                                        local card_action_x, card_action_y = EntityGetTransform(card_action)
                                        local x_mid = x+((card_action_x-x)/2)
                                        local y_mid = y+((card_action_y-y)/2)

                                        GamePlaySound("data/audio/Desktop/event_cues.bank", "event_cues/rune/destroy", x_mid, y_mid) -- spell_refresh_1
                                        GamePlaySound("data/audio/Desktop/event_cues.bank", "event_cues/rune/destroy", x_mid, y_mid) -- spell_refresh_1

                                        -- SET NFrame to 1 to be able to play merging animation
                                        SetExecuteEveryNFrame(entity_id, 1)
                                        SetExecuteEveryNFrame(card_action, 1)
                                        -- script_source_file = "mods/spells_evolutions/files/spell_merging.lua"

                                        ---------- Mergin begin FX:

                                        -- large éclat de parts blanches
                                        EntityLoad(
                                            "mods/spells_evolutions/files/particles/fusion_begin/poof_white_edit.xml",
                                            x_mid, y_mid)
                                        -- parts blanches qui tombent + part blanches qui restent en suspension
                                        EntityLoad(
                                            "mods/spells_evolutions/files/particles/fusion_begin//dust_explosion_edit.xml",
                                            x_mid, y_mid)

                                        -- Little bounce
                                        local bounce_force_y = -67
                                        -- PhysicsApplyForce( entity_id, 0, bounce_force_y) -- do nothing
                                        -- PhysicsApplyForce( card_action, 0, bounce_force_y) -- do nothing

                                        local entity_id_VelocityComponent =
                                            EntityGetFirstComponentIncludingDisabled(entity_id, "VelocityComponent")
                                        local card_action_VelocityComponent =
                                            EntityGetFirstComponentIncludingDisabled(card_action, "VelocityComponent")

                                        ComponentSetValue2(entity_id_VelocityComponent, "mVelocity", 0, bounce_force_y)
                                        ComponentSetValue2(card_action_VelocityComponent, "mVelocity", 0, bounce_force_y)

                                        ------------------------------------
                                        -- Animation v2 Needs 
                                        ------------------------------------

                                        -- -- SAVE locations
                                        -- local entity_x_at_start, entity_y_at_start = EntityGetTransform(entity_id)
                                        -- EntitySetVariable(entity_id, "x_at_start", "float", entity_x_at_start)
                                        -- EntitySetVariable(entity_id, "y_at_start", "float", entity_y_at_start)

                                        -- local card_action_x_at_start, card_action_y_at_start = EntityGetTransform(
                                        --     card_action)
                                        -- EntitySetVariable(card_action, "x_at_start", "float", card_action_x_at_start)
                                        -- EntitySetVariable(card_action, "y_at_start", "float", card_action_y_at_start)

                                        -- -- SAVE who's on the left, who's on the right
                                        -- local entity_x = EntityGetTransform(entity_id)
                                        -- local card_action_x = EntityGetTransform(card_action)
                                        -- if entity_x < card_action_x or entity_x == card_action_x then
                                        --     EntitySetVariable(entity_id, "merge_on_the_right", "float", 0)
                                        --     EntitySetVariable(card_action, "merge_on_the_right", "float", 1)
                                        -- else
                                        --     EntitySetVariable(entity_id, "merge_on_the_right", "float", 1)
                                        --     EntitySetVariable(card_action, "merge_on_the_right", "float", 0)
                                        -- end

                                        -- IF a merge is started, no need to continue, let's BREAK!
                                        break
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end
