dofile_once("mods/spells_evolutions/files/lib/matolabu_tools.lua")

-- print("*\n*********************************************Au début de action de SPELLS EVOLUTION\n*")

-- get the spell.name prefixes
dofile_once("mods/spells_evolutions/files/config.lua")

-- to AVOID doing this script after game started
local players = EntityGetWithTag("player_unit")

-- zePrint("_\n___ #players = " .. tostring(#players))

if #players == 0 then

    -- print(">\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> si y'a pas de joueur\n>")

    function thedeepcopy(orig) -- thx Philadelphus > thanks, http://lua-users.org/wiki/CopyTable
        local orig_type = type(orig)
        local copy
        if orig_type == 'table' then
            copy = {}
            for orig_key, orig_value in next, orig, nil do
                copy[thedeepcopy(orig_key)] = thedeepcopy(orig_value)
            end
            setmetatable(copy, thedeepcopy(getmetatable(orig)))
        else -- number, string, boolean, etc
            copy = orig
        end
        return copy
    end

    dofile("mods/spells_evolutions/files/upgraded_spells_translation_not_found.lua")
    -- local t_actions_snapshot = thedeepcopy(evo_notrans_actions)

    -- zePrint("-\n------- evo_notrans_actions = " .. tostring(evo_notrans_actions))
    -- zePrint("-\n------- number_of_actions = " .. tostring(#t_actions_snapshot))
    -- zePrint("-\n------- number_of_actions = " .. tostring(#evo_notrans_actions))

    for i, spell in pairs(evo_notrans_actions) do

        -- zePrint("-\n----> Spell ID = " .. tostring(spell.id))

        -- GET the original spell ID + boost level
        local spell_original_id, boost_level = string.match(spell.id, "EVO_(.+)_LVL(%d)")

        -- zePrint("----> boost_level = " .. tostring(boost_level))

        -- IF it's a "Infinity" spell
        local spell_is_dg_infinite = string.match(spell_original_id, "DG_INFINITE_(.+)")
        if spell_is_dg_infinite ~= nil then

            -- REMOVE the DG_INFINITE_ prefix
            spell_original_id = spell_is_dg_infinite

            spell_is_dg_infinite = true
        else
            spell_is_dg_infinite = false
        end

        -- zePrint("---->>>> spell_is_dg_infinite ??????????????? = " .. tostring(spell_is_dg_infinite))

        -- zePrint("---->>>> Original spell ID = " .. tostring(spell_original_id))

        local translated_name

        -- GET translated name of the original spell
        for j, action in pairs(actions) do
            if action.id == spell_original_id then

                -- zePrint("---->>>> description = " .. tostring(GameTextGet(action.description)))

                if GameTextGet(action.description) ~= "" or nil then

                    -- zePrint("---->>>> description trouvée !!!!!!! C'est parti pour chopper le nom !")

                    translated_name = GameTextGet(action.name)
                end

                break
            end
        end

        -- zePrint("---->>>> translated_name = " .. tostring(translated_name))

        if translated_name ~= "" or nil then

            -- FIND the name prefix depending on boost level
            local name_prefix = GetNamePrefix(boost_level)

            -- zePrint("---->>>> name_prefix = " .. tostring(name_prefix))

            name_prefix = name_prefix .. " "

            -- GET Infinity prefix if needed
            local infinity_prefix = ""
            if spell_is_dg_infinite then
                dofile("mods/Infinity/config.lua")
                infinity_prefix = cfg_infinite_prefix .. " "
            end

            -- SET name
            local final_name = name_prefix .. infinity_prefix .. translated_name

            -- zePrint("---->>>> FINAL NAME  = " .. tostring(final_name))

            -- spell["name"] = final_name
            -- table.insert(actions, spell)
            
            -- FIND the identical spell in "action" and
            -- UPDATE the name
            for j, action in pairs (actions) do
                if action.id == spell.id then
                    action.name = final_name

                    break
                end
            end

        end

    end -- for "t_actions_snapshot"

end -- IF #players == 0

--
--
--
--

-- zePrint(" ---\n---\n---\n---\n================== current ACTIONS ====================\n---\n---\n---")
-- for i, spell in pairs(actions) do
--     zePrint("-->> spell.id   =   " .. tostring(spell.id))
--     zePrint("---->>>> spell.name   =   " .. tostring(spell.name))
--     -- zePrint("------>>>>>> spell.description   =   " .. tostring(spell.description))
--     zePrint("----")
-- end
--

-- for i, action in pairs (actions) do
--     if action.id == "DG_INFINITE_BOMB" then
--         action.name = "prout"

--         print(" ---->>> test_à_la_con  =  " .. tostring(action.name))

--         break
--     end
-- end
--
--
--
--
--
--
--

-- for i = 1, #t_actions_snapshot do
--     local spell = t_actions_snapshot[i]

-- end

-- local evo_found = false

-- for i = 1, #t_actions_snapshot do
--     local spell = t_actions_snapshot[i]

-- -- zePrint("Spells Evolution,,,,,,,, spell  :   " .. tostring(spell["id"]) .. "   =   " .. tostring(spell["name"]))

-- if (spell["id"] == "LIGHT_BULLET") then

--     zePrint("~~~~~~~~~~ On passe par ici, PROUT on passe par là. ~~~~~~~~~~~~~~~~~~~~")

-- end

-- if (spell["id"] == "EVO_LIGHT_BULLET_LVL2") then

--     zePrint(
--         " ---- J'ai trouvé EVO_LIGHT_BULLET_LVL2 -- PIPI les spells sont bien créés !!!!!!!!! -----------------------------")

--     evo_found = true
-- end

-- end

-- zePrint("------ evo_found  =   " .. tostring(evo_found))

-- if evo_found == false then
--     zePrint(" ---- CUCU Je n'ai pas trouvé de EVO spell, alors on les crée ! -----------------------------")

-- end

-- ça ça marche, mais ça m'aide pas
-- for i, action in ipairs(actions) do
--     zePrint("zePrint actions in action : " .. tostring(action.id) .. "   =   " .. tostring(action.name))
-- end

-- zePrint("A la fin de action de SPELLS EVOLUTION _________________________________________")

--
--
--
--
--
--
--

--->>>>>>>>>>>>>>>>>>>
-- dofile_once("mods/spells_evolutions/files/lib/matolabu_tools.lua")

-- local player_id = getPlayerEntity()

-- zePrint("On cherche le player, s'il y en a un, son ID est = " .. tostring(player_id))

-- if player_id ~= nil then

--     local spell_created = EntityGetVariable(player_id, "spell_created", "bool")

--     zePrint("--- >>> spell_created = " .. tostring(spell_created))

--     if spell_created == nil then

--         zePrint("On crée les spells mamen !!!!!! ---<-<-<-<-<-<-<-<-<")

--         -- -- add Upgradeable Spells actions to the origina Noita's actions
--         -- dofile_once("mods/spells_evolutions/files/create_upgraded_spells.lua")
--         -- CreateUpgradedSpells()

--         EntitySetVariable(player_id, "spell_created", "bool", true)
--     end

-- end
-- <<<<<<<<<<<<<<<<<<<<<<<<<<

--
--
--
--
--
--
--
--
--
--
--

-- for i = 1, #actions_snapshot do
--     local spell = actions_snapshot[i]
--     if (spell["max_uses"] ~= nil) then
--         spell["id"] = "DG_INFINITE_" .. spell["id"]
--         if (string.sub(spell["name"], 1, 1) == "$") then
--             spell["name"] = cfg_infinite_prefix .. " " .. GameTextGet(spell["name"])

--             zePrint (">>>>    Infinity_mod    <<<<<    spell name = " .. tostring(spell["name"])) ---------------------------------------------------

--         else
--             spell["name"] = cfg_infinite_prefix .. " " .. spell["name"]
--         end

--         local split = split_string(spell["spawn_probability"], ",")
--         for i, v in ipairs(split) do
--             split[i] = v * cfg_rarity_multiplier
--         end
--         probability = table.concat(split, ",")

--         spell["spawn_probability"] = probability
--         spell["price"] = math.floor(spell["price"] * cfg_price_multiplier)
--         spell["mana"] = math.floor(spell["mana"] * cfg_mana_multiplier)
--         spell["custom_xml_file"] = nil
--         spell["max_uses"] = nil
--         table.insert(actions, spell)
--     end
-- end
