-- The spell.id prefix for boosted spells
spell_id_prefix = "EVO" -- global, "TEMP" to change in final version to "EVO"

name_prefix_lvl2 = "Super"
name_prefix_lvl3 = "Hyper"
name_prefix_lvl4 = "Ultra"
name_prefix_lvl5 = "Legendary"

function GetNamePrefix (boost_level)
    local prefix = ""
    local level = tostring(boost_level)
    if level == "2" then
        prefix = name_prefix_lvl2
    elseif level == "3" then
        prefix = name_prefix_lvl3
    elseif level == "4" then
        prefix = name_prefix_lvl4
    elseif level == "5" then
        prefix = name_prefix_lvl5
    end

    return prefix
end