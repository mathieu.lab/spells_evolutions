local inventory_is_open = GameIsInventoryOpen() or false

if inventory_is_open == true then

    dofile_once("mods/spells_evolutions/files/lib/matolabu_tools.lua")

    -- print(("-----------------------------------------------------"))
    
    -- local hovered_spell = false
    -- local inventories_contain_boost_spells = 0

    --------------------------------------------------------------------------------------------
    --------------------------------------  FUNCTIONS  -----------------------------------
    --------------------------------------------------------------------------------------------
    function GetInventoryPosition(entity)
        local item_component = EntityGetFirstComponentIncludingDisabled(entity, "ItemComponent")
        return ComponentGetValue2(item_component, "inventory_slot")
    end

    function GetMousePositionVirtual(gui)
        screen_width, screen_height = GuiGetScreenDimensions(gui);

        local mouse_pos_x_world, mouse_pos_y_world = DEBUG_GetMouseWorld()

        local virtual_resolution_x = MagicNumbersGetValue("VIRTUAL_RESOLUTION_X")
        local virtual_resolution_y = MagicNumbersGetValue("VIRTUAL_RESOLUTION_Y")

        -- USELESS?
        -- local scale_x = virtual_resolution_x / screen_width
        -- local scale_y = virtual_resolution_y / screen_height

        local camera_pos_x_world, camera_pos_y_world = GameGetCameraPos()

        -- USELESS? 
        -- local mouse_x_real = mouse_pos_x_world - camera_pos_x_world +214.5
        -- local mouse_y_real = mouse_pos_y_world - camera_pos_y_world + 120.00001525879

        -- on cherche Av
        -- Av / Aw * W / V
        -- Aw = différence entre : mouse_pos_world ET camera_pos_world (mouse_diff_cam_x_world)
        -- V = virtual_resolution (virtual_resolution_x)
        -- W = screen_size (screen_width)
        -- donc, Av = Aw * V / W

        local mouse_diff_cam_x_world = mouse_pos_x_world - camera_pos_x_world
        local mouse_diff_cam_y_world = mouse_pos_y_world - camera_pos_y_world

        local mouse_pos_x_virtual =
            ((mouse_diff_cam_x_world * screen_width / virtual_resolution_x) + screen_width / 2) + 1.499
        local mouse_pos_y_virtual =
            ((mouse_diff_cam_y_world * screen_height / virtual_resolution_y) + screen_height / 2) - 1.486

        -- print(">>>>>>>>>>>>>> mouse_pos_x_virtual = " .. tostring(mouse_pos_x_virtual))
        -- print(">>>>>>>>>>>>>> mouse_pos_y_virtual = ".. tostring(mouse_pos_y_virtual))

        -- local sx, sy = (mouse_pos_x_world - camera_pos_x_world) / scale_x + screen_width / 2 + 1.5, (mouse_pos_y_world - camera_pos_y_world) / scale_y + screen_height / 2

        return mouse_pos_x_virtual, mouse_pos_y_virtual
    end

    function incr_1(value)
        value = value + 1
        return value
    end

    --[[ 
    function UpdateHoveredSpellBool(gui)
        local g_clicked, g_right_clicked, g_hovered, g_x, g_y, g_width, g_height, g_draw_x, g_draw_y, g_draw_width,
            g_draw_height = GuiGetPreviousWidgetInfo(gui)

        -- print(">>>>>>>>>>>>>> g_clicked = ".. tostring(g_clicked))
        -- print(">>>>>>>>>>>>>> g_right_clicked = ".. tostring(g_right_clicked))
        -- print(">>>>>>>>>>>>>> g_hovered = ".. tostring(g_hovered))
        -- print(">>>>>>>>>>>>>> g_x = ".. tostring(g_x))
        -- print(">>>>>>>>>>>>>> g_y = ".. tostring(g_y))
        -- print(">>>>>>>>>>>>>> g_width = ".. tostring(g_width))
        -- print(">>>>>>>>>>>>>> g_height = ".. tostring(g_height))
        -- print(">>>>>>>>>>>>>> g_draw_x = ".. tostring(g_draw_x))
        -- print(">>>>>>>>>>>>>> g_draw_y = ".. tostring(g_draw_y))
        -- print(">>>>>>>>>>>>>> g_draw_width = ".. tostring(g_draw_width))
        -- print(">>>>>>>>>>>>>> g_draw_height = ".. tostring(g_draw_height))

        if g_hovered == true then
            hovered_spell = true
        end
    end
     ]]

    --------------------------------------------------------------------------------------------
    ---------------------------------------  REFS  -------------------------------------------
    --------------------------------------------------------------------------------------------
    local entity_id = GetUpdatedEntityID()

    -- PrintEntity(entity_id)
    -- local x, y = EntityGetTransform(entity_id)
    -- local t_entities_in_radius = EntityGetInRadius(x, y, 100) or {}
    -- for k, v in pairs(t_entities_in_radius) do
    --     PrintEntity(v)
    --     local t_v_children = EntityGetAllChildren(v) or {}
    --     for k2, v2 in pairs(t_v_children) do
    --         PrintEntity(v2)
    --         local t_v2_children = EntityGetAllChildren(v2) or {}
    --         for k3, v3 in pairs(t_v2_children) do
    --             PrintEntity(v3)
    --             local t_v3_children = EntityGetAllChildren(v3) or {}
    --             for k4, v4 in pairs(t_v3_children) do
    --                 PrintEntity(v4)
    --             end
    --         end
    --     end
    -- end
    -- print("________________________________________________________________________________________________")

    local t_player_children = EntityGetAllChildren(entity_id)

    -- get inventories entities
    local ivnentory_quick
    local inventory_full
    for k, v in pairs(t_player_children) do
        if EntityGetName(v) == "inventory_quick" then
            ivnentory_quick = v
            -- PrintEntity(ivnentory_quick)
        elseif EntityGetName(v) == "inventory_full" then
            inventory_full = v
            -- PrintEntity(inventory_full)
        end

        if ivnentory_quick ~= nil and inventory_full ~= nil then
            break -- we got what we need, quit!
        end
    end

    -- get or create GUI
    gui = gui or GuiCreate()
    GuiStartFrame(gui)

    -- local mx, my = GuiGetMousePosition(gui)
    local current_id = 4372635 -- why this number? is it just a random big number? (added +1111111 in case of...)

    -- prepare GUI location calculation
    local slot_x = 0
    local slot_y = 0
    local inv_full_offset_x = 190
    local inv_full_offset_y = 20
    local slot_side = 20

    function InvFullGetX()
        local x = inv_full_offset_x + (slot_x * slot_side)
        return x
    end

    function InvFullGetY()
        local y = inv_full_offset_y + (slot_y * slot_side)
        return y
    end

    --------------------------------------------------------
    -- create slots for spells in inventory full
    --------------------------------------------------------
    local t_inv_full_spells = EntityGetAllChildren(inventory_full) or {}
    for i, spell in pairs(t_inv_full_spells) do

        -- LOAD spell_can_be_boosted_or_is_boosted value
        local spell_can_be_boosted_or_is_boosted =
            EntityGetVariable(spell, "spell_can_be_boosted_or_is_boosted", "bool") -- OPTI
        -- IF no value, let's create it
        if spell_can_be_boosted_or_is_boosted == nil then
            spell_can_be_boosted_or_is_boosted = SpellCanBeBoostedOrIsBoosted(spell)
            EntitySetVariable(spell, "spell_can_be_boosted_or_is_boosted", "bool", spell_can_be_boosted_or_is_boosted)
        end

        -- WIP new frame for munition spells
        --[[ 
        -- IF it's NOT a boost spell, let's CHECK IF it's a ammo spell
        if spell_can_be_boosted_or_is_boosted == false then

            -- LOAD spell_can_be_cumulated value
            local spell_can_be_cumulated = EntityGetVariable(spell, "spell_can_be_cumulated", "bool") -- OPTI
            -- IF no valaue, let's create it
            if spell_can_be_cumulated == nil then
                spell_can_be_cumulated = SpellCanBeCumulated(spell)
                EntitySetVariable(spell, "spell_can_be_cumulated", "bool", spell_can_be_cumulated)
            end

        end ]]

        -- print("spell = " .. tostring(spell))
        -- print("spell_can_be_boosted_or_is_boosted = " .. tostring(spell_can_be_boosted_or_is_boosted))

        if spell_can_be_boosted_or_is_boosted == true then
            slot_x, slot_y = GetInventoryPosition(spell)

            -- LOAD slot sprite
            local slot_sprite_path = EntityGetVariable(spell, "slot_sprite_path", "string") -- OPTI
            -- IF no value, let's create it
            if slot_sprite_path == nil then
                slot_sprite_path = GetSlotSpritePath(spell, false)
                EntitySetVariable(spell, "slot_sprite_path", "string", slot_sprite_path)
            end

            GuiZSetForNextWidget(gui, -0.99)
            GuiImage(gui, incr_1(current_id), InvFullGetX(), InvFullGetY(), slot_sprite_path, 1, 1, 1)
            -- GuiButton( gui:obj, id:int, x:number, y:number, text:string )
            -- GuiImageButton( gui:obj, id:int, x:number, y:number, text:string, sprite_filename:string )

            -- UpdateHoveredSpellBool(gui)
            -- inventories_contain_boost_spells = inventories_contain_boost_spells + 1

            -- print(">>>>>>>>>>>>>> 1 spell in the big inventory!")
        end
    end

    -------------------------------------------------------------------------------------
    -- create slots for spells in wands of inventory inventory_quick
    -------------------------------------------------------------------------------------
    local inv_quick_offset_x = 25
    local inv_quick_offset_y = 77
    local inv_quick_wand_gap_y_base = 39 -- base if wand sprite y is <= 6pixels...
    local inv_quick_wand_gap_y_added = 0 -- ...If more, add: (WandSpriteY - 6) * 2

    function InvQuickGetX(slot_x)
        local x = inv_quick_offset_x + (slot_x * slot_side)
        return x
    end

    function InvQuickGetY(wand_rank)
        local y = inv_quick_offset_y + (wand_rank * (inv_quick_wand_gap_y_base + slot_side)) +
                      inv_quick_wand_gap_y_added
        return y
    end

    -- get all wands in inventory quick
    local t_inv_quick_items = EntityGetAllChildren(ivnentory_quick)
    local t_inv_quick_wands = {}
    for i, item in pairs(t_inv_quick_items) do
        -- if EntityHasTag(item, "wand") == true then
        if EntityHasTag(item, "wand") == true or EntityHasTag(item, "shovel") == true then
            table.insert(t_inv_quick_wands, item)
        end
    end

    for i, wand in pairs(t_inv_quick_wands) do

        -- get the Y offset that can be added if the wand sprite height is bigger than 6 pixels
        local wand_gap_y_added = EntityGetVariable(wand, "wand_gap_y_added", "float") -- OPTI
        if wand_gap_y_added == nil then
            local wand_AbilityComponent = EntityGetFirstComponentIncludingDisabled(wand, "AbilityComponent")
            local wand_sprite_file = ComponentGetValue2(wand_AbilityComponent, "sprite_file")
            local wand_sprite_x, wand_sprite_y = GuiGetImageDimensions(gui, wand_sprite_file, 1)

            -- inv_quick_wand_gap_y_added = 0
            if wand_sprite_y > 6 then
                wand_gap_y_added = (wand_sprite_y - 6) * 2 -- is cumulative for each wand
            else
                wand_gap_y_added = 0
            end
            EntitySetVariable(wand, "wand_gap_y_added", "float", wand_gap_y_added)
        end

        -- add it to the current gap
        inv_quick_wand_gap_y_added = inv_quick_wand_gap_y_added + wand_gap_y_added

        -- create widgets for this wand
        local t_wand_spells = EntityGetAllChildren(wand) or {}

        -- PrintEntity(wand)
        for j, spell in pairs(t_wand_spells) do
            -- PrintEntity(spell)

            --[[ 
            --
            --
            -- print("-- =========== START print entity + components + members ===========")

            -- print("entity id = " .. tostring(spell))
            -- print("entity name = " .. tostring(EntityGetName(spell)))
            -- print("entity tags = " .. tostring(EntityGetTags(spell)))

            local entity_x, entity_y = EntityGetTransform(spell)
            -- print("entity_x = " .. tostring(entity_x))
            -- print("entity_y = " .. tostring(entity_y))

            local t_entity_children = EntityGetAllChildren(spell) or {}
            -- print("entity number of children = " .. tostring(#t_entity_children))

            local t_entity_components = EntityGetAllComponents(spell)
            for i, comp in pairs(t_entity_components) do
                local comp_name = ComponentGetTypeName(comp)

                if comp_name == "SpriteComponent" or comp_name == "ItemComponent" then
                    print("\n\n----- ComponentGetTypeName = " .. ComponentGetTypeName(comp))
                    local members = ComponentGetMembers(comp)
                    for j, comp2 in pairs(members) do
                        if j == "spawn_pos" 
                        or j == "inventory_slot" 
                        or j == "transform_offset" 
                        or j == "offset_animator_offset" 
                        then
                            local x, y = ComponentGetValue2(comp, j)
                            print(tostring(j) .. " =                                " .. tostring(x) .. "                        ;                   ".. tostring(y))
                        else
                            print(tostring(j) .. " = " .. tostring(comp2))
                        end
                    end
                end
            end

            -- print("-- =========== END print an entity components and members ===========")
            print("________________________________________________________________________________________________")
            print("________________________________________________________________________________________________")
            print("________________________________________________________________________________________________")
            print("________________________________________________________________________________________________")
            print("________________________________________________________________________________________________")
            --
            --
 ]]

            -- if not a permanent action
            local spell_ItemComponent = EntityGetFirstComponentIncludingDisabled(spell, "ItemComponent")
            local spell_permanently_attached = ComponentGetValue2(spell_ItemComponent, "permanently_attached")
            if spell_permanently_attached == false then

                local spell_can_be_boosted_or_is_boosted = EntityGetVariable(spell,
                    "spell_can_be_boosted_or_is_boosted", "bool") -- OPTI
                if spell_can_be_boosted_or_is_boosted == nil then
                    spell_can_be_boosted_or_is_boosted = SpellCanBeBoostedOrIsBoosted(spell)
                    EntitySetVariable(spell, "spell_can_be_boosted_or_is_boosted", "bool",
                        spell_can_be_boosted_or_is_boosted)
                end

                if spell_can_be_boosted_or_is_boosted == true then

                    local spell_slot_x = GetInventoryPosition(spell) -- from 0 to 29, 30 cut the screen, 31+ are out of the screen

                    local slot_sprite_path = EntityGetVariable(spell, "slot_sprite_path", "string") -- OPTI
                    if slot_sprite_path == nil then
                        slot_sprite_path = GetSlotSpritePath(spell, false)
                        EntitySetVariable(spell, "slot_sprite_path", "string", slot_sprite_path)
                    end

                    GuiZSetForNextWidget(gui, -0.99)
                    GuiImage(gui, incr_1(current_id), InvQuickGetX(spell_slot_x), InvQuickGetY(i - 1), slot_sprite_path,
                        1, 1, 1)

                    -- UpdateHoveredSpellBool(gui)
                    -- inventories_contain_boost_spells = inventories_contain_boost_spells + 1

                    -- print(">>>>>>>>>>>>>> 1 spell in a wand!")
                end
            end
        end
    end -- end: parse wands in inventory quick

    ----------------------------------------------------------------------------------------------------------
    --------------------------------------  SPELL DRAG & DROP  ------------------------------------
    ----------------------------------------------------------------------------------------------------------

    local dragging = false
    local dragging_previous = false
    local hold_click_first_pos_x
    local hold_click_first_pos_y

    -- print([[_____
    -- ____
    -- _____
    -- ____
    -- _____
    -- ____
    -- _____
    -- ____   ____
    -- _____
    -- ____
    -- _____
    -- ____
    -- _____
    -- ____
    -- _____
    -- ____
    -- _____   ____
    -- _____
    -- ____
    -- _____
    -- ____
    -- _____
    -- ____
    -- _____
    -- ____
    -- _____
    -- _____
    -- ____
    -- _____
    -- ____
    -- _____
    -- ____
    -- _____
    -- ____
    -- _____
    -- ____
    -- ___]])

    -- PAS POSSIBLE DE FAIRE LE DRAG & DROP PUTAIN DE MERDE !!!
    -- Parce que "Player > ControlsComponent > mButtonDownLeftClick"
    -- ne fonctionne que si on ne survole pas un HUD... FAIT IECH !!!!
    -- Tant pis, j'ai tout tenté, tout printé, rien trouvé d'autre... sans regret. x'D
    --[[ 
    if inventories_contain_boost_spells > 0 then
        if CanPlayerTinkerWithWands() == true then

            local controls_component = EntityGetFirstComponentIncludingDisabled(entity_id, "ControlsComponent")
            local button_down_left_click = ComponentGetValue2(controls_component, "mButtonDownLeftClick")

            -- print(">>>>>>>>>>>>>> button_down_left_click = ".. tostring(button_down_left_click)) ----> JE SUIS BAISéééééééé !!!!!!

            if button_down_left_click == true then

                dragging_previous = EntityGetVariable(entity_id, "DraggingPrevious", "bool")

                if dragging_previous == nil or dragging_previous == false then -- if NOT dragging

                    if hovered_spell == false then
                        -- STOP
                    elseif hovered_spell == true then
                        hold_click_first_pos_x = EntityGetVariable(entity_id, "HoldClickFirstPosX", "float")

                        local cursor_pos_x, cursor_pos_y = GetMousePositionVirtual(gui)

                        if hold_click_first_pos_x == nil then -- No first pos, 
                            -- mean that this is the first frame of the movement

                            EntitySetVariable(entity_id, "HoldClickFirstPosX", "float", cursor_pos_x)
                            EntitySetVariable(entity_id, "HoldClickFirstPosY", "float", cursor_pos_y)

                        else -- There's a first pos,
                            -- mean the movement is in progress
                            local hold_click_first_pos_y = EntityGetVariable(entity_id, "HoldClickFirstPosY", "float")

                            local start_drag_dist = 1.9 -- constant

                            local cursor_distance_since_start_move_x = math.abs(cursor_pos_x - hold_click_first_pos_x)
                            local cursor_distance_since_start_move_y = math.abs(cursor_pos_y - hold_click_first_pos_y)

                            if cursor_distance_since_start_move_x > start_drag_dist or
                                cursor_distance_since_start_move_y > start_drag_dist then
                                dragging = true
                            end
                        end
                    end
                end -- end: if dragging_previous ~= true

                if dragging_previous == true or dragging == true then -- if dragging
                    dragging = true

                    -- TODO LERP + ADD kill "IconPosPreviousX" & "-Y" in "Update for next frame"
                    -- local icon_pos_previous_x = EntityGetVariable(entity_id, "IconPosPreviousX", "float")
                    -- local icon_pos_previous_y = EntityGetVariable(entity_id, "IconPosPreviousY", "float")

                    local cursor_pos_x, cursor_pos_y = GetMousePositionVirtual(gui)

                    local icon_pos_new_x = cursor_pos_x + 20
                    local icon_pos_new_y = cursor_pos_y + 20

                    local slot_sprite_path = "mods/spells_evolutions/files/ui_gfx/slots/slot_lvl_5"

                    GuiZSetForNextWidget(gui, -0.99)
                    GuiImage(gui, incr_1(current_id), icon_pos_new_x, icon_pos_new_y, slot_sprite_path, 1, 1, 1)
                end

            end
        end
    end -- end: if inventories_contain_spells > 0

    -- Updates for next frame
    EntitySetVariable(entity_id, "DraggingPrevious", "bool", dragging)

    if hold_click_first_pos_x == nil then
        EntityKillVariable(entity_id, "HoldClickFirstPosX")
        EntityKillVariable(entity_id, "HoldClickFirstPosY")
    end
     ]]

end -- end: if inventory_is_open == true
